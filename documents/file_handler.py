# -*- coding: UTF-8 -*-
from documents.models import Document, File


class FileHandler():
    
    def __init__(self, farm):
        self.farm = farm

    def validateName(self, fileName):
        """
        Verify if exist a file with this name on database
        """
        if Document.objects.filter(farm = self.farm, name = fileName):
            return False
        return True
    
    
    def saveDocument(self, form, request):
        """
        Save document
        """
        doc_file = File()
        doc_file.doc_file = request.FILES['document']
        
        document = form.save(commit=False)
        document.name = request.FILES['document'].name
        document.farm = self.farm        
        document.save()
        
        doc_file.document = document
        doc_file.save()
        document.document.add(doc_file)