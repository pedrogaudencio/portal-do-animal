# -*- coding: UTF-8 -*-
from documents.forms import AddCsv
from documents.file_handler import FileHandler 
from utils.csv_handler import Handle_csv_upload, Handle_csv_generator
from django.template.context import RequestContext
from django.utils.translation import ugettext as _
from django.contrib.auth.decorators import login_required
from exploracao.models import Farm
from animal.models import Bovine
from django.shortcuts import render_to_response


@login_required 
def csvImport(request, slug = None):
    '''
    colcocar que só superadmin ou donos de exploracao podem no fazer(de acordo com o id da exploração)
    '''
    farm  = Farm.objects.get(slug = slug)
    csv_v = []
    fileResult = ''
    if request.method == 'POST':
        form = AddCsv(request.POST, request.FILES)
        if form.is_valid():
            #Document is unique or document isn't unique and user want to save it anyway
            if FileHandler(farm).validateName(request.FILES['document'].name) or 'save_yes' in request.POST:
                FileHandler(farm).saveDocument(form, request)
                csv_v = Handle_csv_upload(request.FILES['document'].name, farm, 
                                          form.cleaned_data['country']).importCsv()
            #Ask user if want to save it replacing the old one
            else:
                fileResult = _('A file with this name already exists. Save it anyway?')
    else:
        form = AddCsv()
 
    context = {'farm': farm, 
               'form': form,
               'csv': csv_v,
               'file': fileResult,
               }
    #alterar pasta do template
    return render_to_response('exploracao/csv/csv.html', context, 
                              context_instance=RequestContext(request))


@login_required
def csvSniraExport(request):
    '''
    add:  exploracao_id, animalsList
    Export a csv file, Sinira type
    '''
    farm  = Farm.objects.get(id = 1) #FIX id
    animalsList = Bovine.objects.filter(exploracao = 1)[:5]
    generator = Handle_csv_generator()

    response, dict_error = generator.sniraExport(farm, animalsList, 'I')

    if dict_error.keys():
        if request.method == 'POST':
            if 'generate' in request.POST:
                return response
        else:
            context={'dict_error': dict_error,
                    }
            return render_to_response('exploracao/csv/csv_generator.html', context,
                                  context_instance=RequestContext(request))
    
    return response    