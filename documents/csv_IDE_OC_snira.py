# -*- coding: UTF-8 -*-
import csv
from datetime import datetime

from entidade.models import Entities, Breeder
from exploracao.models import Farm
from animal.models import Animal
from utils.reference_values import snira 
from utils.nif import Nif
 
from django.core.exceptions import ObjectDoesNotExist



class Csv_Validation():
           
    def validate(self, farm, csv_data, csv_name):
        """
        Validate csv according to SNIRA specifications 52 possible errors 
        """
        for row in csv_data:    
            
            #Error 1
            if not Nif().validate(row[0], 'Portugal'):
                return 'Erro'
            
            #Error 2
            """
            try:
                breeder = Breeder.objects.get(nif = int(row[0]))
            except ObjectDoesNotExist:
                return 'Erro'
            """
            
            #Error 3
            
            #Error 4 - COLOCAR OUTRA DESCRIÇÂO DE ERRO - UMA NOSS
            if farm.breeder.nif != int(row[0]):
                return 'Erro'
            
            #Error 5 - FALTA VERIFICAR SE É AGENTE IDENTIFICADOR REGISTADO
            """
            if row[1]:
                try:
                    identifier_agent = Entities.objects.get(nif = int(row[1])) 
                except ObjectDoesNotExist:
                    return 'Erro'
            """
            #Error 6
            if not row[4]:
                return 'Erro'

            #Error 7
            if row[4] not in snira.SNIRA_SPECIES:
                return 'Erro'
            
            #Error 8
            if len(row[6]) not in [15, 16]:
                return 'Erro'
            
            #Error 9
            ide_number = row[6]
            if ide_number[:3] not in snira.SNIRA_COUNTRIES.values():
                return 'Erro'
            
            #Error 10
            if not row[8]:
                return 'Erro'
            
            #Error 11
            if row[8] not in snira.SNIRA_SPECIES:
                return 'Erro'
            
            #Error 12
            if not row[9]:
                return 'Erro'
            
            #Error 13
            if row[9] == '02' and row[9] not in snira.SNIRA_OVINE:
                return 'Err0'
            if row[9] == '03' and row[9] not in snira.SNIRA_CAPRINE:
                return 'Erro' 
            
            #Error 14
            if not row[10]:
                return 'Erro'
            
            #Error 15
            if not row[10] in ['F','M','C']:
                return 'Erro'
            
            #Error 16
            if not row[3]:
                return 'Error'
            
            #Error 18 and 17 VERIFICAR QUAL O EXECPT
            try:
                identification_date = datetime.strptime(row[3],'%Y%m%d')
            except:
                return 'Error'
            
            if datetime.today() < identification_date:
                return 'Error'
            
            #Error 19
            if not row[11]:
                return 'Error'
            
            #Error 20
            birthdate = datetime.strptime(row[11],'%Y%m%d').date()
            if datetime.today().date() < birthdate:
                return 'Error'
            
            #Error 21 *
            entry_date = datetime.strptime(row[12],'%Y%m%d').date()
            if datetime.today().date() < entry_date:
                return 'Error'
            
            #Error 22
            if entry_date < birthdate:
                return 'Error'
            
            #Error 23
            if not row[2]:
                return 'Error'
            
            #Error 24
            
            #Error 25
            try:
                Farm.objects.get(trademark = row[2])
            except ObjectDoesNotExist:
                return 'Error'
            
            
            #Error 26
            if row[2] != farm.trademark:
                return 'Error'
            
            #Error 27 - Penso que seja isto
            if row[17] == 'A':
                try:
                    Animal.objects.get(sia = row[6], farm__trademark = row[2])
                except ObjectDoesNotExist:
                    return 'Error'
            
            #Error 28
            if not row[17]:
                return 'Error'
            
            #Error 29 e 30
            if row[17] not in ['I', 'A']:
                return 'Error'
            
            #Error 31
            
            #Error 32
            if row[17] == 'I':
                try:
                    Animal.objects.get(sia = row[6])
                    return 'Error'
                except ObjectDoesNotExist:
                    pass
            
            #Error 33
            
            #Error 34
            try:
                animal = Animal.objects.get(sia = row[6])
                if animal.farm.breeder.nif != int(row[0]):
                    return 'Error'
            except ObjectDoesNotExist:
                print 'ATENÇÃO - csv_IDE_OC_snira: error 34'
            
            #Error 35
            
            #Error 36
            if row[17] == 'A':
                try:
                    Animal.objects.get(sia = row[6])
                except ObjectDoesNotExist:
                    return 'Error'
            
            #Error 37
            #Error 38
            #Error 39
            #Error 40
            if row[5] and row[5] not in ['S', 'N']:
                return 'Error'
            
            #Error 41
            if len(row[13]) > 30:
                return 'Error'
            
            #Error 42
            if len(row[14]) > 13:
                return 'Error'
            
            #Error 43
            if len(row[15]) > 16:
                return 'Error'
            
            #Error 44
            if len(row[16]) > 16:
                return 'Error'
            
            #Error 45
            if len(csv_name) != 28:
                return 'Error'
            
            
            #Used for error 46, 47, 48 (see ahead)
            csv_name_nif = csv_name[:9]
            csv_name_date = csv_name[10:18]
            csv_name_hour = csv_name[18:]
            
            #Error 49
            #ATENÇÂO VER QUAL O EXCEPT
            try:
                #nif
                if type(int(csv_name_nif)) is int and Nif().validate(csv_name_nif , 'Portugal'):
                    pass
                #date
                csv_name_date = datetime.strptime(csv_name_date,'%Y%m%d').date()
                #hour
                csv_name_hour = datetime.strptime(csv_name_hour,'%H%M%S').time() 
            except:
                return 'Error'
            
            #Error 46
            if int(csv_name_nif) != farm.breeder.nif:
                return 'Error'

            #Error 47
            if  datetime.today().year < csv_name_date.year:
                return 'Error'
            
            
            #Error 48
            if datetime.today().date < csv_name_date:
                return 'Error'
            
            #Error 50
            
            #Error 51
            
            #Error 52
            if len(row) != 18:
                return 'Error'