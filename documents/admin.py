# -*- coding: UTF-8 -*-
from django.contrib import admin
from documents.models import Document, File


class DocumentAdmin(admin.ModelAdmin):
    list_display = ('name', 'farm')
    list_filter = ['farm']
    search_fields = ['name']

admin.site.register(Document, DocumentAdmin)
admin.site.register(File)