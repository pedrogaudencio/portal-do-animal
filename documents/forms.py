# -*- coding: UTF-8 -*-
from documents.models import Document
from django import forms
from django.forms.models import ModelForm
from django.utils.translation import ugettext as _


class AddCsv(ModelForm):
    
    CSV_COUNTRY_CHOICES = (('pt', _('Portugal')),)
    country = forms.ChoiceField(label = _('Country'), choices = CSV_COUNTRY_CHOICES, required = False)
    
        
    class Meta:
        model = Document
        exclude = ['name', 'date', 'farm', 'document', 'active']
    
    
    def __init__(self, *args, **kwargs):
        super(AddCsv, self).__init__(*args, **kwargs)
        self.fields['document'] = forms.FileField(_('Document'))
        

    def clean_document(self):
        """
        Clean - Max file size: 500MB  
        """
        from django.core.exceptions import ValidationError
        
        document = self.cleaned_data['document'] 
        
        if document.size > 500*1024*1024:
            raise ValidationError(_('The file you tried to upload is too large. Please upload a file of less than 500 Mb.'))
        
        return document
