# -*- coding: UTF-8 -*-
from django.db import models
from datetime import datetime    
from django.utils.translation import ugettext as _
import os


class Document(models.Model):
    name = models.CharField(_('Document name'), max_length = 150, blank = True, null = True)
    farm = models.ForeignKey('exploracao.Farm', verbose_name = _('Farm'), blank = False, null = False)
    document = models.ManyToManyField('File', verbose_name = _('Files'), related_name = 'document_file')
    date = models.DateTimeField(_('Date'), default=datetime.now, blank = True)
    active = models.BooleanField(_('Active'), default = True)
    
    
    def __unicode__(self):
        return '%s' % self.name
    
    
    class Meta():
        verbose_name = _('Document')
        verbose_name_plural = _('Documents')
        

def upload_path_handler(instance, filename):
    """
    If farm doesn't have a folder on the portal, create one with is name.
    """
    
    return os.path.join('documentos/' + instance.document.farm.name, filename)
       

class File(models.Model):
    document = models.ForeignKey('Document', related_name = 'file_document', 
                                 verbose_name = _('Document'), blank = True, null = True)
    doc_file = models.FileField(upload_to = upload_path_handler, blank = False, null = False)
    active = models.BooleanField(_('Active'), default = True) 
    
    def __unicode__(self):
        return '%s' % self.doc_file
    
    
    class Meta():
        verbose_name = _('Document file')
        verbose_name_plural = _('Document files')