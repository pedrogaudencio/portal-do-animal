# -*- coding: UTF-8 -*-
from django.contrib.auth.decorators import login_required
from django.template.context import RequestContext
from django.shortcuts import render_to_response
from preferences.models import Preference
from preferences.forms import ParametersOrdenhaForm, DryOrdenhaForm
from exploracao.models import Farm
import ast


@login_required
def ordenhaPref(request, slug = None):
    '''
    Preferences for ordenhas
    1- Milk parameters
    2- Drying animal prefs (number of milking, quantity and measure) 
    '''

    user = request.user
    farm = Farm.objects.get(slug = slug)
    
    try:    
        initial_param = Preference.objects.get(farm = farm, preferenceType = 'ParAnalyzed')
        initial_param = {'Parametros': initial_param.data}
    except:
        initial_param = None
        
    try:
        initial_dry = Preference.objects.get(farm = farm, preferenceType = 'DryAnimal')
        initial_dry = ast.literal_eval(initial_dry.data)
    except:
        initial_dry = None
    
    
    if request.method == 'POST':
        if 'submit' in request.POST:
            parForm = ParametersOrdenhaForm(user, farm, request.POST)
            dryForm = DryOrdenhaForm(user, farm, request.POST)
            
            if parForm.is_valid() and dryForm.is_valid():
                parForm.save()
                dryForm.save()
                
    else:
        parForm = ParametersOrdenhaForm(user, farm, initial = initial_param)
        dryForm = DryOrdenhaForm(user, farm, initial = initial_dry)
    
    context = {'parForm': parForm,
               'dryForm': dryForm,
               }
    
    return render_to_response('preferences/ordenhaPref.html', context,
                                  context_instance=RequestContext(request)) 