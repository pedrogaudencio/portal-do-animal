# -*- coding: UTF-8 -*-
from django.db import models
from django.utils.encoding import force_unicode
from django.utils.translation import ugettext as _



class DryAnimalManager(models.Manager):
    def get_query_set(self):
        return super(DryAnimalManager, self).get_query_set().filter(preferenceType = 'DryAnimal')


class Preference(models.Model):
    
    PREF_CHOICES = (('ParAnalyzed', 'ParAnalyzed'),('DryAnimal','DryAnimal'),)
    
    preferenceType = models.CharField(_('Preference type'), choices = PREF_CHOICES, max_length = 150, 
                                      blank = True, null = True)
    farm = models.ForeignKey('exploracao.Farm')
    user = models.ForeignKey('utilizador.UserProfile', blank = True, null = True)
    data = models.TextField(blank = True, null = True)
    
    '''
    Managers one for witch preference
    '''
    objects = models.Manager()
    dryAnimal = DryAnimalManager()
    

    def __unicode__(self):
        return '%s - %s' % (force_unicode(self.farm), self.preferenceType)
    
    class Meta:
        verbose_name = _('Preference')
        verbose_name_plural = _('Preferences')
        ordering = ('farm',)
        