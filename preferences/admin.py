# -*- coding: UTF-8 -*-
from preferences.models import Preference
from django.contrib import admin


admin.site.register(Preference)