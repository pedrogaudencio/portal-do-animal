# -*- coding: UTF-8 -*-
from django import forms
from ordenha.models import ParametersAnalyzed
from preferences.models import Preference
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext as _


class CheckboxSelectMultipleWidget(forms.CheckboxSelectMultiple):
    '''
    Renders horizontal multiple check box.
    '''
    def render(self, *args, **kwargs):
        output = super(CheckboxSelectMultipleWidget, self).render(*args,**kwargs) 
        return mark_safe(output.replace(u'<ul>', u'').replace(u'</ul>', u'').replace(u'<li>', u'').replace(u'</li>', u''))


class DryOrdenhaForm(forms.Form):
    '''
    Form for drying out animals prefs
    '''
    
    def __init__(self, user, farm, *args, **kwargs):
        super(DryOrdenhaForm, self).__init__(*args, **kwargs)
        self.user = user
        self.farm = farm
        
    MEASURE_TYPE = (('l', _('L')),('kg', _('Kg')))
    AUTO_DRY = (('1', _('Notify')), ('2', _('Notify and close')), ('3', _('Close')))
    
    milking_number = forms.IntegerField(label = _('Number of milkings'))
    milk_quantity = forms.CharField(label = _('Milk quantity'))
    measure_type = forms.ChoiceField(label = _('Measure type'), choices = MEASURE_TYPE) 
    average = forms.BooleanField(label = _('Do average'), required = False)
    auto_dry_action = forms.ChoiceField(label = _('Auto dry'), 
                                        help_text = _('Action to take on auto dry'), 
                                        choices = AUTO_DRY)

    
    def save(self):
        '''
        Search for a initial pref object, if exists, rewrite that object with the new data
        '''
        try:
            dryPref = Preference.objects.get(farm = self.farm, preferenceType = 'DryAnimal')
        except:
            dryPref = Preference()
            
        dryPref.preferenceType = 'DryAnimal'
        dryPref.farm = self.farm
        
        data = {'milking_number': self.cleaned_data['milking_number'],
                'milk_quantity': self.cleaned_data['milk_quantity'],
                'measure_type': self.cleaned_data['measure_type'],
                'average': self.cleaned_data['average'],
                'auto_dry_action': self.cleaned_data['auto_dry_action'],
                }
        
        dryPref.data = data
        dryPref.save()
        

class ParametersOrdenhaForm(forms.Form):
    '''
    Form for parameters used in milk analysis 
    '''
    def __init__(self, user, farm, *args, **kwargs):
        super(ParametersOrdenhaForm, self).__init__(*args, **kwargs)
        self.user = user
        self.farm = farm
        self.parameters = ParametersAnalyzed.objects.only('name_pt')
        
        self.fields['Parametros'] = forms.ModelMultipleChoiceField(self.parameters, 
                                                                   label = _('Parameters'),  
                                                                   widget=CheckboxSelectMultipleWidget())


    def save(self):
        '''
        Search for a initial pref object, if exists, rewrite that object with the new data
        ''' 
        pList = []
        
        try:
            ordPref = Preference.objects.get(farm = self.farm, preferenceType = 'ParAnalyzed')
        except:
            ordPref = Preference()
        
        ordPref.preferenceType = 'ParAnalyzed'
        ordPref.farm = self.farm    
        parameters = self.cleaned_data['Parametros']

        for p in parameters:
            pList.append(p.id)
    
        ordPref.data = pList
        ordPref.save()
        