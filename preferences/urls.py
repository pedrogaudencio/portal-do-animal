# -*- coding: UTF-8 -*-
from django.conf.urls.defaults import patterns, url


"""
Preferences urls
"""
urlpatterns = patterns('preferences.views',
    #--------------Milking-------------#
    #url(r'^milking/(?P<slug>[-\w\d]+)/$', 'ordenhaPref'), #View
    url(r'^milking/(?P<slug>[-\w\d]+)/edit/$', 'ordenhaPref'), #Edit

)