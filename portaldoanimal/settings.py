# -*- coding: UTF-8 -*-
# Django settings for portaldoanimal project.
import os

'''ADDED'''
DIRNAME = os.path.abspath(os.path.dirname(__file__).decode('utf-8'))
AUTH_PROFILE_MODULE = 'utilizador.UtilizadorProfile'
LOGIN_URL = '/login'
LOGIN_REDIRECT_URL = '/'
#DEFAULT_FROM_EMAIL = ''        # For example no-reply@YOUREMAILSERVER.COM
#EMAIL_HOST = ''               # For example smtp.YOUREMAILSERVER.COM
#MAIL_HOST_USER = 'simaojf@iol.pt'           # For example YOURNAME@YOUREMAILSERVER.COM
#EMAIL_HOST_PASSWORD = ''       # Your email password
EMAIL_HOST = 'localhost'
EMAIL_PORT = 1025 
#EMAIL_USE_TLS = False
#EMAIL_HOST = 'localhost'

SESSION_EXPIRE_AT_BROWSER_CLOSE = True
'''
Para testar email: python -m smtpd -n -c DebuggingServer localhost:1025
'''

#message
MESSAGE_STORAGE = 'django.contrib.messages.storage.session.SessionStorage'


DEBUG = True
TEMPLATE_DEBUG = DEBUG

ADMINS = (
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'sqlite.db',                      # Or path to database file if using sqlite3.
        'USER': '',                      # Not used with sqlite3.
        'PASSWORD': '',                  # Not used with sqlite3.
        'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    }
}

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'Europe/Lisbon'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'pt-PT'

# django-transmeta stuff:
ugettext = lambda s: s
LANGUAGES = (
    ('pt', ugettext('Portugues')),
    ('en', ugettext('English')),
    ('es', ugettext('Espanol')),
    ('fr', ugettext('Francais')),
)
TRANSMETA_DEFAULT_LANGUAGE = 'pt'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale
USE_L10N = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = os.path.join(DIRNAME, 'media/')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = '/media/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = os.path.join(DIRNAME, 'static/')

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'

# URL prefix for admin static files -- CSS, JavaScript and images.
# Make sure to use a trailing slash.
# Examples: "http://foo.com/static/admin/", "/static/admin/".
ADMIN_MEDIA_PREFIX = '/static/admin/'

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
    'dajaxice.finders.DajaxiceFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'ui*h-&j-pvbp-xd2#!6n=h2&=-w8gr99*a0#ls^!8&b%wym0$u'

AUTHENTICATION_BACKENDS = (
    'utilizador.backends.UserProfileAuthenticationBackend',
    'django.contrib.auth.backends.ModelBackend', # this is default
    'guardian.backends.ObjectPermissionBackend',
)

ANONYMOUS_USER_ID = -1
GUARDIAN_RAISE_403 = True
#GUARDIAN_TEMPLATE_403 = True

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    'django.template.loaders.eggs.Loader',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.core.context_processors.media',
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.core.context_processors.request',
    'django.contrib.messages.context_processors.messages',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
#    'debug_toolbar.middleware.DebugToolbarMiddleware', #debug toolbar
    'django.core.files.uploadhandler.MemoryFileUploadHandler',
    'django.core.files.uploadhandler.TemporaryFileUploadHandler',
)

THUMBNAIL_PROCESSORS = (
    'easy_thumbnails.processors.colorspace',
    'easy_thumbnails.processors.autocrop',
    #'easy_thumbnails.processors.scale_and_crop',
    'filer.thumbnail_processors.scale_and_crop_with_subject_location',
    'easy_thumbnails.processors.filters',
)

ROOT_URLCONF = 'portaldoanimal.urls'

TEMPLATE_DIRS = (os.path.join(DIRNAME, 'templates'),)
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.


INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # Uncomment the next line to enable the admin:
    # Uncomment the next line to enable admin documentation:
    'django.contrib.admindocs',
    #Dajax stuff
    'dajaxice',
    'dajax',

    # portal do animal:
    'animal',
    'contactos',
    'exploracao',
    'identificacao',
    'ordenha',
    'partos',
    'red',
    'reproducao',
    'utilizador',
    'entidade',
    'sanidade',
    'documents',
    'preferences',
    'performance',
    'utils',

    #outros:
    'notification',
    'autoslug',
    'cities_light',
    'mptt',
    #'easy_thumbnails',
    'django_google_maps',
    #'filer',
    #'devserver',
    
    #permissões:
    'guardian',

    #migrações:
    'south',

)

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}
