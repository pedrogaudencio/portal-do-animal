# -*- coding: UTF-8 -*-
from django.contrib import admin
from django.conf.urls.defaults import patterns, include, url
from dajaxice.core import dajaxice_autodiscover, dajaxice_config
import os

admin.autodiscover()
dajaxice_autodiscover()

DIRNAME = os.path.abspath(os.path.dirname(__file__).decode('utf-8'))

urlpatterns = patterns('',
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^notifications/', include('notification.urls')),

    #url para autenticacao
    url(r'^login/$', 'django.contrib.auth.views.login', name='login'),
    url(r'^logout/$', 'django.contrib.auth.views.logout', name='logout'),
    #url para registo de utilizador
    url(r'^newUser/$', 'utilizador.views.NewUser', name='new_user'),
    
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': DIRNAME + '/media/'}),
    url(r'^static/(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': DIRNAME + '/media/'}),
)

urlpatterns += patterns('',
    url(r'^animal/', include('animal.urls')),
    url(r'^exploracao/', include('exploracao.urls')),
    
    url(dajaxice_config.dajaxice_url, include('dajaxice.urls')),

    #--------------INDEX-----------------------#
    url(r'^$', 'exploracao.views.index', name = 'index' ),

    #url(r'^ajax/submit/$', 'AjaxSubmit'),

    #---------------ORDENHAS-------------------#
    url(r'^milking/', include('ordenha.urls'), name='milking'),
    #-------------PERFORMANCE------------------#
    url(r'^performance/', include('performance.urls'), name='performance'),
    #-------------PREFERENCES------------------#
    url(r'^preferences/', include('preferences.urls'), name='preferences'),
    #---------------Documents------------------#
    url(r'^documents/', include('documents.urls')),
    #---------------ENTIDADES------------------#
    url(r'^entidade/', include('entidade.urls')),
    #-------------------RED--------------------#
    url(r'^(?P<farm_pk>\d+)/red/', include('red.urls'), name='red'),
    #url(r'^(?P<farm_pk>\d+)/red/', include('red.urls')),
    #url(r'^media/admin/jsi18n', 'django.views.i18n.javascript_catalog'),
    #---------------Identification------------------#
    url(r'^identification/', include('identificacao.urls'), name='identification'),
    
    
    
    #TESTES
    url(r'^teste/sia$', 'teste.views.sia'),
    url(r'^teste/nif$', 'teste.views.nif'),
    url(r'^teste/nib$', 'teste.views.nib'),
    url(r'^teste/csvExport', 'documents.views.csvSniraExport'),

#    url(r'^identificacao/', include('identificacao.urls')),
#    (r'^partos/', include('partos.urls')),
#    (r'^reproducao/', include('reproducao.urls')),
#    (r'^vacinacao/', include('vacinacao.urls')),
#    (r'^animal/', include('animal.urls')),
#    (r'^animal/', include('animal.urls')),
)

urlpatterns += patterns('',
    url(r'^farms/', include('exploracao.urls'), name='farms'),
    #url(r'^ordenha/', include('ordenha.urls')),
)




"""
    url(r'^lactation/add/(?P<lactation_pk>\d+)/$', 'ordenha.views.modifyLactation'),
    url(r'^lactation/add$', 'ordenha.views.addLactation'),
    url(r'^lactation/(?P<animal_pk>\d+)', 'ordenha.views.viewLactation'),
    url(r'^lactation/$', 'ordenha.views.viewLactation'),
    
    url(r'^milking_times/add/(?P<milking_times_pk>\d+)/$', 'ordenha.views.milkingTimes'),
    url(r'^milking_times/add$', 'ordenha.views.milkingTimes'),
    url(r'^milking_times/$', 'ordenha.views.viewMilkingTimes'),
    
    url(r'^milking/add/(?P<milking_pk>\d+)/$', 'ordenha.views.modifyMilking'),
    url(r'^milking/add$', 'ordenha.views.addMilking'),
    url(r'^milking/$', 'ordenha.views.viewMilking'),
    
    url(r'^milk_storage/add/(?P<storage_pk>\d+)/$', 'ordenha.views.milkStorage'),
    url(r'^milk_storage/add$', 'ordenha.views.milkStorage'),
    url(r'^milk_storage/$', 'ordenha.views.viewMilkSorage'),

    url(r'^contrast/add/(?P<contrast_pk>\d+)/$', 'ordenha.views.modifyContrast'),
    url(r'^contrast/add$', 'ordenha.views.addContrast'),
    url(r'^contrast/view/(?P<animal_pk>\d+)$', 'ordenha.views.viewContrast'),
    url(r'^contrast/$', 'ordenha.views.viewContrast'),
    
    #pesagens
    url(r'^weighing/add/(?P<weighing_pk>\d+)/$', 'performance.views.weighing'),
    url(r'^weighing/add$', 'performance.views.weighing'),
    url(r'^animal_state/add/(?P<animal_state_pk>\d+)/$', 'performance.views.animal_state'),
    url(r'^animal_state/add$', 'performance.views.animal_state'),
    url(r'^performance/(?P<animal_pk>\d+)/$', 'performance.views.animal_performance'),
    
    #preferences
    url(r'^preferences_ordenha/$', 'preferences.views.ordenhaPref'),
"""
