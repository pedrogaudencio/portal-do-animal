/* 
 * Identifiers
 * */

/* Check for identifier availability */
function search_form(entitie_pk){
    Dajaxice.identificacao.is_identifiers_available(Dajax.process,{'form':$('#identifier_form').serialize(true),
    																'entitie_pk': entitie_pk,
    																});
}

function reserve_form(entitie_pk){
    Dajaxice.identificacao.reserve_identifiers(Dajax.process,{'form':$('#reserve_form').serialize(true),
    																'entitie_pk': entitie_pk,
    																});
}




/* Show available details */
function show_available_details(){
	$('#available_details').show("slow");
}

/* Show unavailable details */
function show_unavailable_details(){
	$('#unavailable_details').show("slow");
}

function toggleChecked(status) {
	$("#available_details input").each( function() {
		$(this).attr("checked",status);
	})
}
