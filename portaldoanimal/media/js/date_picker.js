$(function() {
		$.datepicker.setDefaults($.datepicker.regional['pt']);
		//change class - add 'btn' (bootstrap)
		$.datepicker.setDefaults($.datepicker._triggerClass = 'btn ui-datepicker-append');
		$(".datePicker").datepicker({showAnim: "fold",
										dateFormat: "yy-mm-dd",
										showOn: "button",
										buttonImage: "/media/icon_calendar.gif",
										buttonImageOnly: false,
										constrainInput: false,
										});
	
});
