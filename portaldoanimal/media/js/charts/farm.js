// Load the Visualization API and the piechart package.
google.load('visualization', '1.0', {'packages':['corechart']});


function get_charts(){
	for (i in farms){
		farm_animal_stats("chart_" + farms[i], farms[i])
	}
}

function farm_animal_stats(div_info, farm_id){  

     Dajaxice.exploracao.get_chart_species(Dajax.process, {'farm_id':farm_id, 'div_info': div_info})
}

function drawChart(d, div) {
	var data = new google.visualization.DataTable(d);
	var options = {'title':'Raças na Exploração',
            'width': 200,
            'height': 100,
            'is3D': true,
            'chartArea': {left:5, top:0, width:"100%",height:"100%"},
            };
    var chart = new google.visualization.PieChart(div);
    chart.draw(data, options);
  }