/** Association stuff
 **/


/**
 * Get form for a new membership registration
 * Make a Ajax call to server with the association and breeder id.
 **/
function get_new_membership_form(obj, association_id, breeder_id){		
	//change button css
	$(obj).css({background: '#900', 'font-weight': 'bold', color: '#FFFFFF'});
	//call to server
	Dajaxice.entidade.new_membership(Dajax.process, {'association_id':association_id, 'breeder_id': breeder_id});
	//show specie details
	$('#new_membership-'+breeder_id).first().show(500);
}


/**
 * Validate field member_number, after the user insert something on the field 
 **/
$('#id_member_number').blur(function(){
	
	//split the string with the numbers
	membership_numbers = members_numbers.split(',');
	//get the value introduced by the user
	value = $('#id_member_number').attr('value');
	
	if ($.inArray(value, membership_numbers) == 0){
		alert('Number already used.');
	}
	if (value == ''){
		alert('Field is required.');
	}
});

function print(obj){
	console.log(obj)
}