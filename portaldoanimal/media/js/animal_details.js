/* 
 * Animal details
 * */


/* *
 * Get breed details
 * Make a Ajax call to server with the specie and farm id.
 * */
function get_breed(obj, specie, farm_id){

	//if nothing displayed
	if($('#breed-'+specie+'-'+farm_id).css('display') == 'none'){
		//change button css
		$(obj).css({background: '#900', 'font-weight': 'bold', color: '#FFFFFF'});
		//call to server
		Dajaxice.exploracao.get_breed_details(Dajax.process, {'specie':specie, 'farm_id': farm_id});
		//show specie details
		$('#breed-'+specie+'-'+farm_id).first().show(500);
	}else{
		//hide details
		$('#breed-'+specie+'-'+farm_id).hide(500);
		//change button css
		$(obj).css({background: 'none', 'font-weight': 'normal', color: '#333', border: '1px solid #900'});
	}
}