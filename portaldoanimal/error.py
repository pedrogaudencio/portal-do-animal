# -*- coding: UTF-8 -*-
from django.utils.translation import ugettext as _


class Error():
    
    def __init__(self):
        self.error = _('Error: %s - Tell us about this error. We will put a herd of cows chewing the problem.') 


class IdentificationError(Error):

    def except_1001(self):
        """
        Returned multiple IdentifierType with the same name, only 1 is allowed (objects.get)
        """
        return self.error % '1001'
    
    def except_1002(self):
        """
        Returned multiple IdentifierType with the same code, only 1 is allowed (objects.get)
        """
        return self.error % '1002'


class PerformanceError(Error):
    
    def except_2001(self):
        """
        Returned multiple Status with the same status, only 1 is allowed (objects.get)
        """ 
        return self.error % '2001'
    
    def except_2002(self):
        """
        Returned multiple Performances for one animal, only 1 is allowed (objects.get)
        File: performances.performance.py
        """
        return self.error % '2002'

    def except_2003(self):
        """
        None evaluation for the animal - The performance should all ready been created
        File: performances.performance.py
        """
        return self.error % '2003'

    def except_2004(self):
        """
        Returned multiple birth reference weight for one breed, only 1 is allowed (objects.get)
        File: performances.performance.py
        Fix: Remove birth reference weight from database 
        """
        return self.error % '2004'

    def except_2005(self):
        """
        No birth reference weight for animal breed
        File: performances.performance.py
        Fix: Add birth reference weight
        """
        return self.error % '2005'

    def except_2006(self):
        """
        Error while attempting to create performance
        """
        return self.error % '2006'


class CsvError(Error):
    
    def except_3001(self):
        """
        Returned multiple animal only 1 is allowed (objects.get)
        File: utils.csv_handler.py
        Fix: Remove one animal object from database 
        """
        return self.error % '3001'

    def except_3002(self):
        """
        Returned multiple identification type, only 1 is allowed (objects.get)
        File: utils.csv_handler.py
        Fix: Remove one identification type object from database 
        """
        return self.error % '3002'

    def except_3003(self):
        """
        The identification type doesn't exist on database
        File: utils.csv_handler.py
        Fix: Add the new identification type on database 
        """
        return self.error % '3003'




ERROR = {'1001': _('Erro 1001 - Tell us about this error. We will put a herd of cows chewing the problem.')
         }