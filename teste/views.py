# Create your views here.
# -*- coding: UTF-8 -*-
from django.http import HttpResponse
from django.shortcuts import render_to_response, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from utils.sia import Sia
from utils.nif import Nif
from utils.nib import Nib

def sia(request):
    
    
    sia = 'PT023897583'
    
    context = {'sia': sia
               }

    return render_to_response('teste/sia.html', context)


def nif(request):
    
    
    nif = Nif()
    
    
    context = {'nif': nif.validate('245129286', 'Portugal')
               }

    return render_to_response('teste/nif.html', context)

def nib(request):
    
    nib = Nib()
    
    context = {'nib': nib.validate('000703250009108000756')
               }

    return render_to_response('teste/nib.html', context)
    
    
    
    
    