# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'Event'
        db.delete_table('animal_event')

    def backwards(self, orm):
        # Adding model 'Event'
        db.create_table('animal_event', (
            ('event_type', self.gf('django.db.models.fields.CharField')(max_length=15)),
            ('animal', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['animal.Animal'])),
            ('guide_date', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('obs', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('event_date', self.gf('django.db.models.fields.DateField')()),
            ('guide', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal('animal', ['Event'])

    models = {
        'animal.animal': {
            'Meta': {'object_name': 'Animal'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'adoptive_mother': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'adoptive_female_mother'", 'null': 'True', 'to': "orm['animal.Animal']"}),
            'birthdate': ('django.db.models.fields.DateField', [], {}),
            'farm': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['exploracao.Farm']"}),
            'father': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'male_father'", 'null': 'True', 'to': "orm['animal.Animal']"}),
            'first_identification': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'herd': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['exploracao.Herd']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'identification': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'animal_identification'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['red.AnimalIdentification']"}),
            'male_castrated': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'mother': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'female_mother'", 'null': 'True', 'to': "orm['animal.Animal']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'nifPI': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'obs': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'passport': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'pelage': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['animal.PelageType']", 'null': 'True', 'blank': 'True'}),
            'sex': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'sia': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'animal_identifier'", 'null': 'True', 'to': "orm['identificacao.Identifier']"}),
            'specie_breed': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['animal.SpecieBreed']"})
        },
        'animal.bird': {
            'Meta': {'object_name': 'Bird', '_ormbases': ['animal.Animal']},
            'animal_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['animal.Animal']", 'unique': 'True', 'primary_key': 'True'})
        },
        'animal.bovine': {
            'Meta': {'object_name': 'Bovine', '_ormbases': ['animal.Animal']},
            'animal_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['animal.Animal']", 'unique': 'True', 'primary_key': 'True'}),
            'milkings': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['ordenha.Milking']", 'null': 'True', 'blank': 'True'})
        },
        'animal.breedtabulatedvalue': {
            'Meta': {'object_name': 'BreedTabulatedValue', '_ormbases': ['animal.TabulatedValue']},
            'specie_breed': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['animal.SpecieBreed']"}),
            'tabulatedvalue_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['animal.TabulatedValue']", 'unique': 'True', 'primary_key': 'True'})
        },
        'animal.caprine': {
            'Meta': {'object_name': 'Caprine', '_ormbases': ['animal.Animal']},
            'animal_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['animal.Animal']", 'unique': 'True', 'primary_key': 'True'}),
            'milkings': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['ordenha.Milking']", 'null': 'True', 'blank': 'True'})
        },
        'animal.equine': {
            'Meta': {'object_name': 'Equine', '_ormbases': ['animal.Animal']},
            'animal_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['animal.Animal']", 'unique': 'True', 'primary_key': 'True'})
        },
        'animal.leporine': {
            'Meta': {'object_name': 'Leporine', '_ormbases': ['animal.Animal']},
            'animal_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['animal.Animal']", 'unique': 'True', 'primary_key': 'True'})
        },
        'animal.medicao': {
            'Meta': {'object_name': 'Medicao'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'animal.ovine': {
            'Meta': {'object_name': 'Ovine', '_ormbases': ['animal.Animal']},
            'animal_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['animal.Animal']", 'unique': 'True', 'primary_key': 'True'}),
            'milkings': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['ordenha.Milking']", 'null': 'True', 'blank': 'True'})
        },
        'animal.pelagetype': {
            'Meta': {'object_name': 'PelageType'},
            'color_en': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'color_es': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'color_fr': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'color_pt': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'name_es': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'name_fr': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'name_pt': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'specie_breed': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['animal.SpecieBreed']"})
        },
        'animal.speciebreed': {
            'Meta': {'object_name': 'SpecieBreed'},
            'breed_en': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'breed_es': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'breed_fr': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'breed_pt': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'code_en': ('django.db.models.fields.CharField', [], {'max_length': '4', 'null': 'True', 'blank': 'True'}),
            'code_es': ('django.db.models.fields.CharField', [], {'max_length': '4', 'null': 'True', 'blank': 'True'}),
            'code_fr': ('django.db.models.fields.CharField', [], {'max_length': '4', 'null': 'True', 'blank': 'True'}),
            'code_pt': ('django.db.models.fields.CharField', [], {'max_length': '4', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'specie': ('django.db.models.fields.CharField', [], {'max_length': '15'})
        },
        'animal.speciestabulatedvalues': {
            'Meta': {'object_name': 'SpeciesTabulatedValues', '_ormbases': ['animal.TabulatedValue']},
            'species_name': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'tabulatedvalue_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['animal.TabulatedValue']", 'unique': 'True', 'primary_key': 'True'})
        },
        'animal.swine': {
            'Meta': {'object_name': 'Swine', '_ormbases': ['animal.Animal']},
            'animal_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['animal.Animal']", 'unique': 'True', 'primary_key': 'True'})
        },
        'animal.tabulatedtypevalue': {
            'Meta': {'object_name': 'TabulatedTypeValue'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'name_es': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'name_fr': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'name_pt': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        'animal.tabulatedvalue': {
            'Meta': {'object_name': 'TabulatedValue'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'custom': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'farm': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['exploracao.Farm']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'sex': ('django.db.models.fields.CharField', [], {'max_length': '1', 'null': 'True', 'blank': 'True'}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'value_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['animal.TabulatedTypeValue']"})
        },
        'contactos.concelho': {
            'Meta': {'object_name': 'Concelho'},
            'concelho': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'contactos.contact': {
            'Meta': {'object_name': 'Contact'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'cellphone': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'company_number': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'company_number_extension': ('django.db.models.fields.CharField', [], {'max_length': '5', 'null': 'True', 'blank': 'True'}),
            'concelho': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contactos.Concelho']"}),
            'district': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contactos.District']"}),
            'fax': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'freguesia': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contactos.Freguesia']"}),
            'home_number': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'zip_code': ('django.db.models.fields.CharField', [], {'max_length': '10'})
        },
        'contactos.district': {
            'Meta': {'object_name': 'District'},
            'district': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'contactos.freguesia': {
            'Meta': {'object_name': 'Freguesia'},
            'freguesia': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'entidade.association': {
            'Meta': {'object_name': 'Association', '_ormbases': ['entidade.Entities']},
            'breed': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['animal.SpecieBreed']", 'symmetrical': 'False'}),
            'entities_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['entidade.Entities']", 'unique': 'True', 'primary_key': 'True'}),
            'members': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'association_members'", 'blank': 'True', 'to': "orm['entidade.BreederMembership']"}),
            'tec': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'})
        },
        'entidade.breeder': {
            'Meta': {'object_name': 'Breeder', '_ormbases': ['entidade.Entities']},
            'entities_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['entidade.Entities']", 'unique': 'True', 'primary_key': 'True'}),
            'ifap': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'})
        },
        'entidade.breedermembership': {
            'Meta': {'object_name': 'BreederMembership'},
            'association': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['entidade.Association']"}),
            'breeder': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['entidade.Breeder']"}),
            'date_entry': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'date_leave': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'member_number': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'reason_to_leave': ('django.db.models.fields.TextField', [], {'max_length': '250', 'null': 'True', 'blank': 'True'})
        },
        'entidade.entities': {
            'Meta': {'object_name': 'Entities'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'contact': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['contactos.Contact']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'nib': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'nif': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'obs': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'})
        },
        'exploracao.farm': {
            'Meta': {'object_name': 'Farm'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'applied_to': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'applied'", 'null': 'True', 'to': "orm['entidade.Association']"}),
            'breeder': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['entidade.Breeder']"}),
            'close_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'contact': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contactos.Contact']", 'null': 'True', 'blank': 'True'}),
            'geolocation': ('django_google_maps.fields.GeoLocationField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'health_status': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['exploracao.HealthStatus']", 'null': 'True', 'blank': 'True'}),
            'herds': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'farm_herd'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['exploracao.Herd']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'managed_by': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'managed'", 'null': 'True', 'to': "orm['entidade.Association']"}),
            'manager': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'map_address': ('django_google_maps.fields.AddressField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'obs': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'production': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['exploracao.Production']", 'symmetrical': 'False', 'blank': 'True'}),
            'register_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'register_number': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '150'}),
            'system': ('django.db.models.fields.CharField', [], {'default': "'extensive'", 'max_length': '20'}),
            'trademark': ('django.db.models.fields.CharField', [], {'max_length': '150', 'unique': 'True', 'null': 'True', 'blank': 'True'})
        },
        'exploracao.healthstatus': {
            'Meta': {'object_name': 'HealthStatus'},
            'acronym_en': ('django.db.models.fields.CharField', [], {'max_length': '15', 'null': 'True', 'blank': 'True'}),
            'acronym_es': ('django.db.models.fields.CharField', [], {'max_length': '15', 'null': 'True', 'blank': 'True'}),
            'acronym_fr': ('django.db.models.fields.CharField', [], {'max_length': '15', 'null': 'True', 'blank': 'True'}),
            'acronym_pt': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'animal': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['animal.SpecieBreed']", 'symmetrical': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'law_en': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'law_es': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'law_fr': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'law_pt': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'start_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'status_en': ('django.db.models.fields.CharField', [], {'max_length': '250', 'null': 'True', 'blank': 'True'}),
            'status_es': ('django.db.models.fields.CharField', [], {'max_length': '250', 'null': 'True', 'blank': 'True'}),
            'status_fr': ('django.db.models.fields.CharField', [], {'max_length': '250', 'null': 'True', 'blank': 'True'}),
            'status_pt': ('django.db.models.fields.CharField', [], {'max_length': '250'})
        },
        'exploracao.herd': {
            'Meta': {'object_name': 'Herd'},
            'farm': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'herd_farm'", 'to': "orm['exploracao.Farm']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'obs': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'})
        },
        'exploracao.production': {
            'Meta': {'object_name': 'Production'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'name_es': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'name_fr': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'name_pt': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        },
        'identificacao.identifier': {
            'Meta': {'unique_together': "(('number', 'id_type'),)", 'object_name': 'Identifier'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'current_breeder': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['entidade.Breeder']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'id_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['identificacao.IdentifierType']"}),
            'inactivation_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'number': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        'identificacao.identifiertype': {
            'Meta': {'object_name': 'IdentifierType'},
            'code_en': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'code_es': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'code_fr': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'code_pt': ('django.db.models.fields.IntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'name_es': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'name_fr': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'name_pt': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'obs_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'obs_es': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'obs_fr': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'obs_pt': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'})
        },
        'ordenha.lactation': {
            'Meta': {'object_name': 'Lactation'},
            'animal': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['animal.Animal']"}),
            'auto_dry': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'dry_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'farm': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['exploracao.Farm']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lact_number': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0', 'null': 'True', 'blank': 'True'}),
            'obs': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'start_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'})
        },
        'ordenha.milking': {
            'Meta': {'object_name': 'Milking'},
            'animal': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['animal.Animal']"}),
            'employee': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['utilizador.Staff']", 'null': 'True', 'blank': 'True'}),
            'farm': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['exploracao.Farm']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lactation': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['ordenha.Lactation']"}),
            'milking_date': ('django.db.models.fields.DateField', [], {}),
            'milking_hour': ('django.db.models.fields.TimeField', [], {'null': 'True', 'blank': 'True'}),
            'milking_number': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0', 'blank': 'True'}),
            'milking_start_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'milking_time': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['ordenha.MilkingTime']"}),
            'obs': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'quantity': ('django.db.models.fields.CharField', [], {'max_length': '6', 'null': 'True', 'blank': 'True'}),
            'tank': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['ordenha.Storage']", 'null': 'True', 'blank': 'True'})
        },
        'ordenha.milkingtime': {
            'Meta': {'object_name': 'MilkingTime'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'end_time': ('django.db.models.fields.TimeField', [], {'null': 'True', 'blank': 'True'}),
            'farm': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['exploracao.Farm']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'obs': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'start_time': ('django.db.models.fields.TimeField', [], {'null': 'True', 'blank': 'True'})
        },
        'ordenha.storage': {
            'Meta': {'unique_together': "(('farm', 'name', 'local'),)", 'object_name': 'Storage'},
            'farm': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['exploracao.Farm']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'local': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'max_load': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'obs': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50', 'blank': 'True'})
        },
        'red.animalidentification': {
            'Meta': {'ordering': "['-timestamp']", 'object_name': 'AnimalIdentification', '_ormbases': ['red.Event']},
            'animal': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'animal_id'", 'to': "orm['animal.Animal']"}),
            'employee': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'colaborador_id'", 'null': 'True', 'to': "orm['utilizador.Staff']"}),
            'entity_nif': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'event_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['red.Event']", 'unique': 'True', 'primary_key': 'True'}),
            'identifier': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'identifier_animal'", 'to': "orm['identificacao.Identifier']"})
        },
        'red.event': {
            'Meta': {'ordering': "['-timestamp']", 'object_name': 'Event'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {})
        },
        'utilizador.staff': {
            'Meta': {'unique_together': "(('identification', 'entitie'),)", 'object_name': 'Staff'},
            'contact': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contactos.Contact']"}),
            'entitie': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['entidade.Entities']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'identification': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'nif': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'post': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '15', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['animal']