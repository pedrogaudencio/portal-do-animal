# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Animal'
        db.create_table('animal_animal', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('farm', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['exploracao.Farm'])),
            ('herd', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['exploracao.Herd'], null=True, blank=True)),
            ('father', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='male_father', null=True, to=orm['animal.Animal'])),
            ('mother', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='female_mother', null=True, to=orm['animal.Animal'])),
            ('adoptive_mother', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='adoptive_female_mother', null=True, to=orm['animal.Animal'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('birthdate', self.gf('django.db.models.fields.DateField')()),
            ('nifPI', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('first_identification', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('sia', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='animal_identifier', null=True, to=orm['identificacao.Identifier'])),
            ('male_castrated', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('sex', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('passport', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('specie_breed', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['animal.SpecieBreed'])),
            ('pelage', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['animal.PelageType'], null=True, blank=True)),
            ('obs', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal('animal', ['Animal'])

        # Adding M2M table for field identification on 'Animal'
        db.create_table('animal_animal_identification', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('animal', models.ForeignKey(orm['animal.animal'], null=False)),
            ('animalidentification', models.ForeignKey(orm['identificacao.animalidentification'], null=False))
        ))
        db.create_unique('animal_animal_identification', ['animal_id', 'animalidentification_id'])

        # Adding model 'Bird'
        db.create_table('animal_bird', (
            ('animal_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['animal.Animal'], unique=True, primary_key=True)),
        ))
        db.send_create_signal('animal', ['Bird'])

        # Adding model 'Bovine'
        db.create_table('animal_bovine', (
            ('animal_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['animal.Animal'], unique=True, primary_key=True)),
        ))
        db.send_create_signal('animal', ['Bovine'])

        # Adding M2M table for field milkings on 'Bovine'
        db.create_table('animal_bovine_milkings', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('bovine', models.ForeignKey(orm['animal.bovine'], null=False)),
            ('milking', models.ForeignKey(orm['ordenha.milking'], null=False))
        ))
        db.create_unique('animal_bovine_milkings', ['bovine_id', 'milking_id'])

        # Adding model 'Caprine'
        db.create_table('animal_caprine', (
            ('animal_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['animal.Animal'], unique=True, primary_key=True)),
        ))
        db.send_create_signal('animal', ['Caprine'])

        # Adding M2M table for field milkings on 'Caprine'
        db.create_table('animal_caprine_milkings', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('caprine', models.ForeignKey(orm['animal.caprine'], null=False)),
            ('milking', models.ForeignKey(orm['ordenha.milking'], null=False))
        ))
        db.create_unique('animal_caprine_milkings', ['caprine_id', 'milking_id'])

        # Adding model 'Equine'
        db.create_table('animal_equine', (
            ('animal_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['animal.Animal'], unique=True, primary_key=True)),
        ))
        db.send_create_signal('animal', ['Equine'])

        # Adding model 'Leporine'
        db.create_table('animal_leporine', (
            ('animal_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['animal.Animal'], unique=True, primary_key=True)),
        ))
        db.send_create_signal('animal', ['Leporine'])

        # Adding model 'Ovine'
        db.create_table('animal_ovine', (
            ('animal_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['animal.Animal'], unique=True, primary_key=True)),
        ))
        db.send_create_signal('animal', ['Ovine'])

        # Adding M2M table for field milkings on 'Ovine'
        db.create_table('animal_ovine_milkings', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('ovine', models.ForeignKey(orm['animal.ovine'], null=False)),
            ('milking', models.ForeignKey(orm['ordenha.milking'], null=False))
        ))
        db.create_unique('animal_ovine_milkings', ['ovine_id', 'milking_id'])

        # Adding model 'Swine'
        db.create_table('animal_swine', (
            ('animal_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['animal.Animal'], unique=True, primary_key=True)),
        ))
        db.send_create_signal('animal', ['Swine'])

        # Adding model 'SpecieBreed'
        db.create_table('animal_speciebreed', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('specie', self.gf('django.db.models.fields.CharField')(max_length=15)),
            ('breed_pt', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('breed_en', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('breed_es', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('breed_fr', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('cod_pt', self.gf('django.db.models.fields.CharField')(max_length=4, null=True)),
            ('cod_en', self.gf('django.db.models.fields.CharField')(max_length=4, null=True, blank=True)),
            ('cod_es', self.gf('django.db.models.fields.CharField')(max_length=4, null=True, blank=True)),
            ('cod_fr', self.gf('django.db.models.fields.CharField')(max_length=4, null=True, blank=True)),
        ))
        db.send_create_signal('animal', ['SpecieBreed'])

        # Adding model 'PelageType'
        db.create_table('animal_pelagetype', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name_pt', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('name_es', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('name_fr', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('color_pt', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('color_en', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('color_es', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('color_fr', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('specie_breed', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['animal.SpecieBreed'])),
        ))
        db.send_create_signal('animal', ['PelageType'])

        # Adding model 'TabulatedTypeValue'
        db.create_table('animal_tabulatedtypevalue', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name_pt', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=30, null=True, blank=True)),
            ('name_es', self.gf('django.db.models.fields.CharField')(max_length=30, null=True, blank=True)),
            ('name_fr', self.gf('django.db.models.fields.CharField')(max_length=30, null=True, blank=True)),
        ))
        db.send_create_signal('animal', ['TabulatedTypeValue'])

        # Adding model 'TabulatedValue'
        db.create_table('animal_tabulatedvalue', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('value_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['animal.TabulatedTypeValue'])),
            ('value', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('sex', self.gf('django.db.models.fields.CharField')(max_length=1, null=True, blank=True)),
            ('custom', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('farm', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['exploracao.Farm'], null=True, blank=True)),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal('animal', ['TabulatedValue'])

        # Adding model 'SpecieBreedTabulatedValue'
        db.create_table('animal_speciebreedtabulatedvalue', (
            ('tabulatedvalue_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['animal.TabulatedValue'], unique=True, primary_key=True)),
            ('specie_breed', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['animal.SpecieBreed'])),
        ))
        db.send_create_signal('animal', ['SpecieBreedTabulatedValue'])

        # Adding model 'SpeciesTabulatedValues'
        db.create_table('animal_speciestabulatedvalues', (
            ('tabulatedvalue_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['animal.TabulatedValue'], unique=True, primary_key=True)),
            ('species_name', self.gf('django.db.models.fields.CharField')(max_length=15)),
        ))
        db.send_create_signal('animal', ['SpeciesTabulatedValues'])

        # Adding model 'Event'
        db.create_table('animal_event', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('animal', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['animal.Animal'])),
            ('event_type', self.gf('django.db.models.fields.CharField')(max_length=15)),
            ('event_date', self.gf('django.db.models.fields.DateField')()),
            ('guide_date', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('guide', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('obs', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal('animal', ['Event'])

        # Adding model 'Medicao'
        db.create_table('animal_medicao', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal('animal', ['Medicao'])

    def backwards(self, orm):
        # Deleting model 'Animal'
        db.delete_table('animal_animal')

        # Removing M2M table for field identification on 'Animal'
        db.delete_table('animal_animal_identification')

        # Deleting model 'Bird'
        db.delete_table('animal_bird')

        # Deleting model 'Bovine'
        db.delete_table('animal_bovine')

        # Removing M2M table for field milkings on 'Bovine'
        db.delete_table('animal_bovine_milkings')

        # Deleting model 'Caprine'
        db.delete_table('animal_caprine')

        # Removing M2M table for field milkings on 'Caprine'
        db.delete_table('animal_caprine_milkings')

        # Deleting model 'Equine'
        db.delete_table('animal_equine')

        # Deleting model 'Leporine'
        db.delete_table('animal_leporine')

        # Deleting model 'Ovine'
        db.delete_table('animal_ovine')

        # Removing M2M table for field milkings on 'Ovine'
        db.delete_table('animal_ovine_milkings')

        # Deleting model 'Swine'
        db.delete_table('animal_swine')

        # Deleting model 'SpecieBreed'
        db.delete_table('animal_speciebreed')

        # Deleting model 'PelageType'
        db.delete_table('animal_pelagetype')

        # Deleting model 'TabulatedTypeValue'
        db.delete_table('animal_tabulatedtypevalue')

        # Deleting model 'TabulatedValue'
        db.delete_table('animal_tabulatedvalue')

        # Deleting model 'SpecieBreedTabulatedValue'
        db.delete_table('animal_speciebreedtabulatedvalue')

        # Deleting model 'SpeciesTabulatedValues'
        db.delete_table('animal_speciestabulatedvalues')

        # Deleting model 'Event'
        db.delete_table('animal_event')

        # Deleting model 'Medicao'
        db.delete_table('animal_medicao')

    models = {
        'animal.animal': {
            'Meta': {'object_name': 'Animal'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'adoptive_mother': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'adoptive_female_mother'", 'null': 'True', 'to': "orm['animal.Animal']"}),
            'birthdate': ('django.db.models.fields.DateField', [], {}),
            'farm': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['exploracao.Farm']"}),
            'father': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'male_father'", 'null': 'True', 'to': "orm['animal.Animal']"}),
            'first_identification': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'herd': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['exploracao.Herd']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'identification': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'animal_identification'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['identificacao.AnimalIdentification']"}),
            'male_castrated': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'mother': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'female_mother'", 'null': 'True', 'to': "orm['animal.Animal']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'nifPI': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'obs': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'passport': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'pelage': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['animal.PelageType']", 'null': 'True', 'blank': 'True'}),
            'sex': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'sia': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'animal_identifier'", 'null': 'True', 'to': "orm['identificacao.Identifier']"}),
            'specie_breed': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['animal.SpecieBreed']"})
        },
        'animal.bird': {
            'Meta': {'object_name': 'Bird', '_ormbases': ['animal.Animal']},
            'animal_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['animal.Animal']", 'unique': 'True', 'primary_key': 'True'})
        },
        'animal.bovine': {
            'Meta': {'object_name': 'Bovine', '_ormbases': ['animal.Animal']},
            'animal_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['animal.Animal']", 'unique': 'True', 'primary_key': 'True'}),
            'milkings': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['ordenha.Milking']", 'null': 'True', 'blank': 'True'})
        },
        'animal.caprine': {
            'Meta': {'object_name': 'Caprine', '_ormbases': ['animal.Animal']},
            'animal_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['animal.Animal']", 'unique': 'True', 'primary_key': 'True'}),
            'milkings': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['ordenha.Milking']", 'null': 'True', 'blank': 'True'})
        },
        'animal.equine': {
            'Meta': {'object_name': 'Equine', '_ormbases': ['animal.Animal']},
            'animal_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['animal.Animal']", 'unique': 'True', 'primary_key': 'True'})
        },
        'animal.event': {
            'Meta': {'object_name': 'Event'},
            'animal': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['animal.Animal']"}),
            'event_date': ('django.db.models.fields.DateField', [], {}),
            'event_type': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'guide': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'guide_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'obs': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'})
        },
        'animal.leporine': {
            'Meta': {'object_name': 'Leporine', '_ormbases': ['animal.Animal']},
            'animal_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['animal.Animal']", 'unique': 'True', 'primary_key': 'True'})
        },
        'animal.medicao': {
            'Meta': {'object_name': 'Medicao'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'animal.ovine': {
            'Meta': {'object_name': 'Ovine', '_ormbases': ['animal.Animal']},
            'animal_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['animal.Animal']", 'unique': 'True', 'primary_key': 'True'}),
            'milkings': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['ordenha.Milking']", 'null': 'True', 'blank': 'True'})
        },
        'animal.pelagetype': {
            'Meta': {'object_name': 'PelageType'},
            'color_en': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'color_es': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'color_fr': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'color_pt': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'name_es': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'name_fr': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'name_pt': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'specie_breed': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['animal.SpecieBreed']"})
        },
        'animal.speciebreed': {
            'Meta': {'object_name': 'SpecieBreed'},
            'breed_en': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'breed_es': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'breed_fr': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'breed_pt': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'cod_en': ('django.db.models.fields.CharField', [], {'max_length': '4', 'null': 'True', 'blank': 'True'}),
            'cod_es': ('django.db.models.fields.CharField', [], {'max_length': '4', 'null': 'True', 'blank': 'True'}),
            'cod_fr': ('django.db.models.fields.CharField', [], {'max_length': '4', 'null': 'True', 'blank': 'True'}),
            'cod_pt': ('django.db.models.fields.CharField', [], {'max_length': '4', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'specie': ('django.db.models.fields.CharField', [], {'max_length': '15'})
        },
        'animal.speciebreedtabulatedvalue': {
            'Meta': {'object_name': 'SpecieBreedTabulatedValue', '_ormbases': ['animal.TabulatedValue']},
            'specie_breed': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['animal.SpecieBreed']"}),
            'tabulatedvalue_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['animal.TabulatedValue']", 'unique': 'True', 'primary_key': 'True'})
        },
        'animal.speciestabulatedvalues': {
            'Meta': {'object_name': 'SpeciesTabulatedValues', '_ormbases': ['animal.TabulatedValue']},
            'species_name': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'tabulatedvalue_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['animal.TabulatedValue']", 'unique': 'True', 'primary_key': 'True'})
        },
        'animal.swine': {
            'Meta': {'object_name': 'Swine', '_ormbases': ['animal.Animal']},
            'animal_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['animal.Animal']", 'unique': 'True', 'primary_key': 'True'})
        },
        'animal.tabulatedtypevalue': {
            'Meta': {'object_name': 'TabulatedTypeValue'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'name_es': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'name_fr': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'name_pt': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        'animal.tabulatedvalue': {
            'Meta': {'object_name': 'TabulatedValue'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'custom': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'farm': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['exploracao.Farm']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'sex': ('django.db.models.fields.CharField', [], {'max_length': '1', 'null': 'True', 'blank': 'True'}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'value_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['animal.TabulatedTypeValue']"})
        },
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contactos.concelho': {
            'Meta': {'object_name': 'Concelho'},
            'concelho': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'contactos.contact': {
            'Meta': {'object_name': 'Contact'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'cellphone': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'company_number': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'company_number_extension': ('django.db.models.fields.CharField', [], {'max_length': '5', 'null': 'True', 'blank': 'True'}),
            'concelho': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contactos.Concelho']"}),
            'district': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contactos.District']"}),
            'fax': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'freguesia': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contactos.Freguesia']"}),
            'home_number': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'zip_code': ('django.db.models.fields.CharField', [], {'max_length': '10'})
        },
        'contactos.district': {
            'Meta': {'object_name': 'District'},
            'district': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'contactos.freguesia': {
            'Meta': {'object_name': 'Freguesia'},
            'freguesia': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'entidade.breeder': {
            'Meta': {'object_name': 'Breeder', '_ormbases': ['entidade.Entities']},
            'entities_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['entidade.Entities']", 'unique': 'True', 'primary_key': 'True'}),
            'ifap': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'})
        },
        'entidade.entities': {
            'Meta': {'object_name': 'Entities'},
            'contact': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['contactos.Contact']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'nib': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'nif': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'obs': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'})
        },
        'exploracao.farm': {
            'Meta': {'object_name': 'Farm'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'breeder': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['entidade.Breeder']"}),
            'close_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'contact': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['contactos.Contact']", 'null': 'True', 'blank': 'True'}),
            'geolocation': ('django_google_maps.fields.GeoLocationField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'health_status': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['exploracao.HealthStatus']", 'null': 'True', 'blank': 'True'}),
            'herds': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'farm_herd'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['exploracao.Herd']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'manager': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'map_address': ('django_google_maps.fields.AddressField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'obs': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'production': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['exploracao.Production']", 'symmetrical': 'False', 'blank': 'True'}),
            'red': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['red.RED']", 'null': 'True', 'blank': 'True'}),
            'register_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'register_number': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'system': ('django.db.models.fields.CharField', [], {'default': "'extensive'", 'max_length': '20'}),
            'trademark': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'})
        },
        'exploracao.healthstatus': {
            'Meta': {'object_name': 'HealthStatus'},
            'acronym_en': ('django.db.models.fields.CharField', [], {'max_length': '15', 'null': 'True', 'blank': 'True'}),
            'acronym_es': ('django.db.models.fields.CharField', [], {'max_length': '15', 'null': 'True', 'blank': 'True'}),
            'acronym_fr': ('django.db.models.fields.CharField', [], {'max_length': '15', 'null': 'True', 'blank': 'True'}),
            'acronym_pt': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'animal': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['animal.SpecieBreed']", 'symmetrical': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'law_en': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'law_es': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'law_fr': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'law_pt': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'start_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'status_en': ('django.db.models.fields.CharField', [], {'max_length': '250', 'null': 'True', 'blank': 'True'}),
            'status_es': ('django.db.models.fields.CharField', [], {'max_length': '250', 'null': 'True', 'blank': 'True'}),
            'status_fr': ('django.db.models.fields.CharField', [], {'max_length': '250', 'null': 'True', 'blank': 'True'}),
            'status_pt': ('django.db.models.fields.CharField', [], {'max_length': '250'})
        },
        'exploracao.herd': {
            'Meta': {'object_name': 'Herd'},
            'farm': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'herd_farm'", 'to': "orm['exploracao.Farm']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'obs': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'})
        },
        'exploracao.production': {
            'Meta': {'object_name': 'Production'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'name_es': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'name_fr': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'name_pt': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        },
        'identificacao.animalidentification': {
            'Meta': {'object_name': 'AnimalIdentification'},
            'animal': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'animal_id'", 'to': "orm['animal.Animal']"}),
            'date': ('django.db.models.fields.DateField', [], {}),
            'employee': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'colaborador_id'", 'null': 'True', 'to': "orm['utilizador.Staff']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'identifier': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'identifier_animal'", 'to': "orm['identificacao.Identifier']"})
        },
        'identificacao.identifier': {
            'Meta': {'object_name': 'Identifier'},
            'entity': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'identifier_entity'", 'null': 'True', 'to': "orm['entidade.Entities']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'id_type': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'type_dentifier'", 'to': "orm['identificacao.IdentifierType']"}),
            'number': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'identificacao.identifiertype': {
            'Meta': {'object_name': 'IdentifierType'},
            'code_en': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'code_es': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'code_fr': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'code_pt': ('django.db.models.fields.IntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'name_es': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'name_fr': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'name_pt': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'obs_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'obs_es': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'obs_fr': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'obs_pt': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'})
        },
        'ordenha.lactation': {
            'Meta': {'object_name': 'Lactation'},
            'animal': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['animal.Animal']"}),
            'auto_dry': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'dry_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'farm': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['exploracao.Farm']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lact_number': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0', 'null': 'True', 'blank': 'True'}),
            'obs': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'start_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'})
        },
        'ordenha.milking': {
            'Meta': {'object_name': 'Milking'},
            'animal': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['animal.Animal']"}),
            'employee': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['utilizador.Staff']", 'null': 'True', 'blank': 'True'}),
            'farm': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['exploracao.Farm']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lactation': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['ordenha.Lactation']"}),
            'milking_date': ('django.db.models.fields.DateField', [], {}),
            'milking_hour': ('django.db.models.fields.TimeField', [], {'null': 'True', 'blank': 'True'}),
            'milking_number': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0', 'blank': 'True'}),
            'milking_start_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'milking_time': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['ordenha.MilkingTime']"}),
            'obs': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'quantity': ('django.db.models.fields.CharField', [], {'max_length': '6', 'null': 'True', 'blank': 'True'}),
            'tank': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['ordenha.Storage']", 'null': 'True', 'blank': 'True'})
        },
        'ordenha.milkingtime': {
            'Meta': {'object_name': 'MilkingTime'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'end_time': ('django.db.models.fields.TimeField', [], {'null': 'True', 'blank': 'True'}),
            'farm': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['exploracao.Farm']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'obs': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'start_time': ('django.db.models.fields.TimeField', [], {'null': 'True', 'blank': 'True'})
        },
        'ordenha.storage': {
            'Meta': {'object_name': 'Storage'},
            'farm': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['exploracao.Farm']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'local': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'max_load': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'obs': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'})
        },
        'red.red': {
            'Meta': {'object_name': 'RED'},
            'authorization_number': ('django.db.models.fields.PositiveIntegerField', [], {'max_length': '20'}),
            'book_appendix': ('django.db.models.fields.PositiveIntegerField', [], {'max_length': '20'}),
            'book_number': ('django.db.models.fields.PositiveIntegerField', [], {'max_length': '20'}),
            'entitie': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['entidade.Entities']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'issuer': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'obs': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'year': ('django.db.models.fields.IntegerField', [], {})
        },
        'utilizador.staff': {
            'Meta': {'object_name': 'Staff'},
            'breeder': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['entidade.Breeder']", 'null': 'True'}),
            'contact': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contactos.Contact']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'identification': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '250'}),
            'nif': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'post': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'user_profile': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['utilizador.UserProfile']", 'null': 'True', 'blank': 'True'})
        },
        'utilizador.userprofile': {
            'Meta': {'object_name': 'UserProfile'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_account_owner': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['auth.User']", 'unique': 'True'}),
            'user_type': ('django.db.models.fields.CharField', [], {'max_length': '1'})
        }
    }

    complete_apps = ['animal']