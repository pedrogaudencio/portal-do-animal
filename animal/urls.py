# -*- coding: UTF-8 -*-
from django.conf.urls.defaults import *
from django.views.generic import DetailView, ListView
from animal.models import Animal
from django.views.generic.base import TemplateView

urlpatterns = patterns('',
    (r'^$',
        ListView.as_view(
            queryset=Animal.objects.order_by('id')[:10],
            context_object_name='latest_animal_list',
            template_name='animal/index.html')),
    (r'^(?P<pk>\d+)/$',
        'animal.views.detalhes'),
    (r'^adicionar/$',
        'animal.views.adicionar_ave'),
    (r'^$',
        TemplateView.as_view(
            template_name='animal/done.html')),
)

'''
    (r'^modificar/$',
        'animal.views.modificar'),
    (r'^remover/$',
        'animal.views.remover'),
'''