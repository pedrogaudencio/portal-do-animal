# -*- coding: UTF-8 -*-
from django.contrib import admin
from animal.models import SpecieBreed, PelageType, Bird, Bovine, Caprine, Equine, Leporine, Ovine, Swine, TabulatedTypeValue, BreedTabulatedValue, SpeciesTabulatedValues

'''
class AnimalAdmin(admin.ModelAdmin):
    fieldsets = [
                 (None, {
                         'classes': ['wide', 'extrapretty'],
                         'fields': (
                                    ('grupo'),
                                    ('nome','sex'),
                                    ('sia','passaporte'),
                                    ('numero_pai','numero_mae'),
                                    )
                         }),
    ]
    #list_display = ('__unicode__','exploracao', 'raca', 'sex')
    #list_display_links = ('__unicode__','exploracao')
    list_filter = ['sex']
    search_fields = ['nome']
    #save_as = True #??
'''


class BirdAdmin(admin.ModelAdmin):
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "father":
            kwargs["queryset"] = Bird.objects.filter(sex='M')
        if db_field.name in ['mother', 'adoptive_mother']:
            kwargs["queryset"] = Bird.objects.filter(sex='F')
        if db_field.name == "specie_breed":
            kwargs["queryset"] = SpecieBreed.objects.filter(specie ='Bovine')
        if db_field.name == "pelage":
            kwargs["queryset"] = PelageType.objects.filter(specie_breed__specie = 'Bird')
        return super(BirdAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)


class BovineAdmin(admin.ModelAdmin):
    list_display = ('__unicode__','farm')
    list_filter = ['farm','sex']
    search_fields = ['sia']
    
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "father":
            kwargs["queryset"] = Bovine.objects.filter(sex='M')
        if db_field.name in ['mother', 'adoptive_mother']:
            kwargs["queryset"] = Bovine.objects.filter(sex='F')
        if db_field.name == "specie_breed":
            kwargs["queryset"] = SpecieBreed.objects.filter(specie ='Bovine')
        if db_field.name == "pelage":
            kwargs["queryset"] = PelageType.objects.filter(specie_breed__specie ='Bovine')
        return super(BovineAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

    #exclude = ('milkings',)


class CaprineAdmin(admin.ModelAdmin):
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "father":
            kwargs["queryset"] = Caprine.objects.filter(sex='M')
        if db_field.name in ['mother', 'adoptive_mother']:
            kwargs["queryset"] = Caprine.objects.filter(sex='F')
        if db_field.name == "specie_breed":
            kwargs["queryset"] = SpecieBreed.objects.filter(specie ='Caprine')
        if db_field.name == "pelage":
            kwargs["queryset"] = PelageType.objects.filter(specie_breed__specie ='Caprine')
        return super(CaprineAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

    #exclude = ('milkings',)


class EquineAdmin(admin.ModelAdmin):
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "father":
            kwargs["queryset"] = Equine.objects.filter(sex='M')
        if db_field.name in ['mother', 'adoptive_mother']:
            kwargs["queryset"] = Equine.objects.filter(sex='F')
        if db_field.name == "specie_breed":
            kwargs["queryset"] = SpecieBreed.objects.filter(specie ='Equine')
        if db_field.name == "pelage":
            kwargs["queryset"] = PelageType.objects.filter(specie_breed__specie ='Equine')
        return super(EquineAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)


class LeporineAdmin(admin.ModelAdmin):
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "father":
            kwargs["queryset"] = Equine.objects.filter(sex='M')
        if db_field.name in ['mother', 'adoptive_mother']:
            kwargs["queryset"] = Equine.objects.filter(sex='F')
        if db_field.name == "specie_breed":
            kwargs["queryset"] = SpecieBreed.objects.filter(specie ='Leporine')
        if db_field.name == "pelage":
            kwargs["queryset"] = PelageType.objects.filter(specie_breed__specie ='Leporine')
        return super(LeporineAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)


class OvineAdmin(admin.ModelAdmin):
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "father":
            kwargs["queryset"] = Equine.objects.filter(sex='M')
        if db_field.name in ['mother', 'adoptive_mother']:
            kwargs["queryset"] = Equine.objects.filter(sex='F')
        if db_field.name == "specie_breed":
            kwargs["queryset"] = SpecieBreed.objects.filter(specie ='Ovine')
        if db_field.name == "pelage":
            kwargs["queryset"] = PelageType.objects.filter(specie_breed__specie ='Ovine')
        return super(OvineAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

    #exclude = ('milkings',)


class SwineAdmin(admin.ModelAdmin):
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "father":
            kwargs["queryset"] = Equine.objects.filter(sex='M')
        if db_field.name in ['mother', 'adoptive_mother']:
            kwargs["queryset"] = Equine.objects.filter(sex='F')
        if db_field.name == "specie_breed":
            kwargs["queryset"] = SpecieBreed.objects.filter(specie ='Swine')
        if db_field.name == "pelage":
            kwargs["queryset"] = PelageType.objects.filter(specie_breed__specie ='Swine')
        return super(SwineAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)


class SpecieBreedAdmin(admin.ModelAdmin):
#    field = ('especie','nome_especie')
    list_display = ('specie','breed','code')
    list_filter = ['specie']
    search_fields = ['breed']


class PelageTypeAdmin(admin.ModelAdmin):
    list_display = ('name','color','specie_breed')
    list_filter = ['specie_breed']
    search_fields = ['name']


class BreedTabulatedValueAdmin(admin.ModelAdmin):
    list_display = ('value_type', 'value', 'specie_breed', 'sex')
    list_filter = ['specie_breed']
    search_fields = ['name']


#admin.site.register(Animal, AnimalAdmin)
admin.site.register(Bird, BirdAdmin)
admin.site.register(Bovine, BovineAdmin)
admin.site.register(Caprine, CaprineAdmin)
admin.site.register(Equine, EquineAdmin)
admin.site.register(Leporine, LeporineAdmin)
admin.site.register(Ovine, OvineAdmin)
admin.site.register(Swine, SwineAdmin)
admin.site.register(SpecieBreed, SpecieBreedAdmin)
admin.site.register(PelageType, PelageTypeAdmin)
admin.site.register(TabulatedTypeValue)
admin.site.register(BreedTabulatedValue, BreedTabulatedValueAdmin)
admin.site.register(SpeciesTabulatedValues)
