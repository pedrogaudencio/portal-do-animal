# -*- coding: UTF-8 -*-
from django.db import models
from django.utils.translation import ugettext as _
from transmeta import TransMeta
from django.utils.encoding import force_unicode


class AnimalActive(models.Manager):
    """
    Filter by active animals
    """
    def get_query_set(self):
        return super(AnimalActive, self).get_query_set().filter(active = True)
    

class Animal(models.Model):
    #data_de_registo = models.DateField(auto_now=True, auto_add_now=True)
    farm = models.ForeignKey('exploracao.Farm', blank = False, null = False)
    herd = models.ForeignKey('exploracao.Herd', verbose_name=_('Herd'), 
                             blank = True, null = True) #grupo
    father = models.ForeignKey('Animal', related_name = 'male_father', 
                               verbose_name=_('Father (#identification)'), 
                               blank=True, null=True)
    mother = models.ForeignKey('Animal', related_name = 'female_mother', 
                               verbose_name=_('Mother (#identification'), 
                               blank=True, null=True)
    adoptive_mother = models.ForeignKey('Animal', related_name = 'adoptive_female_mother', 
                                        verbose_name=_('Adoptive Mother (#identification)'), 
                                        blank=True, null=True)
    name = models.CharField(_('Name'), max_length=20, blank=True, null = True)
    birthdate = models.DateField(_('Birthdate'), blank=False, null = False)
    nifPI = models.IntegerField(_('Identificator NIF'),
                                help_text= _('NIF of first indentificator breeder'), 
                                blank = True, null = True) #NIF do criador que o primeiro o identificou
    first_identification = models.DateField(verbose_name = _('First identification date'), 
                                            blank = True, null = True) #data da primeira identificação
    #pelagem_choices=(('1','Pelagem 1'),('2','Pelagem 2'))
    #pelagem=models.CharField(max_length=10, choices=pelagem_choices, blank=False)
    sia = models.ForeignKey('identificacao.Identifier', related_name='animal_identifier', 
                            blank = True, null = True)
    identification = models.ManyToManyField('red.AnimalIdentification', 
                                            related_name='animal_identification', blank = True, null = True)
    male_castrated = models.BooleanField(_('Castrated Male'), default = False)
    #sia = models.PositiveIntegerField('Número de SIA', max_length=15, blank=True, null=True)
    sex_choices=(('F', _('Female')),('M', _('Male')))
    sex = models.CharField(_('Sex'), max_length=1, choices=sex_choices, blank=False)
    passport = models.PositiveIntegerField(_('Passport number'), blank=True, null=True)
    specie_breed = models.ForeignKey('SpecieBreed', verbose_name = _('Specie - Breed'))
    pelage = models.ForeignKey('PelageType', verbose_name = _('Pelage'), blank = True, null = True)
    #lg = models.ForeignKey('LivroGeneologico')
    obs = models.TextField(_('Observations'), blank=True, null=True)

    active = models.BooleanField(_('Active'), default = True)
    
    #managers
    objects = models.Manager() # The default manager
    actives = AnimalActive() # Animal active


    class Meta():
        verbose_name = _('Animal')
        verbose_name_plural= _('Animals')
    
    
    def __unicode__(self):
        #if IdentificacaoAnimal.objects.filter(animal__animal_id__exact='animal_id'):
        '''
        if Identificador.objects.filter(identificado_animal__identificador__exact=Animal.objects.filter(pk=self.pk)):
            id=Identificador.objects.filter(identificado_animal__identificador__exact=Animal.objects.filter(pk=self.pk))
            return '%s %s' % (id, self.nome)
        else:
            return '#%s [sem identificador associado] %s [%s]' % (self.pk, self.nome, self.sexo)
        '''
        return '%s - %s' % (force_unicode(self.sia), self.sex)
    
    
    def get_specie(self):
        '''
        Get animal specie
        '''
        return self.specie_breed.specie


    def get_breed(self):
        '''
        Get animal breed
        '''
        return self.specie_breed.breed


class Bird(Animal):

    class Meta():
        verbose_name= _('Bird')
        verbose_name_plural= _('Birds')


class Bovine(Animal):
    milkings =  models.ManyToManyField('ordenha.Milking', verbose_name = _('Milking list'), 
                                       blank = True, null = True)
    
    class Meta():
        verbose_name= _('Bovine')
        verbose_name_plural= _('Bovines')


class Caprine(Animal):
    milkings =  models.ManyToManyField('ordenha.Milking', verbose_name = _('Milking list'), 
                                       blank = True, null = True)
    
    class Meta():
        verbose_name= _('Caprine')
        verbose_name_plural= _('Caprines')



class Equine(Animal):
    
    class Meta():
        verbose_name= _('Equine')
        verbose_name_plural= _('Equines')


class Leporine(Animal):
    
    class Meta():
        verbose_name= _('Leporine')
        verbose_name_plural= _('Leporines')


class Ovine(Animal):
    milkings =  models.ManyToManyField('ordenha.Milking', verbose_name = _('Milking list'), 
                                       blank = True, null = True)
    
    class Meta():
        verbose_name= _('Ovine')
        verbose_name_plural= _('Ovines')


class Swine(Animal):
    
    class Meta():
        verbose_name= _('Swine')
        verbose_name_plural= _('Swines')


class SpecieBreed(models.Model): #está a ser usada em Entidades
    __metaclass__ = TransMeta
    species_choices = (('Bird', _('Bird')), 
                       ('Bovine', _('Bovine')), 
                       ('Caprine', _('Caprine')), 
                       ('Equine', _('Equine')), 
                       ('Leporine', _('Leporine')), 
                       ('Ovine', _('Ovine')), 
                       ('Swine', _('Swine')))
    specie = models.CharField(_('Specie'), max_length = 15, choices = species_choices, blank=False)
    breed = models.CharField(_('Breed'), max_length=50, blank=False)
    code = models.CharField(_('Breed Code'), max_length=4, blank=False, null = True)


    class Meta():
        verbose_name= _('Specie/Breed')
        verbose_name_plural= _('Species/Breeds')
        translate = ('breed', 'code',)
    
    
    def __unicode__(self):
        return '%s - %s' % (self.specie, self.breed)


class PelageType(models.Model):
    __metaclass__ = TransMeta
    name = models.CharField(_('Name'), max_length=20, blank=False)
    color = models.CharField(_('Color'), max_length=20, blank=False)
    specie_breed = models.ForeignKey('SpecieBreed', verbose_name= _('Specie - Breed'))


    class Meta():
        verbose_name= _('Pelage type')
        verbose_name_plural= _('Pelages types')
        translate = ('name','color',)


    def __unicode__(self):
        return '%s (%s)' % (self.name, self.color)


class TabulatedTypeValue(models.Model):
    '''
    Type of value
    '''
    __metaclass__ = TransMeta
    name = models.CharField(_('Name'), max_length=30, blank=False)
    
    
    class Meta():
        verbose_name = _('Tabulated type value')
        verbose_name_plural = _('Tabulated type values')
        translate = ('name',)
        
        
    def __unicode__(self):
        return '%s' % self.name


class TabulatedValue(models.Model):
    value_type = models.ForeignKey('TabulatedTypeValue', verbose_name = _('Value type'))
    value = models.CharField(_('Value'), max_length = 50, blank = False, null = False)
    sex_choices=(('F', _('Female')),('M', _('Male')), ('A', _('Any')))
    sex = models.CharField(_('Sex'), max_length=1, choices=sex_choices, blank = True, null = True)
    custom = models.BooleanField(_('Customized by breeder'), default = True)
    farm = models.ForeignKey('exploracao.Farm', blank = True, null = True)
    active = models.BooleanField(_('Active'), default = True)
    
    
    class Meta():
        verbose_name = _('Tabulated value')
        verbose_name_plural = _('Tabulated values')


class ReferenceWeight(models.Manager):
    
    def get_query_set(self):
        return super(ReferenceWeight, self).get_query_set().filter(value_type__name_pt = 'Peso ao nascimento')
    
    
class BreedTabulatedValue(TabulatedValue):
    '''
    Tabulated values for breeds
    '''
    specie_breed = models.ForeignKey('SpecieBreed', verbose_name = _('Specie - Breed'))
    objects = models.Manager()
    reference_weight = ReferenceWeight() 

    class Meta:
        verbose_name = _('Tabulated value for one animal breed')
        verbose_name_plural = _('Tabulated values for one animal breed')


    def __unicode__(self):
        return '%s - %s kg - %s - %s' % (self.value_type, self.value, self.specie_breed, self.sex)


class SpeciesTabulatedValues(TabulatedValue):
    '''
    Tabulated values for species
    '''    
    species_choices = (('Bird', _('Bird')), 
                       ('Bovine', _('Bovine')), 
                       ('Caprine', _('Caprine')), 
                       ('Equine', _('Equine')), 
                       ('Leporine', _('Leporine')), 
                       ('Ovine', _('Ovine')), 
                       ('Swine', _('Swine')))
    species_name = models.CharField(_('Specie'), max_length = 15, choices = species_choices, 
                                    null = False, blank=False)

    
    def __unicode__(self):
        return '%s -%s (%s - %s)' % (self.value_type, self.value, self.species_name, self.sex)

    
    class Meta():
        verbose_name = _('Tabulated value for one Specie')
        verbose_name_plural = _('Tabulated values for one Specie')
        

class Medicao(models.Model):
    pass

