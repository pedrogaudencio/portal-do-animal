# -*- coding: UTF-8 -*-
from animal.models import Animal, SpecieBreed, Bird, Bovine, Caprine, Equine, Leporine, Ovine, Swine
from identificacao.models import Identifier
from django import forms
from django.forms.widgets import CheckboxSelectMultiple, Select
from exploracao.models import Farm

class AdicionarAnimalForm(forms.Form):
    identificacao=forms.CharField(error_messages={'required': 'Por favor, escolha um identificador'},
                                  widget=forms.Select(choices=Identifier.objects.all()))
    exploracao=forms.CharField(error_messages={'required': 'Por favor, escolha a exploração do animal'},
                         widget=forms.Select(choices=Farm.objects.all()))
    nome=forms.CharField(max_length=20, label=('Nome'), required=False)
    sexo=forms.CharField(error_messages={'required': 'Por favor, escolha o sexo do animal'},
                         max_length=1, widget=forms.Select(choices=Animal.sex_choices), label=('Sexo'), required=True)
    sia=forms.IntegerField(label=('Número de SIA'), required=True)
    passaporte=forms.IntegerField(label=('Número de Passaporte'), required=True)
    numero_pai = forms.CharField(widget=forms.Select(choices=Bird.objects.filter(sex='M')), required=True)
    numero_mae = forms.CharField(widget=forms.Select(choices=Bird.objects.filter(sex='F')), required=True)
    obs=forms.CharField(label=('Observações'), max_length=300)
#    lg = models.ForeignKey('LivroGeneologico')


class AdicionarAveForm(AdicionarAnimalForm):
    raca=forms.CharField(error_messages={'required': 'Por favor, escolha a raça da ave'},
                     widget=forms.Select(choices=SpecieBreed.objects.filter(specie='Birt')))
#                     widget=forms.Select(choices=SpecieBreed.objects.filter(nome_especie='Aves')))

    def save(self):
        nova_ave = Bird()
        nova_ave.identificacao = self.cleaned_data['identificacao']
        nova_ave.exploracao = self.cleaned_data['exploracao']
        nova_ave.raca = self.cleaned_data['raca']
        nova_ave.nome = self.cleaned_data['nome']
        nova_ave.sexo = self.cleaned_data['sexo']
        nova_ave.sia = self.cleaned_data['sia']
        nova_ave.passaporte = self.cleaned_data['passaporte']
        nova_ave.numero_pai = self.cleaned_data['numero_pai']
        nova_ave.numero_mae = self.cleaned_data['numero_mae']
        nova_ave.obs = self.cleaned_data['obs']
        
        nova_ave.save()


class AdicionarBovinoForm(AdicionarAnimalForm):
    raca=forms.CharField(error_messages={'required': 'Por favor, escolha a raça do bovino'},
                     widget=forms.Select(choices=SpecieBreed.objects.filter(specie='Bovine')))

    def save(self):
        novo_bovino = Bovine()
        novo_bovino.identificacao = self.cleaned_data['identificacao']
        novo_bovino.exploracao = self.cleaned_data['exploracao']
        novo_bovino.raca = self.cleaned_data['raca']
        novo_bovino.nome = self.cleaned_data['nome']
        novo_bovino.sexo = self.cleaned_data['sexo']
        novo_bovino.sia = self.cleaned_data['sia']
        novo_bovino.passaporte = self.cleaned_data['passaporte']
        novo_bovino.numero_pai = self.cleaned_data['numero_pai']
        novo_bovino.numero_mae = self.cleaned_data['numero_mae']
        novo_bovino.obs = self.cleaned_data['obs']

class AdicionarCaprinoForm(AdicionarAnimalForm):
    raca=forms.CharField(error_messages={'required': 'Por favor, escolha a raça do caprino'},
                     widget=forms.Select(choices=SpecieBreed.objects.filter(specie='Caprine')))

    def save(self):
        novo_caprino = Caprine()
        novo_caprino.identificacao = self.cleaned_data['identificacao']
        novo_caprino.exploracao = self.cleaned_data['exploracao']
        novo_caprino.raca = self.cleaned_data['raca']
        novo_caprino.nome = self.cleaned_data['nome']
        novo_caprino.sexo = self.cleaned_data['sexo']
        novo_caprino.sia = self.cleaned_data['sia']
        novo_caprino.passaporte = self.cleaned_data['passaporte']
        novo_caprino.numero_pai = self.cleaned_data['numero_pai']
        novo_caprino.numero_mae = self.cleaned_data['numero_mae']
        novo_caprino.obs = self.cleaned_data['obs']

class AdicionarEquinoForm(AdicionarAnimalForm):
    raca=forms.CharField(error_messages={'required': 'Por favor, escolha a raça do equino'},
                     widget=forms.Select(choices=SpecieBreed.objects.filter(specie='Equine')))

    def save(self):
        novo_equino = Equine()
        novo_equino.identificacao = self.cleaned_data['identificacao']
        novo_equino.exploracao = self.cleaned_data['exploracao']
        novo_equino.raca = self.cleaned_data['raca']
        novo_equino.nome = self.cleaned_data['nome']
        novo_equino.sexo = self.cleaned_data['sexo']
        novo_equino.sia = self.cleaned_data['sia']
        novo_equino.passaporte = self.cleaned_data['passaporte']
        novo_equino.numero_pai = self.cleaned_data['numero_pai']
        novo_equino.numero_mae = self.cleaned_data['numero_mae']
        novo_equino.obs = self.cleaned_data['obs']

class AdicionarLeporideoForm(AdicionarAnimalForm):
    raca=forms.CharField(error_messages={'required': 'Por favor, escolha a raça do leporídeo'},
                     widget=forms.Select(choices=SpecieBreed.objects.filter(specie='Leporine')))

    def save(self):
        novo_leporideo = Leporine()
        novo_leporideo.identificacao = self.cleaned_data['identificacao']
        novo_leporideo.exploracao = self.cleaned_data['exploracao']
        novo_leporideo.raca = self.cleaned_data['raca']
        novo_leporideo.nome = self.cleaned_data['nome']
        novo_leporideo.sexo = self.cleaned_data['sexo']
        novo_leporideo.sia = self.cleaned_data['sia']
        novo_leporideo.passaporte = self.cleaned_data['passaporte']
        novo_leporideo.numero_pai = self.cleaned_data['numero_pai']
        novo_leporideo.numero_mae = self.cleaned_data['numero_mae']
        novo_leporideo.obs = self.cleaned_data['obs']

class AdicionarOvinoForm(AdicionarAnimalForm):
    raca=forms.CharField(error_messages={'required': 'Por favor, escolha a raça do ovino'},
                     widget=forms.Select(choices=SpecieBreed.objects.filter(specie='Ovine')))

    def save(self):
        novo_ovino = Ovine()
        novo_ovino.identificacao = self.cleaned_data['identificacao']
        novo_ovino.exploracao = self.cleaned_data['exploracao']
        novo_ovino.raca = self.cleaned_data['raca']
        novo_ovino.nome = self.cleaned_data['nome']
        novo_ovino.sexo = self.cleaned_data['sexo']
        novo_ovino.sia = self.cleaned_data['sia']
        novo_ovino.passaporte = self.cleaned_data['passaporte']
        novo_ovino.numero_pai = self.cleaned_data['numero_pai']
        novo_ovino.numero_mae = self.cleaned_data['numero_mae']
        novo_ovino.obs = self.cleaned_data['obs']

class AdicionarSuinoForm(AdicionarAnimalForm):
    raca=forms.CharField(error_messages={'required': 'Por favor, escolha a raça do suíno'},
                     widget=forms.Select(choices=SpecieBreed.objects.filter(specie='Swine')))

    def save(self):
        novo_suino = Swine()
        novo_suino.identificacao = self.cleaned_data['identificacao']
        novo_suino.exploracao = self.cleaned_data['exploracao']
        novo_suino.raca = self.cleaned_data['raca']
        novo_suino.nome = self.cleaned_data['nome']
        novo_suino.sexo = self.cleaned_data['sexo']
        novo_suino.sia = self.cleaned_data['sia']
        novo_suino.passaporte = self.cleaned_data['passaporte']
        novo_suino.numero_pai = self.cleaned_data['numero_pai']
        novo_suino.numero_mae = self.cleaned_data['numero_mae']
        novo_suino.obs = self.cleaned_data['obs']
