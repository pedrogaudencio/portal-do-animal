from django.shortcuts import get_object_or_404, render_to_response
from animal.models import Animal
from django.http import HttpResponseRedirect
from animal.forms import AdicionarAveForm
from django.views.decorators.csrf import csrf_protect
from django.template.context import RequestContext

@csrf_protect
def detalhes(request, pk):
    obj = get_object_or_404(Animal, id=pk)
    
    return render_to_response('animal/detalhes.html', {'animal': obj },
                              context_instance=RequestContext(request))

@csrf_protect
def adicionar_ave(request):
    if request.method == 'POST':
        adicionaform = AdicionarAveForm(request.POST)

        if adicionaform.is_valid():
            print "all validation passed"
            adicionaform.save()
            
            return HttpResponseRedirect('../')
    else:
        print "validation failed"
        adicionaform = AdicionarAveForm()

    return render_to_response('animal/adicionar.html', {'form': adicionaform},
                                context_instance=RequestContext(request))

'''
@csrf_protect
def modificar(request, pk):
    
    

def remover(request, pk):
    
    
'''