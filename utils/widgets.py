from django import forms
import datetime


class DatePickerWidget(forms.TextInput):
    """
    Data picker widget
    """
    def render(self, name, value, attrs=None):
        """
        Widget render
        """
        if isinstance(value, datetime.date):
                value=value.strftime("%d/%m/%Y")


        attrs.update({'class': 'span10 datePicker',
                      })

        return super(DatePickerWidget, self).render(name, value, attrs)


class ChosenSingleWidget(forms.Select):
    """
    Chosen single input
    """
    def __init__(self, *args, **kwargs):
        self.placeholder = kwargs.pop('placeholder', None)
        super(ChosenSingleWidget, self).__init__(*args, **kwargs)
    
    
    def render(self, name, value, attrs=None, choices=()):
        """
        Widget render
        """
        attrs.update({'class': 'chzn-select czn-container-single',      
                      })
        
        """
        if self.placeholder:
            attrs.update({'data-placeholder': self.placeholder,  
                         }) 
        """
        return super(ChosenSingleWidget, self).render(name, value, attrs, choices)