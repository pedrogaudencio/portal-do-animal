# -*- coding: UTF-8 -*-
import csv
from portaldoanimal import settings
from portaldoanimal.error import CsvError
from animal.models import Animal, Bovine, Caprine, SpecieBreed
from identificacao.models import IdentifierType, Identifier
from red.models import AnimalIdentification
from cities_light.models import Country
from documents.models import File
from datetime import datetime
from utils.nif import Nif
from django.http import HttpResponse
from django.utils.translation import ugettext as _
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned


class Handle_csv_upload():

    def __init__(self, csv_file, farm, country):

        #get last csv_file
        self.csv_file = File.objects.filter(document__name = csv_file).latest('document')
        self.farm = farm
        self.country = country

        self.menssageDict = {}
        self.menssage_animal = []


    def importCsv(self):
        """
        SNIRA - Portugal:
        0 - NIF Breeder
        1 - NIF Identifier agent
        2 - Farm trademark
        3 - Identification date
        4 - Identifier type (code)
        5 - Re-Identification? (S or N)
        6 - Identifier number (RFID)
        7 - Number of earpiece mark
        8 - Species code
        9 - Breed code
        10 - Sex (F, M, C)
        11 - Birthdate
        12 - Entry date (at farm)
        13 - Particular identification
        14 - Genealogical registration
        15 - Mother number of earpiece mark
        16 - Father number of earpiece mark
        17 - Document type (I - Insertion, A - Modification)
        """

        with open(str(settings.MEDIA_ROOT) + str(self.csv_file), 'rb') as f:
            reader = csv.reader(f, delimiter = ';')


            #ATENÇÃO A ESTE EXCEPT. APANHA TUDO
            try:
                self._choose_country(reader)
            except:
                self.menssageDict['error'] = _('Error while processing csv.')

            self.menssageDict['animal'] = self.menssage_animal

            if len(self.menssage_animal) == 0 and not self.menssageDict['validateFarm']:
                self.menssageDict['sucess'] = _('Data imported with success')

            return self.menssageDict


    def _choose_country(self, reader):
        """
        Choose different csv model. One for each country
        """

        if self.country == 'pt':
            for row in reader:
            #confirm nif and farm trademark
            #fará sentido dizer que o produtor tb tem de ter nif do país?
                menssage = self._validateFarm(self.farm, row[0], row[2])
                if menssage != True:
                    self.menssageDict['validateFarm'] = menssage
                    break

                mB =  self._save_animal_pt(row, self.farm)
                if mB: #Para não aparecer None
                    self.menssage_animal.append(mB)


    def _save_animal_pt(self, data, farm):
        """
        Save animal. Portugal SINIRA
        """
        #Animal identification - if identifier type code incorrect
        if data[4] not in ['01','02','03','04']:
            return _('Verify identifier type code for animal: %s') % data[7]

        sia = self._getIndentification(data[7], data[4])

        #If csv line is for a insertion, verify if animal already exists on farm
        if data[17] == 'I':
            try:
                Animal.objects.get(farm = farm, sia = sia, active = True)
                return _('Animal with number: ')+ str(sia) + _(' already exist on your farm.')
            except MultipleObjectsReturned:
                return CsvError().except_3001()
            except ObjectDoesNotExist:
                #create new animal
                existent_animal = False
                if data[8] == '02':
                    animal = Bovine()
                if data[8] == '03':
                    animal = Caprine()
        elif data[17] == 'A':
            try:
                #get the animal
                animal = Animal.objects.get(farm = farm, sia = sia, active = True)
            except MultipleObjectsReturned:
                return CsvError().except_3001()
            except ObjectDoesNotExist:
                return _('Animal with number: ')+ str(sia) +_(' doesn\'t exist on your farm.')
        else:
            return _('Check the last row position. Must be: I, for animal insertion, or A, for animal alteration')


        try:
            #farm
            animal.farm = farm
            #birthday
            date = datetime.strptime(data[11],'%Y%m%d')
            animal.birthdate = date.strftime('%Y-%m-%d')
            #sex
            if data[10] =='F':
                animal.sex = 'F'
            if data[10] =='M':
                animal.sex = 'M'
            if data[10] =='C':
                animal.sex = 'M'
                animal.male_castrated = True
            #identification
            animal.sia = sia
            #race
            animal.specie_breed = self._get_breed(data[9])

            animal.save()
        except:
            return _('Error importing animal number: ' + str(data[7]))

        #New identification
        if data[5] == 'S' or not existent_animal:
            #print "New identification"
            #try:
            animal_identification = AnimalIdentification()
            animal_identification.identifier = sia
            animal_identification.animal = animal
            if data[1]:
                animal_identification = data[1]
            #Try identification date
            try:
                animal_identification.date = (datetime.strptime(data[3],'%Y%m%d')).strftime('%Y-%m-%d')
            #if theren't one birth date
            except:
                animal_identification.date = (datetime.strptime(data[11],'%Y%m%d')).strftime('%Y-%m-%d')

            animal_identification.save()

            #Add identification to animal
            animal.identification.add(animal_identification)


    def _getIdentificationType(self, code):
        """
        Return identification type object:
            PT:
            'MA Convencional' - 01
            'MA Electrónica' - 02
            'Subcutâneo IDE' - 03
            'Reticular IDE' - 04
        """

        for lang, dont_give_a_fuck in settings.LANGUAGES:
            field_code = 'code_%s' % lang
            try:
                return IdentifierType.objects.get(**{field_code: code})
            except MultipleObjectsReturned:
                return CsvError().except_3002()
            except ObjectDoesNotExist:
                return CsvError().except_3003()


    def _getIndentification(self, number, code):
        """
        Return identification object.
        Search for a given identification by number, if doesn't exist
        create a new identification with that number.
        """
        try:
            identification = Identifier.objects.get(number = number)
        except ObjectDoesNotExist:
            identification = Identifier()
        try:
            identification.id_type = self._getIdentificationType(code)
        except ValueError:
            self.menssageDict['error_list'] = str(self._getIdentificationType(code))
            identification = None

        identification.number = number
        identification.save()

        return identification


    def _get_breed(self, code):
        """
        Return breed object
        """
        return SpecieBreed.objects.get(code_pt = code)


    def _validateFarm(self, farm, nif, trademark):
        """
        Verify if nif in csv is the same as the farm
        (only portuguese nif)
        """
        menssage = Nif().validate(nif, 'Portugal')
        if menssage == True:
            if farm.breeder.nif != int(nif):
                menssage = _('Nif from csv don\'t match breeder.')
            else:
                if farm.trademark != trademark:
                    menssage = _('Breeder trademark don\'t match.')

        return menssage



class Handle_csv_generator():
    """
    CSV generator
    """

    def __init__(self):
        self.response = HttpResponse(mimetype='text/csv')


    def sniraExport(self, farm, animalsList, register_type):
        """
        Generates a .txt according to SNIRA specifications
            register_type = I - Inserção de animal
            register_type = A - Alteração de animal
        """
        menssage = []
        dict_error = {}

        try:
            nif = farm.breeder.nif
        except:
            nif = '000000000'

        try:
            date_time = datetime.strftime(datetime.today(), "%Y%m%d%H%M%S")
        except:
            date_time = 'aaaammddhhmmss'

        filename = str(nif) + '_' + str(date_time)
        self.response['Content-Disposition'] = 'attachment; filename=' + filename + '.txt'
        writer = csv.writer(self.response, delimiter=';')

        for animal in animalsList:
            #NIF Identifier agent
            try:
                nifPI = animal.nifPI
                if nifPI == None:
                    nifPI = ''
            except:
                nifPI = ''

            #Farm trademark - required
            try:
                trademark = animal.farm.trademark
                if trademark == None:
                    menssage.append(_('Invalid farm trademark'))
            except:
                trademark=''

            #Try to retrieve last identification date, if fails try first identification date - required
            try:
                try:
                    identification_date = animal.identification.order_by('date')[:1][0].date.strftime('%Y%m%d')
                except:
                    identification_date = animal.first_identification.strftime('%Y%m%d')
            except:
                identification_date = ''
                menssage.append(_('First identification date is invalid'))

            #Identifier type (code) - required
            try:
                identification_code = animal.sia.id_type.code_pt
            except:
                identification_code = ''
                menssage.append(_('The identifier type code is invalid'))

            #Re-Identification? (S or N)
            try:
                pass
            except:
                pass


            try: #required
                identification_iso = self._getIdentificationIso(animal.sia.number)
            except:
                identification_iso = ''
                menssage.append(_('Identifier ISO number is invalid'))

            try: #required
                identification = animal.sia.number
            except:
                identification = ''
                menssage.append(_('Identifier number is invalid'))

            #Species - required
            try:
                if animal.specie_breed.specie == 'Bovine':
                    specie = '02'
                if animal.specie_breed.specie == 'Caprine':
                    specie = '03'
            except:
                specie = ''
                menssage.append(_('Invalid species code'))

            #Breed - required
            try:
                breed = animal.specie_breed.code_pt
            except:
                breed = ''
                menssage.append(_('Invalid breed code'))

            #Sex(F, M, C) - required
            try:
                if animal.male_castrated:
                    sex = 'C'
                else:
                    sex = animal.sex
            except:
                sex = ''
                menssage.append(_('Invalid sex'))

            #Birthdate - required
            try:
                birth = animal.birthdate.strftime('%Y%m%d')
            except:
                birth = ''
                menssage.append(_('Invalid birthdate'))

            # Entry date (at farm)
            try:
                entry_data = ''
            except:
                entry_data = ''

            #Particular identification
            try:
                particular_identification = ''
            except:
                particular_identification = ''

            #Genealogical registration
            try:
                genealogical_registration = ''
            except:
                genealogical_registration = ''

            #Mother number of earpiece mark
            try:
                mother_sia = animal.mother.sia.number
            except:
                mother_sia = ''

            #Father number of earpiece mark
            try:
                father_sia = animal.father.sia.number
            except:
                father_sia = ''


            if len(menssage) > 0:
                dict_error[animal] = menssage

            else:
                writer.writerow([str(nif), str(nifPI), str(trademark), str(identification_date),
                                 str(identification_code), str(identification_iso), str(identification),
                                 str(specie), str(breed), str(sex), str(birth), str(entry_data),
                                 str(particular_identification), str(genealogical_registration),
                                 str(mother_sia), str(father_sia), str(register_type),''])

        return (self.response, dict_error)



    def _getIdentificationIso(self, identification):

        code = '000'
        country_code = ''
        remain_identification = ''

        for l in identification:
            if not l.isdigit():
                country_code = country_code + l
            else:
                remain_identification += l

        try:
            code = Country.objects.get(code2 = country_code)
            return 'xxxxxxxxxxxxxxxx' #comentar
            return ('0' + str(code.geoname_id) + str(remain_identification))
        except:
            return 'xxxxxxxxxxxxxxxx' #comentar
            code = Country.objects.get(code3 = country_code)
            return ('0' + str(code.geoname_id) + str(remain_identification))


        return 'xxxxxxxxxxxxxxxx'
