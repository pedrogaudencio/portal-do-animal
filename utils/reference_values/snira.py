# -*- coding: UTF-8 -*-
#from django.utils.translation import ugettext as _
"""
Csv validation errors
"""
SNIRA_SPECIES = {'01': 'Bovinos',
                 '02': 'Ovinos',
                 '03': 'Caprinos',
                 '04': 'Suínos',
                 '05': 'Equídeos',}

SNIRA_CAPRINE = {'01': 'Bravia',
                 '02': 'Charnequeira',
                 '03': 'Algarvia',
                 '04': 'Serpentina',
                 '05': 'Alpina',
                 '06': 'Ana',
                 '07': 'Angora',
                 '08': 'Murc. Granadina',
                 '09': 'Saanen',
                 '10': 'Serrrana',
                 '11': 'Outras',
                 '12': 'Cruzada',
                 }

SNIRA_OVINE = {'01': 'Churra Algarvia',
               '02': 'Merina Preta,',
               '03': 'Mondegueira',
               '04':'Assaf',
               '05': 'Bordaleira Entre Douro e Minho',
               '06': 'Awassi',
               '07': 'Campaniça',
               '08': 'Charolais',
               '09': 'Churra Badana',
               '10': 'Churra Terra Quente',
               '11': 'Churra Entre Douro e Minho',
               '12': 'Churra do Campo',
               '13': 'Churra Marialva',
               '14': 'Cruzada',
               '15': 'Galega Bragançana',
               '16': 'Frísia',
               '17': 'Galega Mirandesa',
               '18': 'Ile de France',
               '19': 'Indeterminada Carne',
               '20': 'Indeterminada Leite',
               '21': 'Outras',
               '22': 'Lacaune',
               '23': 'Manchega',
               '24': 'Romanov',
               '25': 'Merino Alemão',
               '26': 'Merino Beira Baixa',
               '27': 'Merino Branco',
               '28': 'Merino Precoce',
               '29': 'Romney Marsh',
               '30': 'Saloia',
               '31': 'Sarda',
               '32': 'Serra Estrela',
               '33': 'Suffolk',
               '34': 'Tipo Bordaleira',
               '35': 'Tipo Churra',
               '36': 'Tipo Merino',     
               }

SNIRA_COUNTRIES = {'PT': 0620,
                   }

SNIRA_ERROR = {'1': ['O NIF %s não é válido.', 
                     'Verificar o NIF que consta no ficheiro. Corrigir o NIF que consta no ficheiro.'],
               '2': ['O NIF %s não está registado no SNIRA.', 
                     'Verificar o NIF que consta no ficheiro. Verificar se o NIF está registado\
                      no SNIRA. O detentor terá de proceder ao registo noSNIRA.'],
               '3': ['O NIF %s não está registado no SNIRA como detentor ou detentor associado',
                     'Verificar o NIF que consta no ficheiro. Verificar se o NIF está registado\
                      no SNIRA como detentor criador ou detentor associado. O detentor terá de\
                      proceder ao registo no SNIRA como detentor criador ou detentor associado.'],
               '4': ['O NIF %s não pertence ao NIFAP que está a carregar o ficheiro.',
                      'O NIF que conta no campo detentor dos animais no ficheiro, não corresponde\
                      ao NIFAP para o qual criou o documento de iDigital. Verificar o NIF do\
                      detentor dos animais no ficheiro ou o NIFAP para o qual está a tentar submeter\
                      o ficheiro.'],
               '5': ['O NIF %s não está registado no SNIRA como agente identificador.', 
                     'Verificar o NIF que consta no ficheiro no campo referente ao Agente Identificador.\
                      Actualizar o registo referente ao NIF em questão na BD SNIRA mediante as normas\
                      de correcção de dados na BD SNIRA.'],
               '6': ['O código do tipo de identificação electrónica não está preenchido',
                     'O campo obrigatório referente ao Código de Tipo de dentificador, não está\
                      preenchido. Preencher de acordo com a tabela "Código Tipo Identificador", que\
                      consta do "Manual de Transferência de Ficheiros de Identificação electrónica (IDE)\
                      de ovinos e caprinos"'],
               '7': ['O código do tipo de identificação electrónica %s não existe no SNIRA.',
                     'Os dados constantes no campo obrigatório referente ao Código de Tipo de\
                      Identificador, não são válidos, deverá consultar os possíveis códigos de Tipos\
                      de Identificador na tabela "Código Tipo Identificador", que consta do "Manual\
                      de Transferência de Ficheiros de Identificação electrónica (IDE) de ovinos e\
                      caprinos"'],
               '8': ['A Id. electrónica %s não tem 15 ou 16 algarismos.',
                     'O campo obrigatório referente a Identificação Electrónica, não tem o número de\
                      algarismos estipulados. Deverá ser corrigido de acordo com as normas referenciadas\
                      no "Manual de Transferência de Ficheiros de Identificação electrónica (IDE) de\
                      ovinos e caprinos"'],
               '9': ['O país %s não está registado no SNIRA', 
                     'O código País que está inserido na Identificação electrónica do animal, não\
                      consta da tabela "Países" na "Manutenção de dados de Referência" no menu Horizontal\
                       da BD SNIRA'],
               '10': ['A espécie não está preenchida.',
                      'O campo obrigatório referente à Espécie dos animais, não está preenchido.\
                      Preencher de acordo com a tabela "Espécie" que consta do "Manual de Transferência\
                      de Ficheiros de Identificação electrónica (IDE) de ovinos e caprinos"'],
               '11': ['A espécie %s não existe no SNIRA',
                      'Os dados constantes no campo obrigatório referente à espécie não consta da tabela\
                      "Raças" na "Manutenção de dados de Referência" no menu Horizontal da BD SNIRA'],
               '12': ['A raça não está preenchida.',
                      'O campo obrigatório referente à Raça dos animais, não está preenchido. Preencher\
                       de acordo com a tabela "Espécie" que consta do "Manual de Transferência de\
                       Ficheiros de Identificação electrónica (IDE) de ovinos e caprinos"'],
               '13': ['A raça %s não existe no SNIRA para a espécie %s.',
                      'Os dados constantes no campo obrigatório referente à Raça não consta da tabela\
                       "Raças" para a Espécie inserida no ficheiro, na "Manutenção de dados de Referência"\
                       no menu Horizontal da BD SNIRA.'],
               '14': ['O sexo não está preenchido.',
                      'O campo obrigatório referente ao Sexo dos animais, não está preenchido. Preencher\
                       de acordo com a tabela "Código Sexo" que consta do "Manual de Transferência de\
                       Ficheiros de Identificação electrónica (IDE) de ovinos e caprinos"'],
               '15': ['O sexo %s não é válido.',
                      'Os dados constantes no campo obrigatório referente ao Sexo do animal não constam\
                       da tabela "Código Sexo" que consta do "Manual de Transferência de Ficheiros de\
                        Identificação electrónica (IDE) de ovinos e caprinos"'],
               '16': ['A data de identificação não está preenchida.',
                      'O campo obrigatório referente à data de identificação dos animais, não está\
                       preenchido. Preencher de acordo com uma data válida.'],
               '17': ['A data de identificação %s não pode ser superior à data actual.',
                      'Os dados constantes no campo obrigatório referente à data de identificação dos\
                       animais não são válidos em comparação com a data para a qual está a ser carregado\
                        o ficheiro. Corrigir a data de Identificação dos animais do ficheiro'],
               '18': ['A data %s é inválida.',
                      'Os dados constantes no campo da data não estão válidos. Preencher de acordo com\
                       uma data válida no formato'],
               '19': ['A data de nascimento não está preenchida.',
                      'O campo obrigatório referente à data de nascimento dos animais, não está preenchido.\
                       Preencher de acordo com uma data válida.'],
               '20': ['A data de nascimento %s não pode ser superior à data actual.',
                      'Os dados constantes no campo obrigatório referente à data de nascimento dos animais\
                       não são válidos em comparação com a data para a qual está a ser carregado o ficheiro.\
                        Corrigir a data de nascimento dos animais do ficheiro.'],
               '21': ['A data de entrada na exploração %S não pode ser superior à data actual.',
                      'Os dados constantes no campo referente à data de entrada na exploração dos animais não\
                       são válidos em comparação com a data para a qual está a ser carregado o ficheiro.\
                        Corrigir a data de entrada na exploração dos animais do ficheiro.'],
               '22': ['A data de nascimento %s não pode ser superior à data de identificação %s.',
                      'Os dados constantes no campo obrigatório referente à data de nascimento dos animais\
                       não são válidos em comparação com a data de identificação para os animais no ficheiro.\
                        Corrigir a data que esteja incorrecta.'],
               '23': ['A marca de exploração não está preenchida.',
                      'O campo obrigatório referente à marca de exploração dos animais, não está preenchido.\
                       Preencher de acordo com a marca de exploração válida.'],
               '24': ['A marca de exploração %s não está associada à espécie %s.',
                      'Verificar o código da ME, o código da espécie, ou a data da identificação vs a data\
                       de início da ME na BD SNIRA.'],
               '25': ['A marca %s não está registada no SNIRA.',
                      'Verificar o código da ME no ficheiro vs a ME que está registada na BD SNIRA'],
               '26': ['A marca de exploração %s não está associada ao NIF %s.',
                      'O código da ME que está inserida no ficheiro não está registado para o NIF do ficheiro\
                       na BD SNIRA. Verificar a ME e/ou o NIF no ficheiro ou na BD SNIRA.'],
               '27': ['Impossível validar a marca de exploração com os dados deste animal.',
                      'Verificar a Identificação Electrónica ou a Marca de Exploração que consta do ficheiro\
                       que está a tentar submeter.'],
               '28': ['O indicador de Inserção/Alteração não está preenchido.',
                      'O campo obrigatório referente ao indicador de Inserção/Alteração não está preenchido.\
                       Preencher de acordo com um indicador válido.'],
               '29': ['O indicador de Inserção/Alteração %s tem um valor inválido.',
                      'Os dados constantes no campo obrigatório referente ao indicador de Inserção/Alteração\
                       não são válidos. Preencher de acordo com um indicador válido.'],
               '30': ['O indicador de Inserção/Alteração %s só pode ter os valores I ou A.',
                      'Os dados constantes no campo obrigatório referente ao indicador de Inserção/Alteração\
                       não são válidos. Preencher de acordo com um indicador válido.'],
               '31': ['O animal %s já existe neste ficheiro',
                      'O animal com esta identificação já existe registado neste ficheiro. Corrigir a\
                       identificação do animal no ficheiro ou eliminá-lo.'],
               '32': ['O animal %s já está registado no SNIRA.',
                      'O animal com esta identificação já existe registado na BD SNIRA. Verificar a\
                       identificação do animal no ficheiro ou eliminar o mesmo.'],
               '33': ['O animal %s já está morto ou abatido. Não pode alterar os dados.',
                      'O animal com esta identificação está registado como morto ou abatido na BD SNIRA.\
                       Verificar a identificação do animal no ficheiro ou eliminar o mesmo.'],
               '34': ['O animal %s foi registado por outra entidade. Não pode alterar os dados.',
                      'O animal com esta identificação já existe registado na BD SNIRA registado por outra\
                       entidade. Verificar a identificação do animal no ficheiro ou eliminar o mesmo.'],
               '35': ['O animal %s está duplicado no ficheiro.',
                      'O animal com esta identificação já existe registado neste ficheiro. Corrigir a\
                       identificação do animal no ficheiro ou eliminá-lo.'],
               '36': ['O animal %s não está registado no SNIRA. Não pode alterar dados.',
                      'O animal com esta identificação não existe registado na BD SNIRA. Verificar a\
                       identificação do animal no ficheiro ou o Indicador de Inserção/Alteração.'],
               '37': ['O animal com a MA %s já existe neste ficheiro.',
                      'O animal com esta identificação já existe registado neste ficheiro. Corrigir a \
                       identificação do animal no ficheiro ou eliminá-lo.'],
               '38': ['O animal com a MA %s já está registado no SNIRA.',
                      'O animal com esta identificação já existe registado na BD SNIRA. Verificar a \
                      identificação do animal no ficheiro ou eliminar o mesmo.'],
               '39': ['O animal com a MA %s tem a data da morte anterior à data de identificação.',
                      'O animal com esta identificação tem data de morte registada na BD SNIRA anterior à \
                      data de identificação que consta do ficheiro. Corrigir a data de Identificação do \
                      ficheiro para o animal em questão.'],
               '40': ['O indicador de re-identificação tem um valor inválido.',
                      'Os dados constantes no campo obrigatório referente ao indicador de re-identificação \
                      não são válidos. Preencher de acordo com um indicador válido.'],
               '41': ['A identificação particular do animal tem mais de 30 caracteres.',
                      'Os dados constantes no campo referente a identificação particular do animal excedem \
                      o número máximo permitido para o campo. Preencher de acordo com o número máximo \
                      permitido para o campo.'],
               '42': ['O registo genealógico do animal tem mais de 13 caracteres.',
                      'Os dados constantes no campo referente ao registo genealógico do animal excedem o \
                      número máximo permitido para o campo. Preencher de acordo com o número máximo permitido \
                      para ocampo.'],
               '43': ['A identificação da mãe tem mais de 16 caracteres.',
                      'Os dados constantes no campo referente à identificação da mãe do animal excedem o \
                      número máximo permitido para o campo. Preencher de acordo com o número máximo \
                      permitido para o campo.'],
               '44': ['A identificação do pai tem mais de 16 caracteres.',
                      'Os dados constantes no campo referente à identificação da mãe do animal excedem o \
                      número máximo permitido para o campo. Preencher de acordo com o número máximo \
                      permitido para o campo.'],
               '45': ['O nome do ficheiro não tem 28 caracteres.',
                      'O nome do ficheiro está incorrecto. Verificar e preencher de acordo com as normas \
                      referentes no "Manual de Transferência de Ficheiros de Identificação electrónica (IDE) \
                      de ovinos ecaprinos"'],
               '46': ['O NIF do nome do ficheiro é diferente do NIF pertencente ao NIFAP que está a carregar \
                       o ficheiro.',
                      'O NIF que consta no nome do ficheiro não pertence ao NIFAP para o qual está a submeter o \
                      ficheiro. Verificar o NIF no nome do ficheiro ou o NIFAP para o qual está a tentar \
                      submeter oficheiro.'],
               '47': ['O ano do nome do ficheiro é superior ao ano actual.',
                      'O campo referente ao ano constante do nome do ficheiro é superior ao ano actual. Corrigir \
                       o ano que consta no nome do ficheiro'],
               '48': ['A data constante no nome do ficheiro é superior à data actual.',
                      'O campo referente à data constante do nome do ficheiro é superior à data actual. Corrigir \
                      a data que consta no nome do ficheiro'],
               '49': ['Nome de ficheiro incorrecto!',
                      'O nome do ficheiro está incorrecto. Verificar e preencher de acordo com as normas \
                      referenciadas no "Manual de Transferência de Ficheiros de Identificação electrónica \
                      (IDE) de ovinos e caprinos"'],
               '50': ['Ficheiro existente em outro documento.',
                      'O ficheiro que está a tentar submeter já existe em outro documento de iDigital. Verificar o \
                      ficheiro que está a tentar submeter vs os ficheiros já existentes em outros documentos de \
                      iDigital para o mesmo NIFAP.'],
               '51': ['O número de campos do ficheiro é inválido.',
                      'O número de campos existentes no ficheiro que está a tentar submeter está incorrecto. \
                      Verificar os campos no ficheiro de acordo com as normas referenciadas no "Manual de \
                      Transferência de Ficheiros de Identificação electrónica (IDE) de ovinos e caprinos"'],
               '52': ['O tamanho da linha no ficheiro é inválido.',
                      'O númersubmeter está incorrecto. Verificar os campos no ficheiro de o de campos existentes \
                      no ficheiro que está a tentar acordo com as normas referenciadas no "Manual de Transferência \
                      de Ficheiros de Identificação electrónica (IDE) de ovinos e caprinos"'],
            
                }