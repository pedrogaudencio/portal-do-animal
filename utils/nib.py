# -*- coding: UTF-8 -*-
import string

class Nib():
    '''
    Nib validator
    '''
    def validate(self, nib):
        table = ( 73, 17, 89, 38, 62, 45, 53, 15, 50, 5, 49, 34, 81, 76, 27, 90, 9, 30, 3 )
        
        nib = self._intList(nib)
        
        if len(nib) != 21:
            return 'Nib tem de ter 21 algarismos.'
        
        a = nib[-2] * 10 + nib[-1]
        b = self._sumLists(table, nib[:-2]) % 97

        return a == (98 - b)
    
    
    def _sumLists(self, a, b):
        '''
        Return the product of two lists
        '''
        val = 0
        
        for i in map(lambda a,b: a*b, a, b):
            val += int(i)

        return val
    
    
    def _intList(self, number):
        '''
        Return a list of ints streaping spaces and chars
        '''
        
        listInt = []
        
        for i in number: 
            if i in string.digits:
                listInt.append(int(i))
                
        return listInt
        
        
        