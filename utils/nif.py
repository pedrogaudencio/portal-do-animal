# -*- coding: UTF-8 -*-
import string

class Nif():
    '''
    NIF validator
    '''
    def validate(self, nif, country):
        menssage = ''
        nif = self._intList(nif)
        
        if country == 'Portugal':
            nif_size = 9
            menssage = self._controlNIF(nif, nif_size, 'Portugal')
            
            if menssage == True:
                menssage = self._checkDigitMod11(nif, 1)
               
        return menssage
        
    def _controlNIF(self, nif, nif_size, country):
        '''
        Validate length.
        Validate first number: 
            - Portugal:
                1 ou 2: pessoa singular;
                5: pessoa colectiva;
                6: pessoa colectiva pública;
                8: empresário em nome individual (deixou de ser utilizado);
                9: pessoa colectiva irregular ou número provisório.
        '''
        
        if len(nif) != nif_size:
            return 'NIF tem de ter 9 algarismos'
        
        if country == 'Portugal':
            if str(nif[0]) not in '125689':
                return 'Verifique o primeiro algarismo' 
        
        return True
    
    def _checkDigitMod11(self, number, check_position):
        '''
        Validate check digit
        '''
        summ = 0

        for pos, dig in enumerate((number)[:-1]):
            summ += int(dig) * (len(number) - int(pos))
        
        if (summ % 11 and (11 - summ % 11) % 10) == int(number[-check_position]):
            return True
        else :
            return 'Insira um número de contribuinte válido.'


    def _intList(self, number):
        '''
        Return a list of ints streaping spaces and chars
        '''
        
        listInt = []
        
        for i in number: 
            if i in string.digits:
                listInt.append(int(i))
                
        return listInt

