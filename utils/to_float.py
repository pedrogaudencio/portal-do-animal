# -*- coding: UTF-8 -*-

def to_float(number):
    '''
    Return the given number in a float format 
    independently of the decimal separator 
    '''
    try:
        return float(number.replace(',', '.'))
    except:
        return float(number)