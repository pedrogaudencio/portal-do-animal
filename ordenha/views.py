# -*- coding: UTF-8 -*-
from django.contrib.auth.decorators import login_required
from django.template.context import RequestContext
from django.shortcuts import render_to_response
from animal.models import Animal
from notification.models import Notice
from exploracao.models import Farm
from ordenha.models import Storage, MilkingTime, Milking, Lactation, Contrast, Analysis, MilkYield 
from ordenha.forms import AddContrastForm, AddLactationForm, AddMilkingForm,\
                          AddMilkingTimesForm, AddMilkStorage, MilkYieldForm, AnalysisForm, EditContrastForm
from django.forms.formsets import formset_factory
from django.utils.functional import curry
from utils.to_float import to_float
from django.db.models import Avg, Sum
from datetime import datetime, date
from ordenha.fleischmann import Fleischmann
from django.utils.encoding import force_unicode
from utils.make_initial_data import makeInitialData
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.forms.models import inlineformset_factory
from django.forms.widgets import HiddenInput
from django.contrib import messages
from django.utils.translation import ugettext as _


def _make_paramenter_data(parameter, contrast):
    '''
    Make a dictionary with paramenter's information
    '''
    data = {}
        
    try:
        analysis = parameter.analysis_set.get(contraste =contrast, parameter = parameter)
        data[parameter.formName()] = analysis.value
    except:
        data[parameter.formName()] = 'ERRO'
        
    return data
    
    
def _make_yield_data(milk_yield, contrast):
    '''
    Make a dictionary with contrast's measures information
    '''
    data = {}
    print milk_yield
    
    #cValue.quantity = self.cleaned_data[str(milking_time.id)]
    #cValue.measure_type = self.cleaned_data[str(milking_time.id) + '_measure_type']
    try:
        data[str(milk_yield.milking_time.id)] = milk_yield.quantity
        data[str(milk_yield.milking_time.id) + '_measure_type'] = milk_yield.measure_type
    except:
        data[str(milk_yield.milking_time.id)] = 'Erro'


    return data
        

@login_required
def viewMilkSorage(request, slug = None):
    #user = request.user
    farm = Farm.objects.get(slug = slug)
    
    context = {'storages': Storage.objects.filter(farm = farm),
            }
    
    return render_to_response('ordenha/milk_storage.html', context)
    
    
@login_required
def milkStorage(request, slug = None, storage_pk = None):
    #user = request.user
    
    farm = Farm.objects.get(slug = slug)
    try:    
        instance = Storage.objects.get(pk = storage_pk)
        initial_data = None
    except:
        instance = None  
        initial_data = {'farm': farm}
 
    notices = Notice.objects.filter(recipient = request.user)

    if request.method == 'POST':
        if 'submit' in request.POST:
            form = AddMilkStorage(request.POST, instance = instance)
            if form.is_valid():
                form.save()
                context = {'notices': notices,} #TODO: colocar isto no return
                return HttpResponseRedirect(reverse('storage_list', kwargs={'slug': farm.slug}))
    else:
        form = AddMilkStorage(initial = initial_data, instance = instance)

    context = {'form': form,
               }
    return render_to_response('ordenha/milk_storage_add.html', context,
                              context_instance=RequestContext(request))


@login_required
def viewMilkingTimes(request, slug = None):
    #user = request.user
    farm = Farm.objects.get(slug = slug) #FIX id

    context = {'milking_times': MilkingTime.objects.filter(farm = farm)
               }
    
    return render_to_response('ordenha/milking_times.html', context)


@login_required
def milkingTimes(request, slug = None, milking_times_pk = None):
    #user = request.user    
    farm = Farm.objects.get(slug = slug)
    
    try:
        initial_data = makeInitialData(MilkingTime.objects.get(pk = milking_times_pk))
    except:
        initial_data = None
        
    if request.method == 'POST':
        if 'submit' in request.POST:
            form = AddMilkingTimesForm(farm, milking_times_pk, request.POST)
            if form.is_valid():
                form.save()        
                return HttpResponseRedirect(reverse('times_list', kwargs={'slug': farm.slug}))
    else:
        form = AddMilkingTimesForm(farm, milking_times_pk, initial = initial_data)
    
    
    context = {'form': form,
               }
    
    return render_to_response('ordenha/milking_times_add.html', context,
                              context_instance=RequestContext(request))


@login_required
def viewMilking(request, slug = None, animal_pk = None):
    #user = request.user
    farm = Farm.objects.get(slug = slug)
    try:
        animal = Animal.objects.get(id = animal_pk) #FIX
    except:
        pass
    quantity_average = 0
    total_milking  = 0
    
    try:
        lasts_milking = Milking.objects.filter(farm = farm).order_by('milking_date')
        fist_milking = lasts_milking[0]
        lasts_milking = lasts_milking[:3]
    except:
        lasts_milking = None
    #print datetime.strptime(fist_milking.milking_date, "%d")
    #print lasts_milking[:3]
    
    day_prodution = _get_day_production(farm)
    
    print day_prodution['quantity__sum']
    #print Milking.objects.filter(farm = farm, milking_date = datetime.today()).aggregate(Sum('quantity'))
    
    
    #print Milking.objects.values('milking_date').annotate(Avg('quantity'))
    #print Milking.objects.values('milking_time').annotate(Avg('quantity'))
    try:
        avg_day_production = Milking.objects.filter(farm = farm).aggregate(Avg('quantity'))
        avg_day_production = int(avg_day_production['quantity__avg']) * 2 
    except:
        avg_day_production = None
        
        
    
    context = {'lasts_milking': lasts_milking,
               'quantity_average': round(quantity_average, 2),
               #'data_start_date': datetime.strptime(fist_milking.milking_date), 
               'day_prodution': day_prodution['quantity__sum'],
               'avg_day_production': avg_day_production,
               
               }

    return render_to_response('ordenha/view.html', context)


def _get_day_production(farm, date = None, animal = None):
    '''
    Get milk production for one day.
    If date = None, assume today
    If animal = None, assume production for farm
    '''
    if not date:
        date = datetime.today()

    if animal:
        production = Milking.objects.filter(farm = farm, 
                                            animal = animal, 
                                            milking_date = date).aggregate(Sum('quantity'))
    else:
        production = Milking.objects.filter(farm = farm, 
                                            milking_date = date).aggregate(Sum('quantity'))
    
    return production

"""
@login_required
def modifyMilking(request, slug = None, milking_pk = None):
    '''
    View for milking visualization and changes
    '''
    #user = request.user
    farm = Farm.objects.get(slug = slug)
    is_formset = False
    form = None
    
    try:
        initial_data = makeInitialData(Milking.objects.get(pk = milking_pk))
    except:
        initial_data = None
         
    modifyMilkingForm = AddMilkingForm(milking_pk = milking_pk, farm = farm, initial = initial_data)

    notices = Notice.objects.filter(recipient = request.user, unseen = True)
    
    if request.method == 'POST':
        if 'submit' in request.POST:
            form = AddMilkingForm(request.POST, milking_pk = milking_pk, farm = farm)
            if form.is_valid():
                form.save()
                context = {'notices': notices,}
                return render_to_response('done.html', context)  
    else: 
        form = modifyMilkingForm

    context = {'form': form,
               'is_formset': is_formset,
               }

    return render_to_response('ordenha/add.html', context,
                              context_instance=RequestContext(request))
"""

@login_required
def addMilking(request, slug = None, milking_pk = None):
    
    notices = Notice.objects.filter(recipient = request.user, unseen = True)
    farm = Farm.objects.get(slug = slug)
    try:
        initial_data = makeInitialData(Milking.objects.get(pk = milking_pk))
        is_add = True
    except:
        initial_data = None
        is_add = True
    
    
    if request.method == 'POST':
        form = AddMilkingForm(request.POST, farm = farm, milking_pk = milking_pk)
        if 'submit' in request.POST and form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('milking_list_all', 
                                                    kwargs={'slug': farm.slug,
                                                            }))
    else:
        form = AddMilkingForm(farm = farm, milking_pk = milking_pk, initial = initial_data)
    
    context = {'form': form,
               'is_add': is_add,
               'notices': notices,
               }
    return render_to_response('ordenha/add.html', context,
                              context_instance=RequestContext(request))
"""
@login_required
def addMilking(request, slug = None):
    #user = request.user
    farm = Farm.objects.get(slug = slug)
    is_formset = True
    
    addMilkingFormset = formset_factory(AddMilkingForm)
    addMilkingFormset.form = staticmethod(curry(AddMilkingForm, farm = farm))

    notices = Notice.objects.filter(recipient = request.user, unseen = True)
    
    if request.method == 'POST':
        if 'add_new' in request.POST:
            cp = request.POST.copy()
            cp['form-TOTAL_FORMS'] = int(cp['form-TOTAL_FORMS'])+1
            formset = addMilkingFormset(cp)
            
        if 'submit' in request.POST:
            formset = addMilkingFormset(request.POST)
            if formset.is_valid():
                for form in formset:
                    form.save()
                context = {'notices': notices,}
                return render_to_response('done.html', context)          
    else:
        formset = addMilkingFormset()
    
    context = {'formset': formset,
               'is_formset': is_formset,
               }

    return render_to_response('ordenha/add.html', context,
                              context_instance=RequestContext(request))


@login_required
def modifyLactation(request, slug = None, lactation_pk = None):
    '''
    View for lactaion changes
    '''
    #user = request.user
    farm = Farm.objects.get(slug = slug)
    is_formset = False
    form = None
    initial_data = None
    #try:
    initial_data = makeInitialData(Lactation.objects.get(pk = lactation_pk))
    #except:
    #    initial_data = None 
    modifyLactationForm = AddLactationForm(lactation_pk = lactation_pk, farm = farm, initial = initial_data)
    notices = Notice.objects.filter(recipient = request.user, unseen = True)
    
    if request.method == 'POST':
        if 'submit' in request.POST:
            form = AddLactationForm(request.POST, lactation_pk = lactation_pk, farm = farm)
            if form.is_valid():
                form.save()
                context = {'notices': notices,}
                return render_to_response('done.html', context)
    else: 
        form = modifyLactationForm

    context = {'formset': form,
               'is_formset': is_formset,
               }

    return render_to_response('lactation/add.html', context,
                              context_instance=RequestContext(request))
"""
    

@login_required
def viewLactation(request, slug = None, animal_pk = None):
    #user = request.user
    flais_dict = []
    chart_series = []
    production = []
    
    farm = Farm.objects.get(slug = slug)
    animal = Animal.objects.get(id = animal_pk)
    lactations = Lactation.objects.filter(farm = farm, animal = animal).order_by('-start_date', '-lact_number')
    #print "Lactacões:" + str(lactations)
    
    #print lactations[0].production_total()
    #print lactations[0].production_list()
    #print lactations[0].production_total
    #print lactations[0].total_milk
    
    #list with lactactions dict
    for lactation in lactations:
        #print lactation
        #prod_list = Fleischmann(lactation).production_list()
        flais_dict.append(lactation.production_list())
        print 'Flaischmann: ' + str(flais_dict)
        production.append(lactation.production_total())
        
    
    #average of lactations/contrasts
    avg_list = _get_average_lists(map(lambda d:d['production'], flais_dict))
    
    #Put everything on a list for the chart
    chart_series = _make_chart_line_series("Ultima lactação", flais_dict[0]['production'], chart_series)
    chart_series = _make_chart_line_series('Média', avg_list, chart_series)
    
    
    #print Fleischmann(lactations[0]).production_total(flais_dict[0]['production'])
    
    #reverse of production list
    production.reverse()
    production_avg = [sum(production)/len(production)] * len(production)
    
    
    context = {'lactations': lactations, 
               'animal': animal,
               'xAxis': flais_dict[0]['label'],
               'series': chart_series,
               'production': production,
               'production_avg': production_avg,
               }

    return render_to_response('lactation/view.html', context)


def _get_average_lists(lists):
    '''
    Return average between lists: [[2,2,4],[1,2,4]] = [1.5,2,4] 
    '''
    avg_list = []
    
    for values in zip(*lists):
        avg_list.append(sum(values)/len(lists))

    return avg_list


def _make_chart_line_series(name, data, series = []):
    '''
    Return info for line chart 
    '''    
    
    series.append({'name': name, 'data':data})
    
    return series


@login_required
def addLactation(request, slug = None, lactation_pk = None):
    """
    Add or edit a lactation
    """
    #farm
    farm = Farm.objects.get(slug = slug)
    
    try:
        lactation = Lactation.objects.get(pk = lactation_pk)
        initial_data = makeInitialData(lactation)
        lactation_pk = lactation.id
    except:
        initial_data = None
        lactation_pk = None
    
    if request.method == 'POST':
        form = AddLactationForm(request.POST, farm = farm, lactation_pk = lactation_pk)
        if ('submit' or 'submit_add_other' in request.POST) and form.is_valid():
            #method save returns animal, auto_dry and start_date end_date info
            animal, auto_dry, start_date , dry_date = form.save()
            messages.add_message(request, messages.SUCCESS, 
                                     _('Lactation for animal: %s, added with success') % animal)
            
            if 'submit_add_other' in request.POST:
                form = AddLactationForm(farm = farm, initial = {'start_date': start_date,
                                                                'auto_dry': auto_dry,
                                                                'dry_date': dry_date,
                                                                })
            else:
                return HttpResponseRedirect(reverse('lactation_list', 
                                                    kwargs={'slug': farm.slug,
                                                            'animal_pk': animal.id,
                                                            }))
        else:
            messages.add_message(request, messages.ERROR, 
                                 _('Fail while attempting to add lactation!')) 
        
    else: 
        form = AddLactationForm(farm = farm, initial = initial_data)
    
    context = {'farm': farm,
               'form': form,
               'is_add': True,
               }
    
    return render_to_response('lactation/add.html', context,
                                  context_instance=RequestContext(request)) 


@login_required
def viewContrast(request, slug = None, animal_pk = None):
    #user = request.user
    farm = Farm.objects.get(slug = slug)
    animal = Animal.objects.get(id = animal_pk)
    
    context = {'contrasts': Contrast.objects.filter(farm = farm, animal = animal).order_by('-date'),
               'milking_times': farm.milkingtime_set.all(),
               'animal': animal,
               }

    return render_to_response('contraste/view.html', context)


@login_required
def modifyContrast(request, slug = None, contrast_pk = None):
    """
    View for contrast changes
    """
    #farm
    farm = Farm.objects.get(slug = slug)
    #contrast
    contrast = Contrast.objects.get(pk = contrast_pk)
    #milking_times
    milking_times = farm.milkingtime_set.all()
    #analysis parameters
    preference_parAnalyzed = farm.preference_set.get(preferenceType = 'ParAnalyzed')
    #contrasts formset
    CollectionFormSet = inlineformset_factory(Contrast, MilkYield,
                                              form = MilkYieldForm, max_num = len(milking_times))
    #analysis formset
    AnalysisFormSet = inlineformset_factory(Contrast, Analysis, 
                                            form = AnalysisForm, 
                                            max_num = len(eval(preference_parAnalyzed.data)))
    
    if request.method == 'POST':
        if 'submit' in request.POST:
            form = EditContrastForm(request.POST, instance = contrast)
            collection_formset = CollectionFormSet(request.POST, instance = contrast)
            analysis_formset = AnalysisFormSet(request.POST, instance = contrast)
            
            if form.is_valid() and analysis_formset.is_valid() and collection_formset.is_valid():
                collection_formset.save()
                analysis_formset.save()
                form.save()
                return HttpResponseRedirect(reverse('contrast_list', 
                                                    kwargs={'slug': farm.slug,
                                                            'animal_pk': contrast.animal.id}))
    else:
        form = EditContrastForm(instance = contrast)
        collection_formset = CollectionFormSet(instance = contrast)
        analysis_formset = AnalysisFormSet(instance = contrast)

    context = {'formset': form,
               'collection_formset': collection_formset,
               'analysis_formset': analysis_formset,
               'contrast': contrast,
               }

    return render_to_response('contraste/add.html', context,
                              context_instance=RequestContext(request))
    

@login_required
def addContrast(request, slug = None, animal_pk = None):
    """
    View add contrasts
    """
    is_add = True
    #farm
    farm = Farm.objects.get(slug = slug)
        
    #milking_times
    milking_times = farm.milkingtime_set.all()
    #analysis parameters
    preference_parAnalyzed = farm.preference_set.get(preferenceType = 'ParAnalyzed')
    #contrasts formset
    CollectionFormSet = inlineformset_factory(Contrast, MilkYield,
                                              form = MilkYieldForm, max_num = len(milking_times))
    #analysis formset
    AnalysisFormSet = inlineformset_factory(Contrast, Analysis, 
                                            form = AnalysisForm, 
                                            max_num = len(eval(preference_parAnalyzed.data)))
    
    if request.method == 'POST':
        form = AddContrastForm(request.POST, farm = farm)
        collection_formset = CollectionFormSet(request.POST)
        analysis_formset = AnalysisFormSet(request.POST)
        if ('submit' or 'submit_add_other' in request.POST) and form.is_valid() \
            and analysis_formset.is_valid() and collection_formset.is_valid():
            #save new
            new_contrast = form.save()
            
            new_collection = collection_formset.save(commit = False)
            for collection in new_collection:
                collection.contrast = new_contrast
                collection.save()
            
            new_analysis = analysis_formset.save(commit = False)
            for analysis in new_analysis:
                analysis.contrast = new_contrast
                analysis.save()

            animal = new_contrast.animal    
            #Add success 
            messages.add_message(request, messages.SUCCESS, 
                                 _('Contrast for animal: %s, added with success') % animal)
            
            if 'submit_add_other' in request.POST:
                #Add initial stuff from last contrast added
                form = AddContrastForm(farm = farm, initial = {'farm': farm,
                                          'tec': new_contrast.tec,
                                          'date': new_contrast.date,
                                          })
                
                collection_formset = CollectionFormSet()
                analysis_formset = AnalysisFormSet()
            else:       
                return HttpResponseRedirect(reverse('contrast_list', 
                                                kwargs={'slug': farm.slug,
                                                        'animal_pk': animal.id}))
        else:
            messages.add_message(request, messages.ERROR, 
                                 _('Fail while attempting to add contrast!'))
    else:
        collection_formset = CollectionFormSet()
        analysis_formset = AnalysisFormSet()
        form = AddContrastForm(farm = farm, initial = {'farm': farm})

    context = {'formset': form,
               'is_add': is_add,
               'collection_formset': collection_formset,
               'analysis_formset': analysis_formset,
               }

    return render_to_response('contraste/add.html', context,
                              context_instance=RequestContext(request))