# -*- coding: UTF-8 -*-
from ordenha.models import Contrast


class Fleischmann():
    '''
    Fleischmann method 
    '''
    def __init__(self, lact, farm = None):
        #lactation
        self.lact = lact
        self.farm = self.lact.farm
        self.animal = self.lact.animal
        self.contrasts = self._get_contrasts()
        
        
        print 'Contrasts: ' + str(self.contrasts)
        #production_list = self.production_list()
        #str(self.production_total(production_list))
        

    def _get_contrasts(self):
        '''
        Get list with every contrast in a lactation period
        '''
        contrasts = []
        
        if self.lact.dry_date:
            contrasts = Contrast.objects.filter(farm = self.farm, animal = self.animal, 
                                                     date__gte = self.lact.start_date, 
                                                     date__lte = self.lact.dry_date)
        else:
            contrasts = Contrast.objects.filter(farm = self.farm, animal = self.animal, 
                                                     date__gte = self.lact.start_date)

        return contrasts.order_by('date')
    
    
    def production_list(self):
        '''
        Return list with productions results
        '''
        dict = {}
        #production list
        production = []
        label = []
        #lactação
        c2 = self.lact
        label_int = 0
        
        for contrast in self.contrasts:
            label_int += 1
            label.append('P' + str(label_int))
            #first contrast
            if contrast == self.contrasts[0]:
                days = self._get_number_days(contrast.date, c2.start_date)
                #print days
                quantity = days * contrast.total_milk()
                c2 = contrast
                production.append(round(quantity, 2))
            #other contrast
            else:
                days = self._get_number_days(contrast.date, c2.date)
                #print days
                #print list(self.contrasts.values_list('id', flat = True)).index(contrast.id)
                quantity = days * ((contrast.total_milk() + c2.total_milk()) / 2)
                c2 = contrast
                production.append(round(quantity, 2))
                #if is last contrast
                if contrast == self.contrasts[len(self.contrasts)-1]:
                    production.append(round(14 * contrast.total_milk(), 2))
                    label.append('P(secagem)')
        
        
        dict['production'] = production
        dict['label'] = label
 
        return dict
    
    
    def _get_number_days(self, d1, d2):
        '''
        Get number of day between two dates 
        '''
        delta = d1 - d2

        return delta.days
    
    
    def production_total(self, production_list):
        '''
        Total production adjusted to 305 days
        '''
        days_total = 0
        d2 = self.lact.start_date
             
        for contrast in self.contrasts:
            days_total += self._get_number_days(contrast.date, d2)
            d2 = contrast.date

        #prodution correction to 305 days
        if days_total > 305:
            #valid production
            valid_production = sum(production_list[:-1])
            #print 'VALID_prod: ' + str(valid_production)
            valid_days = days_total - 305
            #print 'VALID DAYS: ' + str(valid_days)
            #print valid_production - (valid_days * production_list[-2])
            return valid_production - (valid_days * production_list[-2])
        
        print 'days: ' +str(days_total)
        
        return sum(production_list)