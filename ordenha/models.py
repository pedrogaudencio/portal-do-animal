# -*- coding: UTF-8 -*-
from django.db import models
from django.utils.encoding import force_unicode
from utils.to_float import to_float
from django.utils.translation import ugettext as _
from transmeta import TransMeta
from django.template.defaultfilters import slugify
from django.core.urlresolvers import reverse

'''
Managers
'''
class ActiveManager(models.Manager):
    def get_query_set(self):
        return super(ActiveManager, self).get_query_set().filter(active = True)


'''
Models
'''
class Lactation(models.Model):
    farm = models.ForeignKey('exploracao.Farm', blank = False, null = False)
    animal = models.ForeignKey('animal.Animal', blank = False, null = False)
    start_date = models.DateField(_('Lactation starting date'), blank = True, null = True)
    lact_number = models.PositiveIntegerField(_('Lactation number'), default = 0, 
                                                help_text= _('Number of lactations for the animal'), 
                                                blank = True, null = True)
    auto_dry = models.BooleanField(_('Close automatically'), default = False)
    dry_date = models.DateField(_('Dry date'), blank = True, null = True)
    obs = models.TextField(blank = True, null = True)
    
    '''
    def save(self, *args, **kwargs):     
        self.numero_lactao += 1
        super(Lactacao, self).save(*args, **kwargs)
    '''   
     
    def __unicode__(self):
        return '# %s %d - %s' % (self.animal, self.lact_number, self.start_date)
    
    
    def get_absolute_url(self):
        return '../%s/edit' % self.pk
    
    
    class Meta():
        verbose_name = _('Lactation')
        verbose_name_plural = _('Lactations')


    def production_list(self):
        '''
        Production dictionary, calculated with Fleischmann method
        '''
        from ordenha.fleischmann import Fleischmann
        return Fleischmann(self).production_list()


    def production_total(self):
        """
        Total production ajusted to 305 days
        """
        from ordenha.fleischmann import Fleischmann
        p_list = Fleischmann(self).production_list()
        return Fleischmann(self).production_total(p_list['production'])


class MilkingTime(models.Model):
    """
    Periodos de ordenha
    """
    farm = models.ForeignKey('exploracao.Farm', blank = False, null = False)
    name = models.CharField(_('Name'), max_length = 150, blank = False, null = False)
    start_time = models.TimeField(_('Start hour'), blank = True, null = True)
    end_time = models.TimeField(_('End hour'), blank = True, null = True)
    obs = models.TextField(_('Observations'), blank = True, null = True)

    active = models.BooleanField(_('Active'), default = True)
    objects = models.Manager()
    is_active = ActiveManager()


    def __unicode__(self):
        return '%s (%s - %s)' % (self.name, self.start_time, self.end_time)


    def get_absolute_url(self):
        return '%s/edit' % self.pk


    class Meta():
        verbose_name = _('Milking time')
        verbose_name_plural = _('Milking times')


class Milking(models.Model):
    farm = models.ForeignKey('exploracao.Farm', blank = False, null = False)   
    animal = models.ForeignKey('animal.Animal', blank = False, null = False)
    employee = models.ForeignKey('utilizador.Staff', blank = True, null = True)
    quantity = models.CharField(_('Production'), max_length = 6, blank = True, null = True)
    milking_start_date = models.DateField(_('Data de inicio da ordenha'), blank = True, null = True)
    milking_date = models.DateField(_('Milking date'), blank = False, null = False)
    milking_hour = models.TimeField(_('Milking hour'), blank = True, null = True)
    milking_time = models.ForeignKey('MilkingTime', blank = False, null = False)

    milking_number = models.PositiveIntegerField(_('Milking number'), default = 0, 
                                                 help_text= _('Number of times this animal has been milked'), 
                                                 blank = True)
    tank = models.ForeignKey('Storage', blank = True, null = True) 
    lactation = models.ForeignKey('ordenha.Lactation')
    obs = models.TextField(_('Observations'), blank = True, null = True)

    '''
    def save(self, *args, **kwargs):     
        self.numero_ordenha += 1
        super(Ordenha, self).save(*args, **kwargs)
    '''

    def __unicode__(self):
        return '# %s %d | Data: %s' % (self.animal, self.milking_number, str(self.milking_date))


    def get_absolute_url(self):
        return '%s/edit' % self.pk


    class Meta():
        verbose_name = _('Milking')
        verbose_name_plural = _('Milking')


class Storage(models.Model):
    """
    Local de armazenamento do produto
    """
    farm = models.ForeignKey('exploracao.Farm', blank = False, null = False)
    name = models.CharField(_('Name'), max_length = 150, blank = False, null = False)
    local = models.CharField(_('Local'), max_length = 500, blank = True, null = True)
    max_load = models.IntegerField(_('Capacity'), blank = True, null = True)
    obs = models.TextField(_('Observations'), blank = True, null = True)
    
    slug = models.SlugField(max_length = 50, null = False, blank = True)
    

    def __unicode__(self):
        return '%s - %s' % (self.name, self.local)   

    
    def save(self):
        if not self.id:
            self.slug = slugify(self.farm.trademark)
        super(Storage, self).save()
    
    
    def get_absolute_url(self):
        return '%s/edit' % self.pk
        #return reverse('storage_edit', args={'slug': self.slug,
        #                                      'storage_pk': self.pk})
        
        
    class Meta():
        unique_together = ('farm','name','local')
        verbose_name = _('Storage')
        verbose_name_plural = _('Storage')


class Contrast(models.Model):
    """
    Contraste leiteiro
    """
    farm = models.ForeignKey('exploracao.Farm', verbose_name = 'Farm', blank = False, null = False)
    animal = models.ForeignKey('animal.Animal', blank = False, null = False)
    tec = models.ForeignKey('utilizador.Staff', blank = True, null = True) # ?
    date = models.DateField(_('Contrast date'), blank = False, null = False)
    bottle = models.CharField(_('Container for analysis'), max_length = 50, blank = True, null = True)
    #ficheiro = models.FileField('Ficheiro', blank = True, null = True)
    obs = models.TextField(_('Observations'), blank = True, null = True)    
    
    
    def __unicode__(self):
        return '%s %s' % (self.animal, self.date)
    
    
    class Meta():
        unique_together = ('animal', 'date')
        verbose_name = _('Contrast')
        verbose_name_plural = _('Contrasts')

    
    def total_milk(self):
        """
        Returns milk total on the contrast
        """
        milk_total = 0
        
        for milk_yield in self.milkyield_set.all():
            milk_total += to_float(milk_yield.quantity)
    
        return milk_total

    
    def get_milkYield_by_milking_time(self):
        """
        Return array with quantities by milking_time, if milking_time x didn't have a quantity 
        value return None on that milkin_time.
        """ 
        quantity_list = []
        
        for milking_time in self.farm.milkingtime_set.all():
            try:
                quantity_list.append(self.milkyield_set.get(milking_time = milking_time))
            except:
                quantity_list.append(None)
        return quantity_list
            
            
    def get_absolute_url(self):
        return '../%s/edit' % self.pk
    

class MilkYield(models.Model):
    measure_type_choices= (('l', _('L')),('kg', _('Kg')))
    
    contrast = models.ForeignKey('Contrast')
    milking_time =  models.ForeignKey('MilkingTime', verbose_name = _('Milking time'), 
                                      blank = True, null = True)
    quantity = models.CharField(_('Production'), max_length = 6, blank = True, null = True)
    measure_type = models.CharField(_('Measure'), choices = measure_type_choices, 
                                    max_length = 20, default = 'kg', blank = True, null = True)


    def __unicode__(self):
        return '%s - %s - %s' % (force_unicode(self.contrast), self.milking_time, self.quantity)
  
    
    class Meta():
        unique_together = ('contrast', 'milking_time')
        verbose_name = _('Colheita para contraste ')
        verbose_name_plural = _('Colheitas para contraste')


class Analysis(models.Model):
    contrast = models.ForeignKey('Contrast')
    parameter = models.ForeignKey('ParametersAnalyzed')
    value = models.CharField(_('Value'), max_length = '30', blank = True, null = True)


    def __unicode__(self):
        return '%s - %s - %s' % (self.contrast, force_unicode(self.parameter), self.value)


    class Meta():
        unique_together = ('contrast', 'parameter')
        verbose_name = _('Analysis')
        verbose_name_plural = _('Analysis')


class ParametersAnalyzed(models.Model):
    """
    Outros dados analisados nos contrastes leiteiros 
    """
    __metaclass__ = TransMeta
    name = models.CharField(_('Name'), max_length = 150, blank = False, null = False)


    def __unicode__(self):
        return '%s' % force_unicode(self.name)


    def formName(self):
        return 'parameter' + unicode(self.pk)


    class Meta():
        verbose_name = _('Parameter')
        verbose_name_plural = _('Parameters')
        translate = ('name',)



    '''
    materia_gorda = models.PositiveIntegerField('Matéria gorda', blank = True, null = True)
    materia_proteica = models.PositiveIntegerField('Matéria proteica', blank = True, null = True)
    extrato_seco = models.PositiveIntegerField('Extrato seco', blank = True, null = True) 
    celulas_somaticas = models.PositiveIntegerField('Células somáticas', blank = True, null = True)
    microorganismos = models.PositiveIntegerField('Micro organismos', blank = True, null = True)
    inibidores = models.PositiveIntegerField('Inibidores', blank = True, null = True)
    ureia = models.PositiveIntegerField('Ureia', blank = True, null = True)
    lactose = models.PositiveIntegerField('Lactose', blank = True, null = True)
    ponto_congelacao = models.PositiveIntegerField('Ponto congelação', blank = True, null = True)
    inibidores_camara = models.PositiveIntegerField('Inibidores de camara', blank = True, null = True)
    '''