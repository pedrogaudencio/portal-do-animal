# -*- coding: UTF-8 -*-
from django.forms.models import ModelForm, ModelChoiceField
from django.forms.formsets import BaseFormSet
from django import forms
from ordenha.models import Contrast, Analysis, Lactation, MilkingTime, Milking, Storage, MilkYield
from animal.models import Animal
from identificacao.models import Identifier
from preferences.models import Preference
from ordenha.close_lact import CloseLact
from utilizador.models import UserProfile, Staff
from utils.get_or_none import get_or_none
from utils.to_float import to_float
from django.db.models import Q
import datetime
from django.shortcuts import get_object_or_404
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from utils.widgets import DatePickerWidget, ChosenSingleWidget
from django.utils.translation import ugettext as _
from django.forms.widgets import HiddenInput, TimeInput


class ModelChoiceField_NEW_unicode(ModelChoiceField):
    def label_from_instance(self, obj):
        """
        This method is used to convert objects into strings; it's used to
        generate the labels for the choices presented by this object. Subclasses
        can override this method to customize the display of the choices.
        """
        if obj.sia:
            return (obj.sia.number)
        else:
            return (obj)


class AddMilkStorage(ModelForm):
    """
    Storage form
    """
    class Meta:
        model = Storage
        widgets = {'farm': HiddenInput(),
                   'obs': forms.Textarea(attrs={'rows':2, 'cols':40}),
                   }


class AddMilkingTimesForm(forms.Form):
    """
    Periodos de Milking form
    """
    
    def __init__(self, farm, milking_times_pk, *args, **kwargs):
        self.farm = farm
        self.milking_times_pk = milking_times_pk
        super(AddMilkingTimesForm, self).__init__(*args, **kwargs)
        
    name = forms.CharField(label = _('Designation'))
    start_time = forms.TimeField(label = _('Start hour'), input_formats = ['%H:%M', '%H', '%H:%M:%S'], 
                                 required = False)
    end_time = forms.TimeField(label = _('End hour'), input_formats = ['%H:%M', '%H', '%H:%M:%S'], 
                               required = False)
    obs = forms.CharField(label = _('Observation'), required = False, 
                          widget = forms.Textarea(attrs={'rows':2, 'cols':40}))
    
    
    def clean_name(self):
        """
        Name as to be unique on the farm
        """
        name = self.cleaned_data['name']
        
        try:
            m_times = MilkingTime.objects.filter(name = name, farm = self.farm)
            if len(m_times) == 0:
                return name
            if m_times[0].id == int(self.milking_times_pk):
                return name
            else:
                raise forms.ValidationError
        except:
            raise forms.ValidationError(_('Already exist one milking time with this name.'))
        
        return name
    
    
    def save(self):
        """
        Save milking_times, replace an old one with new information, or add a new
        """
        if self.milking_times_pk:
            mTime =  get_object_or_404(MilkingTime, id = self.milking_times_pk)
        else:
            mTime = MilkingTime()
            
        mTime.farm = self.farm
        mTime.name = self.cleaned_data['name']
        mTime.start_time = self.cleaned_data['start_time']
        mTime.end_time = self.cleaned_data['end_time']
        mTime.obs = self.cleaned_data['obs']
         
        mTime.save()


class AddContrastForm(forms.ModelForm):
    """
    Form for contrasts
    """
    def __init__(self, *args, **kwargs):
        self.farm = kwargs.pop('farm', None)
        super(AddContrastForm, self).__init__(*args, **kwargs)
        
        self.fields['animal'] = ModelChoiceField_NEW_unicode(Animal.actives.filter(farm = self.farm), 
                                                            label = _('Animal'), 
                                                            widget = ChosenSingleWidget)
    
    def clean_animal(self):
        """
        Verify if the animal belongs to that farm
        """
        animal = self.cleaned_data['animal']
        #Search for a active lactation
        lactations = Lactation.objects.filter(farm = self.farm, 
                                              animal = animal).order_by('-start_date')
        last_lactation = lactations[:1]

        if len(last_lactation) > 0:
            if last_lactation[0].dry_date != None:
                raise forms.ValidationError(_('Nao existe lactacao aberta para o animal: ')
                                    + str(animal) + ' . A ultima lactacao deste animal \
                                    terminou a :' + str(last_lactation[0].dry_date))
        else:
            raise forms.ValidationError(_('Este animal nunca teve lactacao aberta'))
        return animal
    
    
    class Meta:
        model = Contrast
        exlude = ['animal']
        widgets = {'farm': HiddenInput(),
                   'date': DatePickerWidget(),
                   'obs': forms.Textarea(attrs={'rows':2, 'cols':40}),
                   }


class EditContrastForm(forms.ModelForm):
    """
    Form for contrasts
    """    
    class Meta:
        model = Contrast
        widgets = {'farm': HiddenInput(),
                   'date': DatePickerWidget(),
                   'animal': HiddenInput(),
                   'obs': forms.Textarea(attrs={'rows':2, 'cols':40}),
                   }


class MilkYieldForm(ModelForm):
    
    class Meta:
        model = MilkYield
        widgets = {'contrast': HiddenInput(),
                   'milking_time': forms.Select(attrs={'class': 'input-block-level'}),
                   'quantity': forms.TextInput(attrs={'class': 'input-block-level'}),
                   'measure_type': forms.Select(attrs={'class': 'input-block-level'}),
                   }
        

class AnalysisForm(ModelForm):
    
    class Meta:
        model = Analysis
        widgets = {'contrast': HiddenInput(),
                   'parameter': forms.Select(attrs={'class': 'input-block-level'}),
                   'value': forms.TextInput(attrs={'class': 'input-block-level'}),
                   }
    

class LactationForm(forms.Form):
    """
    Base Form for lactations
    """
    def __init__(self, *args, **kwargs):
        self.farm = kwargs.pop('farm', None)
        self.lactation_pk = kwargs.pop('lactation_pk', None)
        super(LactationForm, self).__init__(*args, **kwargs)
    
        self.fields['animal'] = ModelChoiceField_NEW_unicode(Animal.actives.filter(farm = self.farm), 
                                                            label = _('Animal'), 
                                                            widget = ChosenSingleWidget)
        
        self.fields['start_date'] = forms.DateField(label = _('Start date'), widget = DatePickerWidget())
        self.fields['auto_dry'] = forms.BooleanField(label = _('Auto dry'), required=False)
        self.fields['dry_date'] = forms.DateField(label = _('End date'), widget = DatePickerWidget() , required = False)
    
        self.fields['obs'] = forms.CharField(label = _('Observation'), required = False, 
                          widget = forms.Textarea(attrs={'rows':2, 'cols':40}))


class AddLactationForm(LactationForm):
    """
    Form to add or modify a lactation
    """        
    def clean_start_date(self):
        """
        Clean start_date field 
        """
        try:
            animal = self.cleaned_data['animal']
        except:
            raise forms.ValidationError(_('Verifique o campo animal'))
        print animal
        
        start_date = self.cleaned_data['start_date']
        
        #Search for a active lactation
        lactations = Lactation.objects.filter(farm = self.farm, animal = animal).order_by('-start_date')
        last_lactation = lactations[:1]
                
        #for a new lactation
        if not self.lactation_pk:
            #Verify if start_date is after the last dry date 
            try:
                if len(last_lactation) < 1:
                    return start_date
                if last_lactation[0].dry_date == None:
                    raise forms.ValidationError(_('Ja existe uma lactacao aberta para o animal: ')
                                                + str(animal) + ' .')
                if last_lactation[0].dry_date > start_date:
                    raise forms.ValidationError(_('Fecho da ultima lactacao: %s') % str(last_lactation[0].dry_date))
            except:
                raise
        #to modify a lactation
        else:
            lact = lactations.get(id = self.lactation_pk)
        
            if lact.dry_date:        
                if lact.id != 1:
                    try:
                        Lactation.objects.get(Q(dry_date__lt = start_date), Q(start_date__lte = lact.dry_date),
                                              farm = self.farm, animal = animal)
                    except:
                        raise forms.ValidationError(_('Existe uma lactacao aberta nessa data.'))
            else:
                if len(Lactation.objects.filter(farm = self.farm, animal = animal, 
                                               start_date__gt = start_date).exclude(id = self.lactation_pk)) != 0:
                    raise forms.ValidationError(_('Existe uma lactacao aberta nessa data.'))
                
                if len(Lactation.objects.filter(farm = self.farm, animal = animal, 
                                                       dry_date__gt = start_date).exclude(id = self.lactation_pk)) != 0:
                    raise forms.ValidationError(_('Existe uma lactacao aberta nessa data.'))
                        
        return start_date
           
            
    def clean_dry_date(self):
        """
        Clean dry_date field
        """
        try:
            animal = self.cleaned_data['animal']
        except:
            raise forms.ValidationError(_('Verifique o campo animal'))
        
        dry_date = self.cleaned_data['dry_date']
        
        try:    
            start_date = self.cleaned_data['start_date']
        except:
            raise forms.ValidationError(_('Verifique o campo data de inicio'))
    
        '''
        Verify if start_date is before dry_date
        '''
        print self.lactation_pk
        
        if self.lactation_pk:
            try:
                lact = Lactation.objects.get(id = self.lactation_pk)
            except:
                raise forms.ValidationError(_('Erro - Lactação não encontrada'))
        
        if dry_date != None:
            if start_date > dry_date:
                raise forms.ValidationError(_('A data de secagem tem de ser posterior á data de inicio da lactação'))  
            try:
                if len(Lactation.objects.filter(farm = self.farm, animal = animal, 
                                           start_date__lte = dry_date).exclude(start_date__lte = lact.start_date)) != 0:                 
                    raise forms.ValidationError(_('Existe uma lactação aberta nessa data'))
            except forms.ValidationError:
                raise forms.ValidationError(_('Existe uma lactação aberta nessa data'))
            except:
                raise forms.ValidationError('Erro 3')
        
        return dry_date


    def save(self):
        '''
        Save lactation. Replace an old one with new information, or add a new
        '''
        
        if self.lactation_pk:
            lact =  get_object_or_404(Lactation, id = self.lactation_pk)
        else:
            lact = Lactation()
        
        lact.farm = self.farm
        
        lact.animal = animal = self.cleaned_data['animal']

        lact.start_date = self.cleaned_data['start_date']
        
        if self.lactation_pk:
            lact_number = lact.lact_number
        else:
            try:
                last_lact = Lactation.objects.filter(animal = animal).order_by('id').reverse()[:1]
                lact_number = last_lact[0].lact_number + 1
            except:
                lact_number = 1
        
        lact.lact_number = lact_number
        lact.auto_dry = self.cleaned_data['auto_dry']
        lact.dry_date = self.cleaned_data['dry_date']
        lact.obs = self.cleaned_data['obs']
        
        lact.save()
        
        #Return animal, auto_dry and date
        return animal, lact.auto_dry, lact.start_date, lact.dry_date

    

class MilkingForm(forms.Form):
    '''
    Base Form for milkings
    '''
    def __init__(self, *args, **kwargs):
        self.farm = kwargs.pop('farm')
        self.milking_pk = kwargs.pop('milking_pk', None)

        super(MilkingForm, self).__init__(*args, **kwargs)
    
    
        self.fields['animal'] = ModelChoiceField_NEW_unicode(Animal.actives.filter(farm = self.farm), 
                                                            label = _('Animal'), 
                                                            widget = ChosenSingleWidget)
        
        self.fields['milking_date'] = forms.DateField(label = _('Milking date'), 
                                       widget = DatePickerWidget(attrs={'class': 'input-block-level'}))
        
        self.fields['milking_hour'] = forms.TimeField(label = _('Milking hour'), 
                                       widget = TimeInput(attrs={'class': 'input-block-level'}),
                                       input_formats = ['%H:%M', '%H', '%H:%M:%S'], required = False)
        try:
            self.fields['employee'] = \
                forms.ModelChoiceField(Staff.objects.filter(breeder = self.farm.breeder),
                                       label = _('Employee'), required=False)
        except:
            pass
        
        try:
            self.fields['milking_time'] = \
                forms.ModelChoiceField(MilkingTime.objects.filter(farm = self.farm),
                                       label = _('Milking time'))
        except:
            pass
        
        try:
            self.fields['tank'] = \
                forms.ModelChoiceField(Storage.objects.filter(farm = self.farm), 
                                       label = _('Storage'), required = False)
        except:
            pass
    
        self.fields['quantity'] = forms.CharField(label = _('Production'))
        #TODO: colocar measure
        self.fields['obs'] = forms.CharField(label = _('Observation'), required = False, 
                              widget = forms.Textarea(attrs={'rows':2, 'cols':40}))
    
     
class AddMilkingForm(MilkingForm):
    """
    Form to add or modify a milking
    """
    def clean_animal(self):
        '''
        Verify if the animal belongs to that farm
        '''
        animal = self.cleaned_data['animal']
        
        if not self.milking_pk:
            """
            If this is a new milking(milking_pk = None) search for a active lactation 
            """        
            try:
                lactations = Lactation.objects.filter(farm = self.farm, 
                                                      animal = animal).order_by('-start_date')
                last_lactation = lactations[:1]
                last_lact = last_lactation[0].dry_date
            except forms.ValidationError:
                raise forms.ValidationError('Não existe lactação aberta para o animal: '
                                        + str(animal) + ' .')
            except IndexError:
                raise forms.ValidationError('O animal %s, nunca teve lactação aberta.' % str(animal))
            
            if last_lact != None:
                raise forms.ValidationError('Não existe lactação aberta para o animal: '
                                        + str(animal) + ' . A ultima lactação deste animal \
                                        terminou a :' + str(last_lactation[0].dry_date))

        return animal
    
    
    def clean_milking_date(self):
        """
        Verify if milking date is posterior than lactation start date
        """
        milking_date = self.cleaned_data['milking_date']
        try:
            animal = self.cleaned_data['animal']
        except forms.ValidationError:
            raise forms.ValidationError('Verifique o campo animal.')
        except KeyError:
            raise forms.ValidationError('Verifique o campo animal.')
        
        
        if not self.milking_pk:
            '''
            If this is a new milking(milking_pk = None search for a active lactation 
            '''
            lactations = Lactation.objects.filter(farm = self.farm, 
                                                 animal = animal).order_by('-start_date')
            last_lactation = lactations[:1]
            
            try:
                if last_lactation[0].start_date <= milking_date:
                    return milking_date
                else:
                    raise
            except forms.ValidationError:
                raise forms.ValidationError('Data da Milking anterior à data de inicio da ultima lactação.')
            except TypeError:
                raise forms.ValidationError('Verifique a data da lactação')
        
        else:
            '''
            If is a modification search for lactations on the new(if theres a new) date 
            '''
            try:
                milking_date = self.cleaned_data['milking_date']
            except forms.ValidationError:
                raise forms.ValidationError('Verifique o campo data de Milking.')
             
            try:
                if len(Lactation.objects.filter(Q(farm = self.farm, animal = animal, 
                                          start_date__lte = milking_date),
                                          Q(dry_date__gt = milking_date) | Q(dry_date = None))) != 0:
                    return milking_date
                else:
                    raise forms.ValidationError('Não existiam lactações abertas na data indicada.')
            except forms.ValidationError:
                raise forms.ValidationError('Não existiam lactações abertas na data indicada.')
                
        return milking_date
    
    
    def clean_milking_time(self):
        """
        Verify if same milking_time doesn't been used for the animal on the same day
        """
        milking_time = self.cleaned_data['milking_time']
        try:
            animal = self.cleaned_data['animal']
        except forms.ValidationError:
            raise forms.ValidationError('Verifique o campo animal.')
        except KeyError:
            raise forms.ValidationError('Verifique o campo animal.')
        
        try:
            milking_date = self.cleaned_data['milking_date']
        except forms.ValidationError:
            raise forms.ValidationError('Verifique o campo data de Milking.')
        except KeyError:
            raise forms.ValidationError('Verifique os dados introduzidos')
    
        '''
        Verify if the day in question doesn't have already the max number of 
        milkings times allowed
        '''
        #get milkings on the given day
        try:
            number_milkings = len(Milking.objects.filter(farm = self.farm, animal = animal,
                                                     milking_date = milking_date))
        except:
            number_milkings = 0
        #get number of milking times
        try:
            number_milkings_times = len(MilkingTime.objects.filter(farm = self.farm))
        except:
            number_milkings_times = 0

        if number_milkings >= number_milkings_times:
            raise forms.ValidationError('Já existem: %d Milkings nesta data.' % (number_milkings))
       
        try:
            m = get_or_none(Milking, farm = self.farm, animal = animal, 
                           milking_date = milking_date, milking_time = milking_time)
                 
            if m and m.id != int(self.milking_pk): 
                raise forms.ValidationError('Já existe uma Milking para o animal: ' 
                                        + str(animal) + ' no periodo de ordenha: ' + str(milking_time))
            else:
                return milking_time
        except forms.ValidationError:
            raise forms.ValidationError('Já existe uma Milking para o animal: ' 
                                        + str(animal) + ' no periodo de ordenha: ' + str(milking_time))
        except MultipleObjectsReturned:
            raise forms.ValidationError('Já existe mais de uma Milking para o animal: ' 
                                        + str(animal) + ' no periodo de ordenha: ' + str(milking_time))
        return milking_time
      
        
    def save(self):
        '''
        Save the milking, replace an old one with new information, or add a new
        '''
        try: 
            milking =  get_object_or_404(Milking, id = self.milking_pk)
        except:
            milking = Milking()
        
        #Farm
        milking.farm = self.farm
        #Animal
        animal = self.cleaned_data['animal']
        milking.animal = animal
        #Employee        
        milking.employee = self.cleaned_data['employee']
        #Milk quantity
        quantity = to_float(self.cleaned_data['quantity'])
        milking.quantity = quantity
        #Date
        milking_date = self.cleaned_data['milking_date']
        milking.milking_date = milking_date
        #Hour
        milking.milking_hour = self.cleaned_data['milking_hour']
        #Time 
        milking.milking_time = self.cleaned_data['milking_time']
        
        #Milking start date, if is a new milking
        if not self.milking_pk:
            try:
                lactations = Lactation.objects.filter(farm = self.farm, animal = animal).order_by('-start_date')
                last_lactation = lactations[:1]
                last_lactation_date = last_lactation[0].start_date
            except ObjectDoesNotExist:
                    print 'Lactation not found.'
            try:
                Milkings_list = Milking.objects.filter(farm = self.farm, animal = animal, milking_date__gte = last_lactation_date).order_by('milking_date')
                first_Milking = Milkings_list[:1]
                milking.milking_start_date = first_Milking[0].milking_date
            except:
                milking.milking_start_date = self.cleaned_data['milking_date']
        #Milking start date, if replacing a milking
        else:
            lactations = Lactation.objects.filter(Q(farm = self.farm, animal= animal, 
                                               start_date__lte = milking_date), 
                                               Q(dry_date__gt = milking_date) | Q(dry_date = None))
            print lactations
            last_lactation = lactations[:1]
        
        #Last lactation
        milking.lactation = last_lactation[0]
        
        lasts_milkings = None
        try:
            lasts_milkings = Milking.objects.filter(farm = self.farm, animal = animal).order_by('-milking_number')
            milking_number = lasts_milkings[0].milking_number + 1
        except:
            lasts_milkings = None
            milking_number = 1

        if not self.milking_pk:
            milking.milking_number = milking_number

        milking.tank = self.cleaned_data['tank']
        milking.obs = self.cleaned_data['obs']

        #save milking
        milking.save()

        #If lactation as automatic dry checked
        #verify if quantity is in preferences parameters
        #try:
        if last_lactation[0].auto_dry and not last_lactation[0].dry_date and lasts_milkings != None:
            CloseLact(self.farm, animal, last_lactation[0], self.cleaned_data['milking_date'])
        #except:
        #    pass