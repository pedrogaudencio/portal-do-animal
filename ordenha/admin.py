# -*- coding: UTF-8 -*-
from ordenha.models import Milking, Storage, Lactation, MilkingTime, Contrast, ParametersAnalyzed, Analysis, MilkYield
from django.contrib import admin


class MilkingTimeAdmin(admin.ModelAdmin):

    pass

    """
    fieldsets = [
                 (None, {
                         'classes': ['wide', 'extrapretty'],
                         'fields': (
                                    #'numero_animal',
                                    ('inicio','numero_lactao'),
                                    ('data_contraste', 'numero_ordenha'),
                                    ('producao'),
                                    )
                         }),
    ]
    readonly_fields = ['numero_lactao', 'numero_ordenha']

    list_display = ('data_contraste','producao', 'numero_lactao')
    #list_display_links = ('__unicode__','morada')
    #list_filter = ['distrito']
    #search_fields = ['numero_animal']
    #save_as = True
    """

    
class AnalysisInline(admin.TabularInline):
    model = Analysis


class MilkYieldInline(admin.TabularInline):
    model = MilkYield


class ContrastAdmin(admin.ModelAdmin):
    inlines = [AnalysisInline, MilkYieldInline]


class PeriodoOrdenhaAdmin(admin.ModelAdmin):
    list_filter = ['farm']
    

class MilkYieldAdmin(admin.ModelAdmin):
    list_filter = ['contrast__date']


admin.site.register(Milking)
admin.site.register(Storage)
admin.site.register(Lactation)
admin.site.register(MilkingTime, MilkingTimeAdmin)
admin.site.register(Contrast, ContrastAdmin)
admin.site.register(ParametersAnalyzed)
admin.site.register(Analysis)
admin.site.register(MilkYield, MilkYieldAdmin)