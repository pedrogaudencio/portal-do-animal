# -*- coding: UTF-8 -*-
from preferences.models import Preference
from ordenha.models import Milking
from django.core.exceptions import ObjectDoesNotExist
from django.conf import settings
from utilizador.models import UserProfile
import ast

class CloseLact():
    '''
    Verify if needs to close the lactation
    '''
    def __init__(self, farm, animal, lactation, date):
        self.farm = farm
        self.animal = animal
        #self.quantity = quantity
        #self.milkings = milkings
        self.lactation = lactation
        self.date = date
        #self.request = request
 
        try:
            raw_data = Preference.dryAnimal.get(farm = self.farm)
            self.data = ast.literal_eval(raw_data.data)
        except ObjectDoesNotExist:
            print 'Preference data not found or error retrieving data'
        
        try:
            last_milkings = Milking.objects.filter(farm = self.farm, 
                                                   animal = self.animal, 
                                                   lactation = self.lactation).order_by('-milking_date')[:self.data['milking_number']]
            if len(last_milkings) < self.data['milking_number']:
                raise
        except:
            return None
        
        '''    
        For the last x milking, increment milk quantity, 
        if milk quantity is less than preferences increment counter. 
        ''' 
        milk_total = 0
        counter = 0
        for milking in last_milkings:
            milk_total += float(milking.quantity)
            
            if float(milking.quantity) < float(self.data['milk_quantity']):
                counter += 1

        '''
        If average selected, make average and round it;  
        '''
        if self.data['average']:
            average = milk_total / self.data['milking_number']
            print 'Media' + str(average)
            
            if average < float(self.data['milk_quantity']):
                self._do_action('Média de produção: ' + str(round(average, 2)))
        else:        
            if counter == int(self.data['milking_number']):
                self._do_action('O valor de referencia de ordenhas não foi atingido.')
        '''
        Test other reasons to dry animals, such as given birth in x days, sanidade
        '''
                               
    def _do_action(self, message):        
        '''
        Verify the preference auto_dry_action
        1- Give warning
        2- Give warning and close lactation
        3- Just close lactation
        '''
        
        if self.data['auto_dry_action'] == '1':
            
            self._send_notification(op_message = [message, ' deve de ser fechada.'])
            
        if self.data['auto_dry_action'] == '2':
            self._send_notification(op_message = [message, ' foi fechada.'])
            
            self.lactation.dry_date = self.date
            self.lactation.save()
            
        if self.data['auto_dry_action'] == '3':
            self.lactation.dry_date = self.date
            #self.lactation.save()

    def _send_notification(self, op_message):
        
        if "notification" in settings.INSTALLED_APPS:
            from notification import models as notification
        else:
            notification = None
        
        user_profile = UserProfile.objects.get(id = 1)
        
        
        if notification:
            notification.send([user_profile.user], "close_lactation", {'op_message': op_message,
                                                                       'lactation': self.lactation,
                                                                       'animal': self.animal,
                                                                       })
