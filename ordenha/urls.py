# -*- coding: UTF-8 -*-
from django.conf.urls.defaults import patterns, url


"""
Milking urls
"""
urlpatterns = patterns('ordenha.views',
    #--------------Storage-------------#
    url(r'^storage/(?P<slug>[-\w\d]+)/$', 'viewMilkSorage', name = 'storage_list'), #List
    url(r'^storage/(?P<slug>[-\w\d]+)/add/$', 'milkStorage', name = 'storage_add'), #Add
    url(r'^storage/(?P<slug>[-\w\d]+)/(?P<storage_pk>\d+)/edit/$', 'milkStorage', 
                                                            name = 'storage_edit'), #Edit
    
    #------------Milking times---------#
    url(r'^times/(?P<slug>[-\w\d]+)/$', 'viewMilkingTimes', name = 'times_list'), #List
    url(r'^times/(?P<slug>[-\w\d]+)/add/$', 'milkingTimes'), #Add
    url(r'^times/(?P<slug>[-\w\d]+)/(?P<milking_times_pk>\d+)/edit/$', 'milkingTimes'), #Edit

    
    #------------Lactations------------#
    url(r'^lactation/(?P<slug>[-\w\d]+)/(?P<animal_pk>\d+)/$', 'viewLactation', 
                                                                name = 'lactation_list'), #List
    url(r'^lactation/(?P<slug>[-\w\d]+)/add/$', 'addLactation', name = 'lactation_add'), #Add
    url(r'^lactation/(?P<slug>[-\w\d]+)/(?P<lactation_pk>\d+)/edit/$', 'addLactation', 
                                                                name = 'lactation_edit'), #Edit
    
    #------------Contrast--------------#
    url(r'^contrast/(?P<slug>[-\w\d]+)/(?P<animal_pk>\d+)/$', 'viewContrast', name = 'contrast_list'), #List
    url(r'^contrast/(?P<slug>[-\w\d]+)/add/$', 'addContrast', name = 'contrast_add'), #Add
    url(r'^contrast/(?P<slug>[-\w\d]+)/(?P<contrast_pk>\d+)/edit/$', 'modifyContrast'), #Edit
    
    
    #-------------Milking--------------#  
    url(r'^milking/(?P<slug>[-\w\d]+)/$', 'viewMilking', name = 'milking_list_all'), #List
    url(r'^milking/(?P<slug>[-\w\d]+)/(?P<animal_pk>\d+)/$', 'viewMilking'), #List -----TODO
    url(r'^milking/(?P<slug>[-\w\d]+)/add/$', 'addMilking', name = 'milking_add'), #Add
    url(r'^milking/(?P<slug>[-\w\d]+)/(?P<milking_pk>\d+)/edit/$', 'addMilking', name = 'milking_edit'), #Edit

)