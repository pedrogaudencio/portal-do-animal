# -*- coding: UTF-8 -*-
from django.shortcuts import render_to_response
from django.contrib.auth import authenticate, login


def index(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(username=username, password=password)
    if user is not None:
        if user.is_active:
            login(request, user)
            print "Forneceu um username e senha corretos!"
            #return render_to_response('testes/ok.html') 
        else:
            print "Conta desativada!"
    else:
        return render_to_response('testes/erro.html')
