# -*- coding: UTF-8 -*-
from django.shortcuts import get_object_or_404, render_to_response, render
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth.decorators import login_required
from django.template.context import RequestContext
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.core.exceptions import ValidationError, ObjectDoesNotExist
from django.utils.translation import ugettext as _
from exploracao.models import Farm
from exploracao.forms import FarmForm
from utilizador.models import UserProfile
from entidade.models import Breeder
from contactos.forms import AddressForm
from utils.make_initial_data import makeInitialData
from django.http import Http404


@login_required
def index(request):
    user = request.user
    print user
    
    print user.identification
    
    '''
    #TODO user.app_lisns/user-manage/<user_id>/
.../permissions/group-manage/<gt
    #TODO user.get lista de criadores
    app_list = None
    user_farms = None
    farm_resume_list = [] 
    farms_id = []
    
    #See if user have more than one breeder
    
    try:
        user.userprofile.breeder
    except ObjectDoesNotExist:
        raise Http404
    
    #If user works with one breeder
    if user.userprofile.breeder:
        #Get farms from breeder
        user_farms = user.userprofile.get_farms()
        farms_species = user.userprofile.breeder.get_animals_species()    
    
    for farm in user_farms.filter(active = True):
        farms_id.append(farm.id)
        animals_details, animals_total = farm.get_species_details(farms_species) 
        farm_resume_list.append({
            'farm': farm,
            'animals': animals_details,
            'animals_total': animals_total
            })

    #if user.is_superuser:
    app_list = [{'name': 'Exploração', 'url':['exploracao']}, 
                    {'name': 'Ordenha', 'url': ['Add','View', 'Cenas']}
                    ]

    context = {'app_list': app_list,
               'user_farms': farm_resume_list,
               'farms_id': farms_id}
    '''
    return render_to_response('index.html', None, context_instance = RequestContext(request))


@login_required
def farm_overview(request, farm_pk = None):
    """
    Farm overview 
    """
    #delete farm from session, if any
    try:
        del request.session['farm']
    except:
        pass
    #set the farm on user session
    #TODO: permission over farm
    farm = get_object_or_404(Farm, pk = farm_pk)
    request.session['farm'] = farm
    
    #Animal details
    animals_details, animals_total = farm.get_species_details()
    farm_resume_list = {'farm': farm,
                        'animals': animals_details,
                        'animals_total': animals_total
                        }
    context = {'farm': farm,
               'farm_resume_list':farm_resume_list,
               }
        
    return render(request, 'exploracao/farm.html', context)
    

@csrf_protect
def listar(request, pk):
    obj = get_object_or_404(Farm, id=pk)
    
    return render_to_response('exploracao/listar.html', {'exploracao': obj },
                              context_instance=RequestContext(request))


@login_required
def view_my_farms(request):
    user = get_object_or_404(UserProfile, user=request.user)
    my_farms = Farm.objects.filter(breeder=user.breeder)

    return render(request, 'exploracao/index.html', {'my_farms': my_farms,})


@login_required
def view_farm_details(request, slug):
    farm = get_object_or_404(Farm, slug = slug)

    #if request.user.has_perm('view_farm', farm):
        #print '\tPermissions: %s has permission to view farm %s' % (request.user, farm)
    return render(request, 'exploracao/farm_details.html', {'farm': farm,})
    #else:
    #    raise ValidationError(_('You don\'t have permissions to view this farm.'))


@login_required
def add_farm(request, breeder_id = None, slug = None, edit = False):
    user = get_object_or_404(UserProfile, user=request.user)
    farm = None
    contact = None
    
    #retrieve farm from session
    try: 
        farm = request.session['farm']
        slug = farm.slug
    except KeyError:
        return HttpResponseRedirect(reverse('index'))
    
    #TODO: redirect to choose breeder - ?colocar breeder em cima, como utilizador?
    if not breeder_id and not edit:
        return HttpResponseRedirect(reverse('view_my_farms')) 
    
    #TODO: Permissões do utilizador a criar breeder
    if not edit:
        breeder = get_object_or_404(Breeder, id = breeder_id)
    else:
        farm = get_object_or_404(Farm, slug = slug)
        breeder = farm.breeder
        contact = farm.contact

    if request.method == 'POST':
        address_form = AddressForm(request.POST, instance = contact) 
        farm_form = FarmForm(breeder, request.POST, instance = farm )

        if farm_form.is_valid() and address_form.is_valid():
            address_form.save()
            farm = farm_form.save(commit=False)
            farm.breeder = breeder
            farm.save()
            return HttpResponseRedirect(reverse('view_my_farms'))

    else:
        farm_form = FarmForm(breeder, instance = farm)
        address_form = AddressForm(instance = contact)
        
    context = {'breeder': breeder,
               'farm_form': farm_form,
               'address_form': address_form}

    return render_to_response('exploracao/add_farm.html', context, context_instance=RequestContext(request))


def remove_farm(request, farm_id):
    farm = get_object_or_404(Farm, pk=farm_id)
    farm.remove()

    return render(request, 'exploracao/remove_farm.html',)
