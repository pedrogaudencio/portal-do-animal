# -*- coding: UTF-8 -*-
from exploracao.models import Farm
from django.forms import ModelForm, Textarea, Select, TextInput
from utils.widgets import DatePickerWidget


class AdicionarExploracaoModelForm(ModelForm):
    class Meta:
        model=Farm
        

class FarmForm(ModelForm):
    
    def __init__(self, breeder, *args, **kwargs):
        self.breeder = breeder 
        super(FarmForm, self).__init__(*args, **kwargs)
        
    class Meta:
        model = Farm
        fields = ('name', 'trademark', 'register_number', 'close_date', 'register_date',
                  'obs', 'production', 'system', 'manager', 'health_status')
        
        widgets = {'breeder': TextInput(attrs={'disabled': 'disabled'}),
                   #'register_date': TextInput(attrs={'rows': 1, 'cols': 5}),
                   'close_date': DatePickerWidget(),
                   'register_date': DatePickerWidget(),
                   'obs': Textarea(attrs={'rows': 4,})
                   } 
