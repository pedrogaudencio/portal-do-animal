# -*- coding: UTF-8 -*-
from django.contrib import admin
from django.db import models
from exploracao.models import Farm, Herd, Production, HealthStatus
from django_google_maps import widgets as map_widgets
from django_google_maps import fields as map_fields
from guardian.admin import GuardedModelAdmin #@UnresolvedImport
from django.forms.widgets import CheckboxSelectMultiple


class HerdInLine(admin.TabularInline):
    model = Herd
    extra = 1
    verbose_name = "Herd"


class FarmAdmin(admin.ModelAdmin):
    fieldsets = [
                 (None, {
                         'classes': ['wide', 'extrapretty'],
                         'fields': (
                                    ('breeder','production'),
                                    ('name','register_number','trademark'),
                                    ('register_date','active','close_date'),
                                    ('manager','contact'),
                                    ('geolocation'),
                                    ('map_address',),
                                    ('applied_to', 'managed_by'),
                                    ('obs'),
                                    ('slug'),
                                    )
                         }),
    ]
    formfield_overrides = {
          map_fields.AddressField: {'widget': map_widgets.GoogleMapsAddressWidget},
    }
    inlines = [HerdInLine]
    list_filter = ['production','active']
    list_display = ('__unicode__','name','trademark','active','breeder', '_csvImport')

    readonly_fields = ['slug']

    
    def _csvImport(self, obj):
        return '<a href="/documents/csv/%s/add" onclick="return showAddAnotherPopup(this)">\
                csv</a>' % (obj.slug)
    
    _csvImport.allow_tags = True
    _csvImport.short_description = 'csv'



class HealthStatusAdmin(admin.ModelAdmin): #ver se isto dá algum jeito
    formfield_overrides = {
          models.ManyToManyField: {'widget': CheckboxSelectMultiple},
    }

admin.site.register(Farm, FarmAdmin)
admin.site.register(Herd)
admin.site.register(Production)
admin.site.register(HealthStatus, HealthStatusAdmin)
