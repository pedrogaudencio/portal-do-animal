# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Farm'
        db.create_table('exploracao_farm', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('breeder', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['entidade.Breeder'])),
            ('trademark', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
            ('register_number', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('register_date', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('system', self.gf('django.db.models.fields.CharField')(default='extensive', max_length=20)),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('close_date', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('manager', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
            ('map_address', self.gf('django_google_maps.fields.AddressField')(max_length=150, null=True, blank=True)),
            ('geolocation', self.gf('django_google_maps.fields.GeoLocationField')(max_length=150, null=True, blank=True)),
            ('red', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['red.RED'], null=True, blank=True)),
            ('obs', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal('exploracao', ['Farm'])

        # Adding M2M table for field production on 'Farm'
        db.create_table('exploracao_farm_production', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('farm', models.ForeignKey(orm['exploracao.farm'], null=False)),
            ('production', models.ForeignKey(orm['exploracao.production'], null=False))
        ))
        db.create_unique('exploracao_farm_production', ['farm_id', 'production_id'])

        # Adding M2M table for field contact on 'Farm'
        db.create_table('exploracao_farm_contact', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('farm', models.ForeignKey(orm['exploracao.farm'], null=False)),
            ('contact', models.ForeignKey(orm['contactos.contact'], null=False))
        ))
        db.create_unique('exploracao_farm_contact', ['farm_id', 'contact_id'])

        # Adding M2M table for field health_status on 'Farm'
        db.create_table('exploracao_farm_health_status', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('farm', models.ForeignKey(orm['exploracao.farm'], null=False)),
            ('healthstatus', models.ForeignKey(orm['exploracao.healthstatus'], null=False))
        ))
        db.create_unique('exploracao_farm_health_status', ['farm_id', 'healthstatus_id'])

        # Adding M2M table for field herds on 'Farm'
        db.create_table('exploracao_farm_herds', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('farm', models.ForeignKey(orm['exploracao.farm'], null=False)),
            ('herd', models.ForeignKey(orm['exploracao.herd'], null=False))
        ))
        db.create_unique('exploracao_farm_herds', ['farm_id', 'herd_id'])

        # Adding model 'Herd'
        db.create_table('exploracao_herd', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('farm', self.gf('django.db.models.fields.related.ForeignKey')(related_name='herd_farm', to=orm['exploracao.Farm'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=150)),
            ('obs', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal('exploracao', ['Herd'])

        # Adding model 'HealthStatus'
        db.create_table('exploracao_healthstatus', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('status_pt', self.gf('django.db.models.fields.CharField')(max_length=250)),
            ('status_en', self.gf('django.db.models.fields.CharField')(max_length=250, null=True, blank=True)),
            ('status_es', self.gf('django.db.models.fields.CharField')(max_length=250, null=True, blank=True)),
            ('status_fr', self.gf('django.db.models.fields.CharField')(max_length=250, null=True, blank=True)),
            ('acronym_pt', self.gf('django.db.models.fields.CharField')(max_length=15)),
            ('acronym_en', self.gf('django.db.models.fields.CharField')(max_length=15, null=True, blank=True)),
            ('acronym_es', self.gf('django.db.models.fields.CharField')(max_length=15, null=True, blank=True)),
            ('acronym_fr', self.gf('django.db.models.fields.CharField')(max_length=15, null=True, blank=True)),
            ('start_date', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('law_pt', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
            ('law_en', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
            ('law_es', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
            ('law_fr', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
        ))
        db.send_create_signal('exploracao', ['HealthStatus'])

        # Adding M2M table for field animal on 'HealthStatus'
        db.create_table('exploracao_healthstatus_animal', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('healthstatus', models.ForeignKey(orm['exploracao.healthstatus'], null=False)),
            ('speciebreed', models.ForeignKey(orm['animal.speciebreed'], null=False))
        ))
        db.create_unique('exploracao_healthstatus_animal', ['healthstatus_id', 'speciebreed_id'])

        # Adding model 'Production'
        db.create_table('exploracao_production', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name_pt', self.gf('django.db.models.fields.CharField')(max_length=150)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
            ('name_es', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
            ('name_fr', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
        ))
        db.send_create_signal('exploracao', ['Production'])

    def backwards(self, orm):
        # Deleting model 'Farm'
        db.delete_table('exploracao_farm')

        # Removing M2M table for field production on 'Farm'
        db.delete_table('exploracao_farm_production')

        # Removing M2M table for field contact on 'Farm'
        db.delete_table('exploracao_farm_contact')

        # Removing M2M table for field health_status on 'Farm'
        db.delete_table('exploracao_farm_health_status')

        # Removing M2M table for field herds on 'Farm'
        db.delete_table('exploracao_farm_herds')

        # Deleting model 'Herd'
        db.delete_table('exploracao_herd')

        # Deleting model 'HealthStatus'
        db.delete_table('exploracao_healthstatus')

        # Removing M2M table for field animal on 'HealthStatus'
        db.delete_table('exploracao_healthstatus_animal')

        # Deleting model 'Production'
        db.delete_table('exploracao_production')

    models = {
        'animal.speciebreed': {
            'Meta': {'object_name': 'SpecieBreed'},
            'breed_en': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'breed_es': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'breed_fr': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'breed_pt': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'cod_en': ('django.db.models.fields.CharField', [], {'max_length': '4', 'null': 'True', 'blank': 'True'}),
            'cod_es': ('django.db.models.fields.CharField', [], {'max_length': '4', 'null': 'True', 'blank': 'True'}),
            'cod_fr': ('django.db.models.fields.CharField', [], {'max_length': '4', 'null': 'True', 'blank': 'True'}),
            'cod_pt': ('django.db.models.fields.CharField', [], {'max_length': '4', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'specie': ('django.db.models.fields.CharField', [], {'max_length': '15'})
        },
        'contactos.concelho': {
            'Meta': {'object_name': 'Concelho'},
            'concelho': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'contactos.contact': {
            'Meta': {'object_name': 'Contact'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'cellphone': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'company_number': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'company_number_extension': ('django.db.models.fields.CharField', [], {'max_length': '5', 'null': 'True', 'blank': 'True'}),
            'concelho': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contactos.Concelho']"}),
            'district': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contactos.District']"}),
            'fax': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'freguesia': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contactos.Freguesia']"}),
            'home_number': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'zip_code': ('django.db.models.fields.CharField', [], {'max_length': '10'})
        },
        'contactos.district': {
            'Meta': {'object_name': 'District'},
            'district': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'contactos.freguesia': {
            'Meta': {'object_name': 'Freguesia'},
            'freguesia': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'entidade.breeder': {
            'Meta': {'object_name': 'Breeder', '_ormbases': ['entidade.Entities']},
            'entities_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['entidade.Entities']", 'unique': 'True', 'primary_key': 'True'}),
            'ifap': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'})
        },
        'entidade.entities': {
            'Meta': {'object_name': 'Entities'},
            'contact': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['contactos.Contact']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'nib': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'nif': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'obs': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'})
        },
        'exploracao.farm': {
            'Meta': {'object_name': 'Farm'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'breeder': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['entidade.Breeder']"}),
            'close_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'contact': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['contactos.Contact']", 'null': 'True', 'blank': 'True'}),
            'geolocation': ('django_google_maps.fields.GeoLocationField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'health_status': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['exploracao.HealthStatus']", 'null': 'True', 'blank': 'True'}),
            'herds': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'farm_herd'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['exploracao.Herd']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'manager': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'map_address': ('django_google_maps.fields.AddressField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'obs': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'production': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['exploracao.Production']", 'symmetrical': 'False', 'blank': 'True'}),
            'red': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['red.RED']", 'null': 'True', 'blank': 'True'}),
            'register_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'register_number': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'system': ('django.db.models.fields.CharField', [], {'default': "'extensive'", 'max_length': '20'}),
            'trademark': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'})
        },
        'exploracao.healthstatus': {
            'Meta': {'object_name': 'HealthStatus'},
            'acronym_en': ('django.db.models.fields.CharField', [], {'max_length': '15', 'null': 'True', 'blank': 'True'}),
            'acronym_es': ('django.db.models.fields.CharField', [], {'max_length': '15', 'null': 'True', 'blank': 'True'}),
            'acronym_fr': ('django.db.models.fields.CharField', [], {'max_length': '15', 'null': 'True', 'blank': 'True'}),
            'acronym_pt': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'animal': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['animal.SpecieBreed']", 'symmetrical': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'law_en': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'law_es': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'law_fr': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'law_pt': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'start_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'status_en': ('django.db.models.fields.CharField', [], {'max_length': '250', 'null': 'True', 'blank': 'True'}),
            'status_es': ('django.db.models.fields.CharField', [], {'max_length': '250', 'null': 'True', 'blank': 'True'}),
            'status_fr': ('django.db.models.fields.CharField', [], {'max_length': '250', 'null': 'True', 'blank': 'True'}),
            'status_pt': ('django.db.models.fields.CharField', [], {'max_length': '250'})
        },
        'exploracao.herd': {
            'Meta': {'object_name': 'Herd'},
            'farm': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'herd_farm'", 'to': "orm['exploracao.Farm']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'obs': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'})
        },
        'exploracao.production': {
            'Meta': {'object_name': 'Production'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'name_es': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'name_fr': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'name_pt': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        },
        'red.red': {
            'Meta': {'object_name': 'RED'},
            'authorization_number': ('django.db.models.fields.PositiveIntegerField', [], {'max_length': '20'}),
            'book_appendix': ('django.db.models.fields.PositiveIntegerField', [], {'max_length': '20'}),
            'book_number': ('django.db.models.fields.PositiveIntegerField', [], {'max_length': '20'}),
            'entitie': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['entidade.Entities']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'issuer': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'obs': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'year': ('django.db.models.fields.IntegerField', [], {})
        }
    }

    complete_apps = ['exploracao']