# -*- coding: UTF-8 -*-
from dajax.core import Dajax
from dajaxice.decorators import dajaxice_register
from exploracao.models import Farm
from django.template.context import RequestContext
from django.shortcuts import get_object_or_404, render_to_response, render
from django.template.loader import render_to_string

@dajaxice_register
def get_breed_details(request, specie, farm_id):
    dajax = Dajax()
    farm = Farm.objects.get(id = farm_id) #TRY PARA FAIL
    details = farm.get_breed_details(specie)
    render = render_to_string('exploracao/resume_details_breed.html', locals())
    dajax.assign('#breed-'+specie+'-'+farm_id, 'innerHTML', render)
    
    return dajax.json()
    

@dajaxice_register
def get_chart_species(request, farm_id, div_info):
    """
    Get charts for species by farm 
    """
    dajax = Dajax()
    farm = Farm.objects.get(id = farm_id)
    dajax.script('drawChart(%s, %s);' % (str(farm.get_species_chart()), str(div_info)))
    return dajax.json()