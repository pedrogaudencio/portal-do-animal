# -*- coding: UTF-8 -*-
from django.db import models
from animal.models import Animal, SpecieBreed
from django_google_maps import fields as map_fields
from django.utils.encoding import force_unicode
from django.utils.translation import ugettext as _
from django.template.defaultfilters import slugify
from django.core.urlresolvers import reverse
from transmeta import TransMeta #@UnresolvedImport
from django.contrib.auth.models import Group


class ManagedManager(models.Manager):
    """
    Filter farms there aren't managed by an association 
    """
    def get_query_set(self):
        return super(ManagedManager, self).get_query_set().exclude(managed_by__isnull = False)


#TODO: Colocar alguns dos campos a balnk = False
class Farm(models.Model):
    breeder = models.ForeignKey('entidade.Breeder', verbose_name = _('Breeder'), blank = False, null = False)
    trademark = models.CharField(_('Farm product mark'), max_length = 150, blank = True, 
                                 null = True, unique = True)
    production = models.ManyToManyField('Production' ,verbose_name= _('Production type'), 
                                        blank = True, null = False) #para pesquisa de tipos de produção, vendedores de carne etc
    name = models.CharField(_('Farm name'), max_length = 150, blank = True, null = True)
    register_number = models.CharField(_('Farm register number'), 
                                        max_length = 50, blank = True , null = True)
    register_date = models.DateField(_('Register date'), blank = True, null = True)
    SYSTEM_OPTIONS = (('intensive', _('Intensive/Stable')),
                       ('extensive', _('Extensive/Outside')),
                       )
    system = models.CharField(_('Production system'), max_length = 20, choices = SYSTEM_OPTIONS, default='extensive', 
                              blank = False, null = False)
    active = models.BooleanField(_('Active farm'), default = True)
    close_date = models.DateField(_('Date of close'), blank = True, null = True)

    contact = models.ForeignKey('contactos.Contact', verbose_name = _('Contact'), blank = True, null = True)
    manager = models.CharField(_('Farm manager'), max_length = 150, blank = True, null = True) #COLOCAR UM FOREIGNKEY?
    health_status = models.ManyToManyField('HealthStatus', 
                                                help_text = _('Farm health status'), 
                                                blank = True, null = True)
    #candidaturas_ifap
    herds = models.ManyToManyField('Herd', related_name = 'farm_herd', verbose_name = 'List of herds', 
                                   blank = True, null = True)
    map_address = map_fields.AddressField(max_length = 150, blank = True, null = True)
    geolocation = map_fields.GeoLocationField(max_length = 150, blank = True, null = True)
    obs = models.TextField(_('Observations'), blank = True, null = True)
    
    managed_by = models.ForeignKey('entidade.Association', related_name = 'managed', 
                                   verbose_name = _('Managed by association:'), 
                                   blank = True, null = True)
    applied_to = models.ForeignKey('entidade.Association', related_name = 'applied',
                                   verbose_name = _('Applied to association:'), 
                                   blank = True, null = True)
    
    slug = models.SlugField(max_length = 150, blank = False, null = False)
    
    objects = models.Manager() # The default manager.
    not_managed = ManagedManager() # Farms not managed by an association
     
    
    class Meta():
        verbose_name = _('Farm')
        verbose_name_plural = _('Farms')
        permissions = (
            ('view_farm', _('Can view farm')),
        )


    def __unicode__(self): 
        return '%s - %s' %  (force_unicode(self.breeder), self.name)
    
    
    def get_animals_species(self):
        """
        Get list of animal species
        """
        species_list = []
        
        animals_list = Animal.objects.filter(farm = self)
        
        for animal in animals_list:
            if animal.specie_breed.specie not in species_list:
                species_list.append(animal.specie_breed.specie)
    
        return species_list

        
    def get_species_details(self, species = None):
        """
        Get farm resume, by specie
        """
        animal_list = []
        animal_total = 0
        if not species:
            species = []
            animals_list = Animal.objects.filter(farm = self)
        
            for animal in animals_list:
                if animal.specie_breed.specie not in species:
                    species.append(animal.specie_breed.specie) 
        
        for specie in species:
            animals = Animal.objects.filter(farm__id = self.id, specie_breed__specie = specie)
            animal_total += animals.count()
            if animals.count() > 0:
                animal_list.append({
                    'specie': specie,
                    'males': animals.filter(sex = 'M').count(),
                    'females': animals.filter(sex = 'F').count(),
                    'total': animals.count()
                    })
            else:
                animal_list.append(None)

        self.get_species_chart()
        return animal_list, animal_total
    
    
    def get_breed_details(self, specie = None):
        """
        Get resume of specie
        """
        animal_list = []
        
        for specie_breed in SpecieBreed.objects.filter(specie = specie):
            animals = Animal.objects.filter(farm__id = self.id, specie_breed = specie_breed)
            
            if animals.count() > 0:
                animal_list.append({
                    'breed': specie_breed.breed,
                    'males': animals.filter(sex = 'M').count(),
                    'females': animals.filter(sex = 'F').count(),
                    'total': animals.count()
                    })

        return animal_list
        

    def get_specie_breed(self):
        """
        Get list of animal species
        """
        species_list = []
        
        animals_list = Animal.objects.filter(farm__id = self.id)
        
        for animal in animals_list:
            if animal.specie_breed.specie not in species_list:
                species_list.append(animal.specie_breed.specie)
    
        return species_list



    def get_species_chart(self):
        import gviz_api

        description = {"name": ("string", _("Specie")),
                       "animals": ("number", _("Number of animals")),
                       }
        data = []
        for specie_breed in self.get_animals_species():
            animals = Animal.objects.filter(farm = self, specie_breed__specie = str(specie_breed))
            data.append({"name": specie_breed, "animals": len(animals)})
                    
        # Loading it into gviz_api.DataTable
        data_table = gviz_api.DataTable(description)
        data_table.LoadData(data)
        
        
        # Creating a JavaScript code string
        jscode = data_table.ToJSCode("jscode_data",
                               columns_order=("name", "animals"),
                               order_by="animals")
        # Creating a JSon string
        json = data_table.ToJSon(columns_order=("name", "animals"),
                           order_by="animals")
        return json
    
    
    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = slugify(self.trademark)
    
            n='%s (%s)' % (self.name, self.breeder)
            group = Group.objects.create(name=n)
            group.save()
        super(Farm, self).save(*args, **kwargs)
    
    #def get_absolute_url(self):
    #    return reverse('ordenha.views.milkStorage', kwargs={'slug': self.slug})


class Herd(models.Model):
    farm = models.ForeignKey('exploracao.Farm', related_name = 'herd_farm', verbose_name = _('Farm'), 
                             blank=False, null= False)
    name = models.CharField(_('Name'), max_length=150, blank=False, null = False)
    obs = models.TextField(_('Obeservations'), blank = True, null = True)
    #area_global = models.PositiveIntegerField('Área Global (Ha)', blank=False) # Ha
    #area_regadio = models.PositiveIntegerField('Área de Regadio (Ha)', blank=False) # Ha
    #area_sequeiro = models.PositiveIntegerField('Área de Sequeiro (Ha)', blank=False) # Ha

    def __unicode__(self):
        return '%s - %s' %  self.name, self.farm

    class Meta:
        verbose_name = 'Herd'
        verbose_name_plural = 'Herds'
        permissions = (
            ('view_herd', _('Can view herd')),
        )



class HealthStatus(models.Model):
    __metaclass__ = TransMeta
    status = models.CharField(_('Name for health status'), max_length = 250, 
                                blank = False, null = False)
    acronym = models.CharField(_('Acronym'), max_length = 15, blank = False, null = False)
    animal = models.ManyToManyField('animal.SpecieBreed', 
                                    help_text = _('List of animals for which this status is applied'), 
                                    blank = False, null = False, verbose_name = 'Species - Breeds')
    start_date = models.DateField(_('Entry date'), 
                                    help_text = _('Date of status entry'),
                                    blank = True, null = True)
    law = models.CharField(_('Law'), max_length = 150, help_text = _('Aproval law'), blank = True, null = True)
    #anexo

    class Meta:
        verbose_name = 'Health status'
        verbose_name_plural = 'Health status'
        translate = ('status', 'acronym', 'law',)

    def __unicode__(self):
        return '%s - %s'% (self.status, self.acronym)


class Production(models.Model):
    __metaclass__ = TransMeta
    name = models.CharField(_('Production type'), max_length = 150, blank = False, null = False)

    class Meta:
        verbose_name = 'Production type'
        verbose_name_plural = 'Productions type'
        translate = ('name',)

    def __unicode__(self):
        return '%s' % self.name
