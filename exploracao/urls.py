# -*- coding: UTF-8 -*-
from django.conf.urls.defaults import *
from django.views.generic import DetailView, ListView
from exploracao.models import Farm
from django.views.generic.base import TemplateView

urlpatterns = patterns('',
    (r'^$',
        ListView.as_view(
            queryset=Farm.objects.order_by('id')[:10],
            context_object_name='latest_exploracao_list',
            template_name='exploracao/index.html')),
    (r'^(?P<pk>\d+)/$',
        'exploracao.views.listar'),
    (r'^adicionar/$',
        'exploracao.views.adicionar'),
    (r'^$',
        TemplateView.as_view(
            template_name='animal/done.html')),
)

urlpatterns = patterns('exploracao.views',
    url(r'^$', 'view_my_farms', name='view_my_farms'),
    url(r'^(?P<slug>[-\w\d]+)/$', 'view_farm_details', name='view_farm_details'),
    
    #add farm
    url(r'^(?P<breeder_id>\d+)/add/$', 'add_farm', {'slug': None}, name='add_farm'),
    url(r'^add/$', 'add_farm', name='add_farm'),
    
    url(r'^(?P<pk>\d+)/remove/$', 'remove_farm', name='remove_farm'),
    
    
    url(r'^farm/edit/$', 'add_farm', {'edit': True, 'breeder_id': None}, name = 'edit_farm'), #Edit
    
    
    #Farm overview
    url(r'^farm/(?P<farm_pk>\d+)/$', 'farm_overview', name='farm_overview'),
    
)

"""
url(r'^csv/(?P<farm_id>\d+)/$', 'csvImport', name='csvImport'),
"""