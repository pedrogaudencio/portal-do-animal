# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Staff.breeder'
        db.delete_column('utilizador_staff', 'breeder_id')

        # Deleting field 'Staff.user_profile'
        db.delete_column('utilizador_staff', 'user_profile_id')

        # Adding field 'Staff.entitie'
        db.add_column('utilizador_staff', 'entitie',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=1, to=orm['entidade.Entities']),
                      keep_default=False)


        # Changing field 'Staff.name'
        db.alter_column('utilizador_staff', 'name', self.gf('django.db.models.fields.CharField')(max_length=500))

        # Changing field 'Staff.title'
        db.alter_column('utilizador_staff', 'title', self.gf('django.db.models.fields.CharField')(max_length=15, null=True))

        # Changing field 'Staff.nif'
        db.alter_column('utilizador_staff', 'nif', self.gf('django.db.models.fields.CharField')(max_length=50, null=True))

        # Changing field 'Staff.identification'
        db.alter_column('utilizador_staff', 'identification', self.gf('django.db.models.fields.CharField')(max_length=50))

        # Changing field 'Staff.post'
        db.alter_column('utilizador_staff', 'post', self.gf('django.db.models.fields.CharField')(max_length=500, null=True))
    def backwards(self, orm):
        # Adding field 'Staff.breeder'
        db.add_column('utilizador_staff', 'breeder',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['entidade.Breeder'], null=True),
                      keep_default=False)

        # Adding field 'Staff.user_profile'
        db.add_column('utilizador_staff', 'user_profile',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['utilizador.UserProfile'], null=True, blank=True),
                      keep_default=False)

        # Deleting field 'Staff.entitie'
        db.delete_column('utilizador_staff', 'entitie_id')


        # Changing field 'Staff.name'
        db.alter_column('utilizador_staff', 'name', self.gf('django.db.models.fields.CharField')(max_length=250))

        # Changing field 'Staff.title'
        db.alter_column('utilizador_staff', 'title', self.gf('django.db.models.fields.CharField')(default='Sr.', max_length=15))

        # Changing field 'Staff.nif'
        db.alter_column('utilizador_staff', 'nif', self.gf('django.db.models.fields.CharField')(default=123123123, max_length=10))

        # Changing field 'Staff.identification'
        db.alter_column('utilizador_staff', 'identification', self.gf('django.db.models.fields.CharField')(max_length=20))

        # Changing field 'Staff.post'
        db.alter_column('utilizador_staff', 'post', self.gf('django.db.models.fields.CharField')(default='Admin', max_length=50))
    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contactos.concelho': {
            'Meta': {'object_name': 'Concelho'},
            'concelho': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'contactos.contact': {
            'Meta': {'object_name': 'Contact'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'cellphone': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'company_number': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'company_number_extension': ('django.db.models.fields.CharField', [], {'max_length': '5', 'null': 'True', 'blank': 'True'}),
            'concelho': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contactos.Concelho']"}),
            'district': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contactos.District']"}),
            'fax': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'freguesia': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contactos.Freguesia']"}),
            'home_number': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'zip_code': ('django.db.models.fields.CharField', [], {'max_length': '10'})
        },
        'contactos.district': {
            'Meta': {'object_name': 'District'},
            'district': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'contactos.freguesia': {
            'Meta': {'object_name': 'Freguesia'},
            'freguesia': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'entidade.entities': {
            'Meta': {'object_name': 'Entities'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'contact': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['contactos.Contact']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'nib': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'nif': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'obs': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'})
        },
        'utilizador.staff': {
            'Meta': {'object_name': 'Staff'},
            'contact': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contactos.Contact']"}),
            'entitie': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['entidade.Entities']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'identification': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'nif': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'post': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '15', 'null': 'True', 'blank': 'True'})
        },
        'utilizador.userprofile': {
            'Meta': {'object_name': 'UserProfile', '_ormbases': ['utilizador.Staff']},
            'is_account_owner': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'staff_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['utilizador.Staff']", 'unique': 'True', 'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['auth.User']", 'unique': 'True'}),
            'user_type': ('django.db.models.fields.CharField', [], {'max_length': '1'})
        }
    }

    complete_apps = ['utilizador']