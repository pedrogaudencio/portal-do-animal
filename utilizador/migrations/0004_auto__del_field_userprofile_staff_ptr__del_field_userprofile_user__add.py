# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'UserProfile.staff_ptr'
        db.delete_column('utilizador_userprofile', 'staff_ptr_id')

        # Deleting field 'UserProfile.user'
        db.delete_column('utilizador_userprofile', 'user_id')

        # Adding field 'UserProfile.user_ptr'
        db.add_column('utilizador_userprofile', 'user_ptr',
                      self.gf('django.db.models.fields.related.OneToOneField')(default=1, to=orm['auth.User'], unique=True, primary_key=True),
                      keep_default=False)

        # Adding field 'UserProfile.title'
        db.add_column('utilizador_userprofile', 'title',
                      self.gf('django.db.models.fields.CharField')(max_length=15, null=True, blank=True),
                      keep_default=False)

        # Adding field 'UserProfile.post'
        db.add_column('utilizador_userprofile', 'post',
                      self.gf('django.db.models.fields.CharField')(max_length=500, null=True, blank=True),
                      keep_default=False)

        # Adding field 'UserProfile.identification'
        db.add_column('utilizador_userprofile', 'identification',
                      self.gf('django.db.models.fields.CharField')(default=1, max_length=50),
                      keep_default=False)

        # Adding field 'UserProfile.nif'
        db.add_column('utilizador_userprofile', 'nif',
                      self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True),
                      keep_default=False)

        # Adding field 'UserProfile.entitie'
        db.add_column('utilizador_userprofile', 'entitie',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=1, to=orm['entidade.Entities']),
                      keep_default=False)

        # Adding field 'UserProfile.contact'
        db.add_column('utilizador_userprofile', 'contact',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=1, to=orm['contactos.Contact']),
                      keep_default=False)

    def backwards(self, orm):
        # Adding field 'UserProfile.staff_ptr'
        db.add_column('utilizador_userprofile', 'staff_ptr',
                      self.gf('django.db.models.fields.related.OneToOneField')(default='cenas', to=orm['utilizador.Staff'], unique=True, primary_key=True),
                      keep_default=False)

        # Adding field 'UserProfile.user'
        db.add_column('utilizador_userprofile', 'user',
                      self.gf('django.db.models.fields.related.OneToOneField')(default=1, to=orm['auth.User'], unique=True),
                      keep_default=False)

        # Deleting field 'UserProfile.user_ptr'
        db.delete_column('utilizador_userprofile', 'user_ptr_id')

        # Deleting field 'UserProfile.title'
        db.delete_column('utilizador_userprofile', 'title')

        # Deleting field 'UserProfile.post'
        db.delete_column('utilizador_userprofile', 'post')

        # Deleting field 'UserProfile.identification'
        db.delete_column('utilizador_userprofile', 'identification')

        # Deleting field 'UserProfile.nif'
        db.delete_column('utilizador_userprofile', 'nif')

        # Deleting field 'UserProfile.entitie'
        db.delete_column('utilizador_userprofile', 'entitie_id')

        # Deleting field 'UserProfile.contact'
        db.delete_column('utilizador_userprofile', 'contact_id')

    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contactos.concelho': {
            'Meta': {'object_name': 'Concelho'},
            'concelho': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'contactos.contact': {
            'Meta': {'object_name': 'Contact'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'cellphone': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'company_number': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'company_number_extension': ('django.db.models.fields.CharField', [], {'max_length': '5', 'null': 'True', 'blank': 'True'}),
            'concelho': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contactos.Concelho']"}),
            'district': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contactos.District']"}),
            'fax': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'freguesia': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contactos.Freguesia']"}),
            'home_number': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'zip_code': ('django.db.models.fields.CharField', [], {'max_length': '10'})
        },
        'contactos.district': {
            'Meta': {'object_name': 'District'},
            'district': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'contactos.freguesia': {
            'Meta': {'object_name': 'Freguesia'},
            'freguesia': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'entidade.entities': {
            'Meta': {'object_name': 'Entities'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'contact': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['contactos.Contact']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'nib': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'nif': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'obs': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'})
        },
        'utilizador.staff': {
            'Meta': {'object_name': 'Staff'},
            'contact': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contactos.Contact']"}),
            'entitie': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['entidade.Entities']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'identification': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'nif': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'post': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '15', 'null': 'True', 'blank': 'True'})
        },
        'utilizador.userprofile': {
            'Meta': {'object_name': 'UserProfile', '_ormbases': ['auth.User']},
            'contact': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contactos.Contact']"}),
            'entitie': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['entidade.Entities']"}),
            'identification': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'is_account_owner': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'nif': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'post': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '15', 'null': 'True', 'blank': 'True'}),
            'user_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['auth.User']", 'unique': 'True', 'primary_key': 'True'}),
            'user_type': ('django.db.models.fields.CharField', [], {'max_length': '1'})
        }
    }

    complete_apps = ['utilizador']