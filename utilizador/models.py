# -*- coding: UTF-8 -*-
from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import ugettext as _
from django.core.mail import send_mail
from django.db.utils import IntegrityError
#from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned


class Staff(models.Model):
    """
    Person that works on entities, don't have access to the system.
    """
    name = models.CharField(_('Name'), max_length = 500, blank = False, null = False)
    title = models.CharField(_('Title'), help_text= _('Sr., Sra., Eng., Dr.'), 
                             max_length=15, blank = True, null = True)
    post = models.CharField(help_text=_('Work post'), 
                            max_length = 500, blank = True, null = True)
    identification = models.CharField(_('Identification'), help_text='BI/CC/Passaporte', 
                                      max_length=50, blank = False, null = False)
    nif = models.CharField(_('NIF'), max_length=50, blank = True, null = True)
    entitie = models.ForeignKey('entidade.Entities', blank = False, null = False)
    contact = models.ForeignKey('contactos.Contact', blank = False, null = False)


    def __unicode__(self):
        return '%s' % (self.name)

    
    class Meta():
        unique_together = ('identification', 'entitie')
        verbose_name = _('Staff')
        verbose_name_plural = _('Staff')


    @classmethod
    def create(cls, name, title, post, identification, nif, entitie, contact):
        """
        Create a new Staff
        """        
        try:
            staff = cls(name = name, title = title, post = post, identification = identification, 
                    nif = nif, entitie = entitie, contact = contact)
            staff.save()
        except IntegrityError:
            pass
    '''
    def get_farms(self):
        "Get farms from breeder"
        #TODO: cada user tem permissões apenas para algumas farms do breede?
        return self.breeder.farm_set.all()
    '''

class UserProfile(User):
    """
    User profile
    """
    title = models.CharField(_('Title'), help_text= _('Sr., Sra., Eng., Dr.'), 
                             max_length=15, blank = True, null = True)
    post = models.CharField(help_text=_('Work post'), 
                            max_length = 500, blank = True, null = True)
    identification = models.CharField(_('Identification'), help_text='BI/CC/Passaporte', 
                                      max_length=50, blank = False, null = False)
    nif = models.CharField(_('NIF'), max_length=50, blank = True, null = True)
    entitie = models.ForeignKey('entidade.Entities', blank = False, null = False)
    contact = models.ForeignKey('contactos.Contact', blank = False, null = False)
    
    type_choices = (('1', _('Basic')),('2', _('Intermediate')),('3', _('Expert')))
    user_type = models.CharField(_('User type'), max_length=1, choices=type_choices, blank = False)
    
#    foto_small = models.ImageField(upload_to=settings.PEOPLE_DIR, blank=True, null=True)
#    foto_large = models.ImageField(upload_to=settings.PEOPLE_DIR, blank=True, null=True)
    is_account_owner = models.BooleanField(default = False, help_text = _('Is the entitie manager.'))
    
    
    def __unicode__(self):
        return '%s' % (self.username)
    
    
    class Meta():
        verbose_name = _('User profile')
        verbose_name_plural = _('Users profile')
    
    
    #TODO: make set permissions for a given farm
    def save(self, *args, **kwargs):
        """
        On save, username will be email.
        """
        if not self.id:
            self.username = self.email
            raw_password = 'qwerty'#UserManager().make_random_password(length = 6)
            self.set_password(raw_password)
            _send_email(self.email, self.username, raw_password)
        
        #A user is also a staff
        Staff.create((self.first_name + ' ' + self.last_name), self.title, self.post, 
                         self.identification, self.nif, self.entitie, self.contact)
        
            

        super(UserProfile, self).save(*args, **kwargs)


def _clean_email(user_id, email):
    """
    Email must be unique
    """
    from django.core.exceptions import ValidationError
    
    client = User.objects.filter(email = email).exclude(id = user_id)
    if len(client) > 0:
        raise ValidationError(_('E-mail: %s, already is assigned to user: %s %s. \
                            Please introduce other e-mail.') % (email, 
                                                                client[0].first_name, 
                                                                client[0].last_name))


def _send_email(user_email, username, password):
    """
    Send email with username and password to the new user
    """
    subject = 'Pedido de registo'
    message = 'Novo utilizador: %s com a password: %s.' % (username, password)
    sender = 'simaojf@iol.ptteste'
    receiver = user_email
    
    try:
        send_mail(subject, message, sender, [receiver], fail_silently=False)
    except:
        print 'ERROR - User: %s | Pass: %s' % (username, password)  
