# -*- coding: UTF-8 -*-
from utilizador.models import UserProfile

class UserProfileAuthenticationBackend(object):
    def authenticate(self, username=None, password=None):
        try:
            person = UserProfile.objects.get(username=username)
            if person.check_password(password):
                return person
        except UserProfile.DoesNotExist:
            pass

        return None

    def get_user(self, user_id):
        try:
            return UserProfile.objects.get(pk=user_id)
        except UserProfile.DoesNotExist:
            return None