# -*- coding: UTF-8 -*-
from utilizador.forms import UtilizadorForm
from django.template.context import RequestContext
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.core import urlresolvers
from django.core.mail import send_mail

def NewUser(request):
    if request.method=='POST':
        form = UtilizadorForm(request.POST)
        if form.is_valid():
            userInfo = form.save() 
            '''
            response para o email 
            '''
            context = {'subject': 'Pedido de registo',
                       'head': 'O Portal do Animal informa que foi pedido um novo registo',
                       'nome': userInfo['nome'],
                       'cargo': userInfo['cargo'],
                       'bi': userInfo['bi'],
                       'link': 'http://127.0.0.1:8000/admin/auth/user/' + str(userInfo['user_id'])         
                     }
            
            try:
                send_mail(context['subject'], str(context), 'admin@portalanimal.com',
                            ['teste@portalanimal.com'], fail_silently=False)

            except:
                return render_to_response('registration/emailExcept.html')   
            

            return render_to_response('registration/done.html')
    else: 
        form = UtilizadorForm() 
    #return render_to_response('testes/testes.html')
    
    
    return render_to_response('registration/new_user.html', 
                                {'newUserForm' : form, },
                                context_instance=RequestContext(request))
    