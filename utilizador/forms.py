# -*- coding: UTF-8 -*-
from django import forms
from django.contrib.auth.models import User
from utilizador.models import UserProfile
from contactos.models import Contact, District, Concelho, Freguesia
from entidade.models import Breeder


class UserProfileForm(forms.ModelForm):
    """
    Override User form to make fields required, for clients
    """
    def __init__(self, *args, **kwargs):
        #Make field first_name required
        self.base_fields['first_name'].required = True
        #Make field email required
        self.base_fields['email'].required = True
        super(UserProfileForm, self).__init__(*args, **kwargs)

    class Meta:
        model = UserProfile


class UtilizadorForm(forms.ModelForm):
    """
    Adiciona um novo utilizador à base de dados, mas sem estar 'activo' 
    """ 
    
    TITLE_CHOICES = (('Sr.','Sr.'), ('Sra.', 'Sra.'), ('Eng.', 'Eng'), ('Dr.', 'Dr.'))
    
    
    confirme_a_senha = forms.CharField(label = 'Confira a palavra-passe:', max_length=30, widget=forms.PasswordInput)
    nome_completo = forms.CharField(label = 'Nome:', required = True)
    #first_name = forms.CharField(label = 'Nome:', required = True)
    #last_name = forms.CharField(label = 'Apelido:', required = True)
    titulo = forms.ChoiceField(label = 'Titulo:', required = False, choices = TITLE_CHOICES)
    cargo = forms.CharField(label = 'Cargo:', required = False)
    identificacao = forms.CharField(label = 'Identificação:', required = True)
    nif = forms.IntegerField(label = 'NIF:', required = True)
    empresa = forms.IntegerField(label = 'NIF do criador:', required = True)
    #contactos para ser mais que um temos de fazer outra class
            
#    foto_small = models.ImageField(upload_to=settings.PEOPLE_DIR, blank=True, null=True)
#    foto_large = models.ImageField(upload_to=settings.PEOPLE_DIR, blank=True, null=True)

    #is_account_owner = models.BooleanField(default=False)
   
    class Meta:
        """
        Adiciona ao form os campos do modelo contrib.User
        """
        model = User
        fields = ('username','email','password')
    
        
    def __init__(self, *args, **kwargs):
        """
        Override de password, verificar se a pass fica encriptada
        """
        self.base_fields['password'].help_text = 'Insira uma password.'
        self.base_fields['password'].widget = forms.PasswordInput()
        super(UtilizadorForm, self).__init__(*args, **kwargs)
        
    def clean_username(self):
        """
        Procura se já existe algum utilizador com o username dado.
        """
        if User.objects.filter(username=self.cleaned_data['username'],).count():
            raise forms.ValidationError('Ja existe um utilizador com este username')
        return self.cleaned_data['username']
    
    def clean_email(self):
        """
        Procura se o email já está registado.
        """
        if User.objects.filter(
            email=self.cleaned_data['email'],
            ).count():
            raise forms.ValidationError('Ja existe um utilizador com este e-mail')
        return self.cleaned_data['email']

    def clean_confirme_a_senha(self):
        """
        Confirma a pass introduzida.
        """
        if self.cleaned_data['confirme_a_senha'] != self.data['password']:
            raise forms.ValidationError('A password não coincide!')
        return self.cleaned_data['confirme_a_senha']
    
    def clean_empresa(self):
        """
        Confirma se o criador já se encontra registado na base de dados e 
        se estiver retorna o FK do mesmo
        """
        if Breeder.objects.filter(nif = self.cleaned_data['empresa'],).count():
            return Breeder.objects.get(nif = self.cleaned_data['empresa'],)
        raise forms.ValidationError('O criador não se encontra registado.')
        
        
    def save(self):
        """
        Função que valida os dados introduzidos e grava o novo utilizador na base
        de dados.
        """
        user = User()  
        user.username = self.cleaned_data['username']
        user.email = self.cleaned_data['email']
        #user.first_name = self.cleaned_data['first_name']
        #user.last_name = self.cleaned_data['last_name']
        #user.password = self.cleaned_data['password'] 
        #só fica activo se o produtor disser que sim
        user.is_active = False
        user.set_password(self.cleaned_data['password'])
        user.save()
  
        user_profile = UserProfile()
        user_profile.user = user
        user_profile.nome_completo = self.cleaned_data['nome_completo']
        user_profile.titulo = self.cleaned_data['titulo']
        user_profile.cargo = self.cleaned_data['cargo']
        user_profile.identificacao = self.cleaned_data['identificacao']
        user_profile.nif = self.cleaned_data['nif']
        user_profile.empresa = self.cleaned_data['empresa']
        user_profile.save()
        
        #return user.id
        return {'nome': user_profile.nome_completo, 
                'cargo': user_profile.cargo,
                'bi': user_profile.identificacao, 
                'nif': user_profile.nif,
                'user_id': user.id}
        
        
class UtilizadorContactoForm(forms.ModelForm):
    
    telemovel = forms.CharField(label = 'Telemóvel:', required = True)
    telefone_empresa = forms.CharField(label = 'Telefone - Empresa:', required = False)
    telefone_empresa_ext = forms.CharField(label = 'Extensão', required = False)
    telefone_casa = forms.CharField(label = 'Telefone - Casa:', required = False)
    
    rua = forms.CharField(label = 'Rua:', required = True)
    codigo_postal = forms.CharField(label = 'Código postal:', required = True)
    distrito = forms.CharField(label = 'Distrito:', required = True)
    concelho = forms.CharField(label = 'Concelho:', required = True)
    freguesia = forms.CharField(label = 'Freguesia:', required = True)
        
    
    def save(self):
    
        user_contacto = Contact()
        user_contacto.telemovel = self.cleaned_data['telemovel']
        user_contacto.telefone_empresa = self.cleaned_data['telefone_empresa']
        user_contacto.telefone_empresa_ext = self.cleaned_data['telefone_empresa_ext']
        user_contacto.telefone_casa = self.cleaned_data['telefone_casa']
        user_contacto.rua = self.cleaned_data['rua']
        user_contacto.codigo_postal = self.cleaned_data['codigo_postal']
        user_contacto.save()
        
        user_distrito = District()
        user_distrito.distrito = self.cleaned_data['distrito']
        user_distrito.save()
        
        user_concelho = Concelho()
        user_concelho.concelho = self.cleaned_data['concelho']
        user_concelho.save()
        
        user_freguesia = Freguesia()
        user_freguesia.freguesia = self.cleaned_data['freguesia']
        user_freguesia.save()
