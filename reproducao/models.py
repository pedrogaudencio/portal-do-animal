# -*- coding: UTF-8 -*-
from django.db import models
from django.utils.translation import ugettext as _


class Mating(models.Model):
    mother = models.ForeignKey('animal.Animal', related_name='female_cobricao', blank = False)
    father = models.ForeignKey('animal.Animal', related_name='male_cobricao', null = True)
    herd = models.ForeignKey('exploracao.Herd',blank = True, null=True)
    exit_date = models.DateTimeField(_('Exit date'))
    entry_date = models.DateTimeField(_('Entry date'))
    auto_delivery_forecast = models.BooleanField(_('Automatic delivery forecast'), default = True)
    delivery_forecast = models.DateField(_('Delivery forecast'), blank = True, null = True)

    def __unicode__(self):
        return 'de %d a %d' % (self.entry_date, self.exit_date)

    class Meta():
        verbose_name = _('Mating')
        verbose_name_plural = _('Mating')


class Insemination(models.Model):
    mother = models.ForeignKey('animal.Animal', blank = False)
    date = models.DateTimeField(_('Date and hour'), blank = False) #confirmar se é preciso hora
    lot_number = models.CharField(_('Lot number'), max_length = 50, blank = True, null = True)
    inseminator = models.ForeignKey('utilizador.UserProfile', verbose_name = _('Inseminator'), 
                                    blank = True, null = True)
    auto_delivery_forecast = models.BooleanField(_('Automatic delivery forecast'), default = True)
    delivery_forecast = models.DateField(_('Delivery forecast'), blank = True, null = True)

    def __unicode__(self):
        return '%s em %d' % (self.mother, self.date)
    
    class Meta():
        verbose_name= _('Insemination')
        verbose_name_plural= _('Inseminations')
        
        
class GestationDiagnosis(models.Model):
    female = models.ForeignKey('animal.Animal', blank = False)
    result_choices = (('positive', _('Positive')),
                      ('negative', _('Negative')),
                      ('inconclusive', _('Inconclusive')) 
                      )
    result = models.CharField(_('Diagnostic'), choices = result_choices, 
                                 max_length = 15, blank = False, null = True)
    gestation_days = models.IntegerField(_('Number of gestation days'), blank = True, null = True)
    person = models.ForeignKey('utilizador.UserProfile', 
                               help_text = _('Person making diagnostic'), blank = True, null = True)
    next_diagnostic_date = models.DateTimeField(_('Next appointment'), blank = True, null = True) 
    obs = models.TextField(_('Obeservations'), blank = True, null = True)
    
    def __unicode__(self):
        return '%s - %s' % (self.female, self.result)
    
    class Meta():
        verbose_name = _('Gestation diagnosis')
        verbose_name_plural = _('Gestation diagnosis')
