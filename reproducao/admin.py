from reproducao.models import Mating, Insemination, GestationDiagnosis
from django.contrib import admin

class MatingAdmin(admin.ModelAdmin):
    """
    fieldsets = [
                 (None, {
                         'classes': ['wide', 'extrapretty'],
                         'fields': (
                                    ('numero_femea','numero_macho', 'grupo'),
                                    ('data_entrada','data_saida'),
                                    )
                         }),
    ]
    """
    list_display = ('mother','father','exit_date','entry_date')
    #list_display_links = ('__unicode__','morada')
    list_filter = ['father']
    search_fields = ['mother']
    #save_as = True

class InseminationAdmin(admin.ModelAdmin):
    """
    fieldsets = [
                 (None, {
                         'classes': ['wide', 'extrapretty'],
                         'fields': (
                                    ('numero_femea','numero_lote'),
                                    ('data_inseminacao'),
                                    )
                         }),
    ]
    """
    list_display = ('mother','date','lot_number')
    #list_display_links = ('__unicode__','morada')
    #list_filter = ['reproducao','dificuldade']
    search_fields = ['mother']
    #save_as = True

admin.site.register(Mating, MatingAdmin)
admin.site.register(Insemination, InseminationAdmin)
admin.site.register(GestationDiagnosis)