# -*- coding: UTF-8 -*-
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from performance.models import Performance
from performance import parameters
from animal.models import BreedTabulatedValue
from portaldoanimal.error import PerformanceError
from django.utils.translation import ugettext as _
from datetime import timedelta
from django.db.models import Q
from django.utils.encoding import force_unicode


class Performances():
    
    def __init__(self, animal, farm):
        self.animal = animal
        self.farm = farm
    
    
    def maternal_ability(self):
        '''
        Performance ao aleitamento
        '''
        #for the animal x, search for any performance object 
        try:
            animal_performance = Performance.objects.get(animal = self.animal)
        except MultipleObjectsReturned:
            return PerformanceError().except_2002()
        except ObjectDoesNotExist:
            return _(' Animal: %s - ') % str(self.animal) + str(PerformanceError().except_2003())
        
        #search for Aleitamento evaluation
        rates = animal_performance.get_rates()
        try: 
            return rates['Aleitamento']
        except KeyError:
            if self.animal.get_specie() == 'Bovine':
                return self._get_maternal_ability_bovine(animal_performance)
            if self.animal.get_specie() in ['Caprine', 'Ovine']:
                return self._get_maternal_ability_caprine_ovine(animal_performance)
            return _('Avaliacao ao aleitamento nao esta disponivel.')

    
    def reproductive_ability(self):
        '''
        Performance á recria
        '''
        #for the animal x, search for any performance object 
        try:
            animal_performance = Performance.objects.get(animal = self.animal)
        except MultipleObjectsReturned:
            return PerformanceError().except_2002()
        except ObjectDoesNotExist:
            return _(' Animal: %s - ') % force_unicode(self.animal) + str(PerformanceError().except_2003())
        
        #search for Aleitamento evaluation
        rates = animal_performance.get_rates()
        try: 
            return rates['Recria']
        except KeyError:
            if self.animal.get_specie() == 'Bovine':
                return self._get_reproductive_ability_bovine(animal_performance)
            if self.animal.get_specie() in ['Caprine', 'Ovine']:
                return self._get_reproductive_ability_caprine_ovine(animal_performance)
            return _('Avaliacao a recria nao esta disponivel.')
            
    
    def _get_reproductive_ability_caprine_ovine(self, animal_performance):        
        '''
        Caprines and ovines
        '''    
        birth_weight = None
        used_reference_weight = False
        
        weight_list = animal_performance.weighing.order_by('date')
        #get birth weight
        try: 
            birth_weight = weight_list.get(animal_status__status_pt = 'Nascimento')
        except ObjectDoesNotExist:
            try:
                #get reference weight
                birth_weight = self._get_reference_birth_weight(self.animal.specie_breed, self.animal.sex)
                used_reference_weight = True
            except MultipleObjectsReturned:
                return PerformanceError().except_2004()
            except ObjectDoesNotExist:
                return _(' Animal: %s - ') % self.animal + PerformanceError().except_2005()
 
        print 'Pesagem ao nascimento: %s | Usado peso Referencia: %s ' % (birth_weight, used_reference_weight)
        #print weight_list
        print 'Data de nascimento + 70: ' + str(self.animal.birthdate + timedelta(days = 70))
        #print 'Min: %s' % (self.animal.birthdate + timedelta(days = 21)) 
        #print 'Max: %s' % (self.animal.birthdate + timedelta(days = 46)) 
        
        
        #list with weighings in parameters date range
        list_d2 = weight_list.filter(
                                 date__range = ((self.animal.birthdate + timedelta(days = 59)), 
                                                (self.animal.birthdate + timedelta(days = 92))))
        
        #one weighing on that range
        if len(list_d2) == 1:
            print '[PN: %s || P2: %s]' % (birth_weight, list_d2[0])
    
        #Warnings if there aren't any weighing in parameters date range    
        if not list_d2 and len(weight_list) < 1:
            #Alterar avisos
            return _('Avaliacao a recria nao esta disponivel. Segunda pesagem nao existe')
        if not list_d2 and len(weight_list) > 1:
            #Alterar avisos
            return _('Avaliacao ao recria nao esta disponivel. Segunda pesagem nao esta dentro dos parametros')
        
        #more than one weighings in range, choose the one close to midle_date parameter
        if list_d2 > 1:
            d2_2 = None
            teste = timedelta(days = 1000)
            for d2 in list_d2:
                print 'Diferença entre as duas datas :' + str(abs(d2.date - (self.animal.birthdate + timedelta(days = 30))))
                if abs(d2.date - (self.animal.birthdate + timedelta(days = 70))) < teste:
                    teste = abs(d2.date - (self.animal.birthdate + timedelta(days = 70)))
                    d2_2 = d2
                    
            print '[PN: %s || P2: %s]' % (birth_weight, d2_2)
            return '[PN: %s || P2: %s]' % (birth_weight, d2_2)
            

    def _get_maternal_ability_caprine_ovine(self, animal_performance):
        '''
        Caprines and ovines
        '''
        
        birth_weight = None
        used_reference_weight = False
        
        weight_list = animal_performance.weighing.order_by('date')
        #get birth weight
        try: 
            birth_weight = weight_list.get(animal_status__status_pt = 'Nascimento')
        except ObjectDoesNotExist:
            try:
                #get reference weight
                birth_weight = self._get_reference_birth_weight(self.animal.specie_breed, self.animal.sex)
                used_reference_weight = True
            except MultipleObjectsReturned:
                return PerformanceError().except_2004()
            except ObjectDoesNotExist:
                return _(' Animal: %s - ') % self.animal + PerformanceError().except_2005()
 
        print 'Pesagem ao nascimento: %s | Usado peso Referencia: %s ' % (birth_weight, used_reference_weight)
        #print weight_list
        print 'Data de nascimento + 30: ' + str(self.animal.birthdate + timedelta(days = 30))
        #print 'Min: %s' % (self.animal.birthdate + timedelta(days = 21)) 
        #print 'Max: %s' % (self.animal.birthdate + timedelta(days = 46)) 
        
        
        #list with weighings in parameters date range
        list_d2 = weight_list.filter(
                                 date__range = ((self.animal.birthdate + timedelta(days = 21)), 
                                                (self.animal.birthdate + timedelta(days = 46))))

        #one weighing on that range
        if len(list_d2) == 1:
            print '[PN: %s || P2: %s]' % (birth_weight, list_d2[0])
    
        #Warnings if there aren't any weighing in parameters date range    
        if not list_d2 and len(weight_list) < 1:
            return _('Avaliacao ao aleitamento nao esta disponivel. Segunda pesagem nao existe')
        if not list_d2 and len(weight_list) < 2:
            return _('Avaliacao ao aleitamento nao esta disponivel. Segunda pesagem nao esta dentro dos parametros')
        
        #more than one weighings in range, choose the one close to midle_date parameter
        if list_d2 > 1:
            d2_2 = None
            teste = timedelta(days = 1000)
            for d2 in list_d2:
                print 'Diferença entre as duas datas :' + str(abs(d2.date - (self.animal.birthdate + timedelta(days = 30))))
                if abs(d2.date - (self.animal.birthdate + timedelta(days = 30))) < teste:
                    teste = abs(d2.date - (self.animal.birthdate + timedelta(days = 30)))
                    d2_2 = d2
                    
            print '[PN: %s || P2: %s]' % (birth_weight, d2_2)
            return '[PN: %s || P2: %s]' % (birth_weight, d2_2)

    def _get_reproductive_ability_bovine(self, animal_performance):
        
        #birth_weight = None
        #used_reference_weight = False
        
        #fazer a equação com a correcção peso/idade
        print 'Data de nascimento + 210: ' + str(self.animal.birthdate + timedelta(days = 210))
        
        #list with weigth's
        weight_list = animal_performance.weighing.order_by('date')
        
        if len(weight_list) < 1: #0 weight
            return _('Numero de pesagens insuficiente')
        
        print self._get_weights_lists(weight_list, parameters.BOVINE_210['midle_date'])
        list_d1, list_d2 = self._get_weights_lists(weight_list, parameters.BOVINE_210['midle_date'])
    
        #A1    
        if list_d1 and list_d2:
            peso_1, peso_2 = self._get_first_2_weights(parameters.BOVINE_120['delta_min'], parameters.BOVINE_120['delta_max'], list_d1, list_d2)
            print 'A1 - [P1: %s || P2: %s]' % (peso_1,  peso_2)
            return 'A1 - [P1: %s || P2: %s]' % (peso_1,  peso_2)
        #A2
        #print (list_d1[0].date,  (self.animal.birthdate + timedelta(days = 210 - 30)))
        if not list_d2 and list_d1[0].date >= (self.animal.birthdate + timedelta(days = 210 - 30)):
            peso_1, peso_2 = self._get_first_2_weights(parameters.BOVINE_120['delta_min'], parameters.BOVINE_120['delta_max'], list_d1, list_d2)
            print 'A2 - [P1: %s || P2: %s]' % (peso_1,  peso_2)
            return 'A2 - [P1: %s || P2: %s]' % (peso_1,  peso_2)
         
        #A3
        #print (list_d2[0].date,  (self.animal.birthdate + timedelta(days = 210 + 30)))
        if not list_d1 and list_d2[0].date <= (self.animal.birthdate + timedelta(days = 210 + 30)): 
            peso_1, peso_2 = self._get_first_2_weights(parameters.BOVINE_120['delta_min'], parameters.BOVINE_120['delta_max'], list_d1, list_d2)
            print 'A3 - [P1: %s || P2: %s]' % (peso_1,  peso_2)
            return 'A3 - [P1: %s || P2: %s]' % (peso_1,  peso_2)
    
    
    
    def _get_maternal_ability_bovine(self, animal_performance):        
        
        birth_weight = None
        used_reference_weight = False
        
        #fazer a equação com a correcção peso/idade
        print 'Data de nascimento + 120: ' + str(self.animal.birthdate + timedelta(days = 120))
        
        #list with weigth's
        weight_list = animal_performance.weighing.order_by('date')
        
        #search for 2 weights within the parameters
        if len(weight_list) < 1: #0 weight
            return _('Numero de pesagens insuficiente')
        
        #get birth weight
        try: 
            birth_weight = weight_list.get(animal_status__status_pt = 'Nascimento')
        except ObjectDoesNotExist:
            try:
                #get reference weight
                birth_weight = self._get_reference_birth_weight(self.animal.specie_breed, self.animal.sex)
                used_reference_weight = True
            except MultipleObjectsReturned:
                return PerformanceError().except_2004()
            except ObjectDoesNotExist:
                return _(' Animal: %s - ') % self.animal + PerformanceError().except_2005()
 
        print 'Pesagem ao nascimento: %s | Usado peso Referencia: %s ' % (birth_weight, used_reference_weight)
    
        
        list_d1, list_d2 = self._get_weights_lists(weight_list, parameters.BOVINE_120['midle_date'])
        
        #A1    
        if list_d1 and list_d2:
            peso_1, peso_2 = self._get_first_2_weights(parameters.BOVINE_120['delta_min'], parameters.BOVINE_120['delta_max'], list_d1, list_d2)
            print 'A1 - [P1: %s || P2: %s]' % (peso_1,  peso_2)
            return 'A1 - [P1: %s || P2: %s]' % (peso_1,  peso_2)

        #A2
        if not list_d2 and list_d1[0].date >= (self.animal.birthdate + timedelta(days = 120 - 30)):
            peso_1, peso_2 = self._get_first_2_weights(parameters.BOVINE_120['delta_min'], parameters.BOVINE_120['delta_max'], list_d1, list_d2)
            print 'A2 - [P1: %s || P2: %s]' % (peso_1,  peso_2)
            return 'A2 - [P1: %s || P2: %s]' % (peso_1,  peso_2)
        #A3
        if not list_d1 and list_d2[0].date <= (self.animal.birthdate + timedelta(days = 120 + 30)): 
            peso_1, peso_2 = self._get_first_2_weights(parameters.BOVINE_120['delta_min'], parameters.BOVINE_120['delta_max'], list_d1, list_d2)
            print 'A3 - [P1: %s || P2: %s]' % (peso_1,  peso_2)
            return 'A3 - [P1: %s || P2: %s]' % (peso_1,  peso_2)
    
        
    def _get_weights_lists(self, weight_list, midle_date):
        '''
        Returns two lists, one with  
        Procura as pesagens dentro dos parametros mas retira a pesagem ao nascimento
        '''
        #print self.animal.birthdate
        #print self.animal.birthdate  + timedelta(days = midle_date)
        
        list_d1 = weight_list.filter(~Q(animal_status__status_pt = 'Nascimento'), 
                                     date__lte = self.animal.birthdate \
                                     + timedelta(days = midle_date)).order_by('-date')
        
        list_d2 = weight_list.filter(~Q(animal_status__status_pt = 'Nascimento'), 
                                     date__gte = self.animal.birthdate \
                                     + timedelta(days = midle_date)).order_by('date')
        
        return list_d1, list_d2
        
        
    def _get_first_2_weights(self, delta_min, delta_max, list_d1 = None, list_d2 = None):
        '''
        Return the two first dates that satisfy the max and min number of days between the to.
        '''
        #A1 and B1
        a1_1 = timedelta(days = delta_max)
        if list_d1 and list_d2:
            #procura a menor diferença entre datas
            #Fixa a lista anterior à data intermédia
            for d1 in list_d1:
                for d2 in list_d2:
                    print 'Diferença entre as duas datas :' + str(d2.date - d1.date) + '  | (data 1, data 2) : (%s | %s)' % (d1.date,d2.date) 
                    if timedelta(days = delta_min) <= (d2.date - d1.date) <= timedelta(days = delta_max) and (a1_1 >= (d2.date - d1.date)):
                        a1_1 = (d2.date - d1.date)
                        d1_1 = d1
                        d2_1 = d2
            
            return d1_1, d2_1
                            
        #A2 and B2
        if list_d1 and not list_d2:
            for d1 in list_d1[1:]:
                print 'Diferença entre as duas datas :' + str(list_d1[0].date - d1.date)
                if timedelta(days = delta_min) <= (list_d1[0].date - d1.date) <= timedelta(days = delta_max):
                    d2 = list_d1[0]
                    return d1, d2
 
        #A3 and B3
        if not list_d1 and list_d2:
            for d2 in list_d2[1:]:
                print 'Diferença entre as duas datas :' + str(d2.date - list_d2[0].date)
                if timedelta(days = delta_min) <= (d2.date - list_d2[0].date) <= timedelta(days = delta_max):
                    d1 = list_d2[0]
                    return d1, d2

        return [], []


    def _get_reference_birth_weight(self, breed, sex):
        '''
        Return the birth weight for a given breed and sex
        '''
        return BreedTabulatedValue.reference_weight.get(specie_breed = breed, sex__in = ['A', sex])
