# -*- coding: UTF-8 -*-
from django.contrib.auth.decorators import login_required
from django.template.context import RequestContext
from django.shortcuts import render_to_response 
from exploracao.models import Farm
from performance.models import Weighing, AnimalStatus
from performance.forms import WeighingForm, AddAnimalStatus, WeighingModifyForm
from notification.models import Notice
from utils.make_initial_data import makeInitialData
from performance.performances import Performances
from animal.models import Animal
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.contrib import messages
from django.utils.translation import ugettext as _
from django.core.exceptions import ObjectDoesNotExist


@login_required
def animal_status_list(request, slug = None):
    """
    List all animal status for a given farm
    """
    farm = Farm.objects.get(slug = slug)
    animal_status = AnimalStatus.objects.filter(farm = farm)
    
    context = {'animal_status': animal_status
               }
    
    return render_to_response('performance/animal_status_list.html', context)


@login_required
def animal_status(request, slug = None, animal_state_pk = None):
    """
    Animal status when they are weighted
    """
    notices = Notice.objects.filter(recipient = request.user, unseen = True)
    farm = Farm.objects.get(slug = slug)

    try:    
        initial_data = makeInitialData(AnimalStatus.objects.get(pk = animal_state_pk))
    except:
        initial_data = None
        
    #TODO: como gravar em diferente linguas
    lang = 'pt'

    if request.method == 'POST':
        if 'submit' in request.POST:
            form = AddAnimalStatus(request.POST, farm = farm, 
                                   animal_state_pk = animal_state_pk, lang = lang)
            if form.is_valid():
                form.save()
                return HttpResponseRedirect(reverse('animal_status_list', kwargs={'slug': farm.slug}))
    else:
        form = AddAnimalStatus(farm = farm, animal_state_pk = animal_state_pk, 
                               initial = initial_data, lang = lang)
        
    context = {'form': form,
               }
    return render_to_response('performance/animal_state.html', context,
                              context_instance=RequestContext(request))


@login_required
def weighing_list(request, slug = None):
    """
    List weighing
    """
    farm = Farm.objects.get(slug = slug)
    
    context = {'weighing_list': Weighing.objects.filter(performance__animal__farm = farm)
               }
    
    return render_to_response('performance/weight_list.html', context)


@login_required
def weighing_add(request, slug = None, weighing_pk = None):
    """
    Add weighing
    """    
    farm = Farm.objects.get(slug = slug)
    
    is_add = True
    
    if request.method == 'POST':
        form = WeighingForm(request.POST, farm = farm)
        if ('submit' or 'submit_add_other' in request.POST) and form.is_valid():
            new_weighing = form.save()
            messages.add_message(request, messages.SUCCESS, 
                                 _('Contrast for animal: %s, added with success') %\
                                 new_weighing.performance.animal)
            
            if 'submit_add_other' in request.POST:
                form = WeighingForm(farm = farm,
                                    initial = {'date': new_weighing.date,
                                               'equipment': new_weighing.equipment,
                                               'supervisor': new_weighing.supervisor,
                                               'animal_status': new_weighing.animal_status,
                                               })
        else:
            print form
            messages.add_message(request, messages.ERROR, 
                                 _('Fail while attempting to add weighing!'))
    else:
        form = WeighingForm(farm = farm)
        
    context = {'form': form,
               'is_add': is_add,
               }
    
    return render_to_response('performance/weight_add.html', context,
                              context_instance=RequestContext(request))


@login_required
def weighing_modify(request, slug = None, weighing_pk = None):
    """
    Modify Weighing
    """    
    farm = Farm.objects.get(slug = slug)
    try:
        weighing = Weighing.objects.get(id = weighing_pk) #object instance
        is_add = False
    except ObjectDoesNotExist:
        return render_to_response('performance/weight_add.html', #TODO: Alterar para página de erro
                                       context_instance=RequestContext(request))

    if request.method == 'POST':
        form = WeighingModifyForm(request.POST, farm = farm, instance = weighing)
        if 'submit' in request.POST and form.is_valid():
            new_weighing = form.save()
            messages.add_message(request, messages.SUCCESS, 
                                 _('Contrast for animal: %s, has been modified with success') %\
                                 new_weighing.performance.animal)
            #TODO: Go to somewhere
        else:
            messages.add_message(request, messages.ERROR, 
                                 _('Fail while attempting to add weighing!'))
    else:
        form = WeighingModifyForm(farm = farm, instance = weighing)
        
    context = {'form': form,
               'is_add': is_add,
               'weighing': weighing,
               }
    
    return render_to_response('performance/weight_add.html', context,
                              context_instance=RequestContext(request))


@login_required
def animal_performance(request, slug = None, animal_pk = None, performance = None):
    
    animal = Animal.objects.get(id = animal_pk)
    farm = Farm.objects.get(slug = slug)

    if performance == 'maternal':
        form = Performances(animal, farm).maternal_ability()
    if performance == 'reproductive':
        form = Performances(animal, farm).reproductive_ability()
    
    
    context = {'form': form,
               }
    return render_to_response('performance/performance.html', context,
                              context_instance=RequestContext(request))
