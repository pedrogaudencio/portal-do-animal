# -*- coding: UTF-8 -*-
from performance.models import Performance, Rate, RateType, Weighing, AnimalStatus
from django.contrib import admin

    
class RateInline(admin.TabularInline):
    model = Rate


class WeighingInline(admin.TabularInline):
    model = Weighing


class PerformanceAdmin(admin.ModelAdmin): 
    inlines = [RateInline, WeighingInline]
    
    
    def get_object(self, request, object_id):
        # Hook obj for use in formfield_for_manytomany
        self.obj = super(PerformanceAdmin, self).get_object(request, object_id)
        return self.obj

    
    def formfield_for_manytomany(self, db_field, request, **kwargs):
        if db_field.name == 'weighing' and getattr(self, 'obj', None):
            kwargs["queryset"] = Weighing.objects.filter(performance = getattr(self, 'obj', None))
        if db_field.name == 'rate':
            kwargs["queryset"] = Rate.objects.filter(performance = getattr(self, 'obj', None))
        return super(PerformanceAdmin, self).formfield_for_manytomany(db_field, request, **kwargs)
    

class AnimalStatusAdmin(admin.ModelAdmin):
    list_filter = ('all_farms', 'farm',)


admin.site.register(Performance, PerformanceAdmin)
admin.site.register(Rate)
admin.site.register(RateType)
admin.site.register(Weighing)
admin.site.register(AnimalStatus, AnimalStatusAdmin)