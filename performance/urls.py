# -*- coding: UTF-8 -*-
from django.conf.urls.defaults import patterns, url


"""
Performance urls
"""
urlpatterns = patterns('performance.views',
    #--------------Weighing-------------#
    url(r'^weighing/(?P<slug>[-\w\d]+)/$', 'weighing_list'), #List 
    url(r'^weighing/(?P<slug>[-\w\d]+)/add/$', 'weighing_add'), #Add  
    url(r'^weighing/(?P<slug>[-\w\d]+)/(?P<weighing_pk>\d+)/edit/$', 'weighing_modify'), #Edit 
    #------------Animal State-----------#
    url(r'^animal_status/(?P<slug>[-\w\d]+)/add/$', 'animal_status'), #Add
    url(r'^animal_status/(?P<slug>[-\w\d]+)/(?P<animal_state_pk>\d+)/edit/$', 'animal_status'), #Edit
    url(r'^animal_status/(?P<slug>[-\w\d]+)/$', 'animal_status_list', name = 'animal_status_list'), #List
    
    #------------Performance------------#
    # TEST retorna as melhores duas para fazer as contas para as avaliações
    url(r'^maternal/(?P<slug>[-\w\d]+)/(?P<animal_pk>\d+)/$', 'animal_performance', 
                                                    {'performance': 'maternal'}, name = 'maternal'), 
    url(r'^reproductive/(?P<slug>[-\w\d]+)/(?P<animal_pk>\d+)/$', 'animal_performance',
                                                    {'performance': 'reproductive'}, name = 'reproductive'), 
)
