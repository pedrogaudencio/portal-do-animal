# -*- coding: UTF-8 -*-
from django.db import models
from django.utils.translation import ugettext as _
from transmeta import TransMeta
from django.utils.encoding import force_unicode


class Performance(models.Model):
    animal = models.ForeignKey('animal.Animal', blank = False, null = False)

    
    def __unicode__(self):
        return '%s' % force_unicode(self.animal)
    
    
    def get_rates(self):
        """
        Return a dictionary with all evaluation for the animal x 
        """
        rates = {}
        
        for rate in self.rate_set.all():
            rates[rate.rate_type.name_pt] = rate.value
        
        return rates
    
    
    class Meta():
        verbose_name = _('Performance')
        verbose_name_plural = _('Performances')


class Rate(models.Model):
    performance = models.ForeignKey('Performance', verbose_name = _('Performance'))
    rate_type = models.ForeignKey('RateType', verbose_name = _('Evaluation type'))
    value = models.CharField(verbose_name = _('Value'), max_length = 50, null = True, blank = True)
    #colcocar o tipo de rate que foi: A1, A2, A3 ect...

    def __unicode__(self):
        return '%s - %s' % (self.performance, self.rate_type)


    class Meta():
        verbose_name = _('Evaluation')
        verbose_name_plural = _('Evaluations')


class RateType(models.Model):
    __metaclass__ = TransMeta
    name = models.CharField(verbose_name = _('Name'), max_length = 50, null = False, blank = False)
    farm = models.ForeignKey('exploracao.Farm', verbose_name = _('Farm'), 
                             null = True, blank = True)
    all_farms = models.BooleanField(verbose_name = _('Availed to all farms'), default = True)


    def __unicode__(self):
        return '%s - %s' % (self.name, self.all_farms)


    class Meta():
        verbose_name = _('Type of evaluation')
        verbose_name_plural = _('Types of evaluation')
        translate = ('name',)


class Weighing(models.Model):
    performance = models.ForeignKey('Performance', verbose_name = _('Performance'))
    weight = models.CharField(verbose_name = _('Weight'), max_length = 10, null = False, blank = False)
    date = models.DateField(verbose_name = _('Date'), null = False, blank = False)
    equipment = models.CharField(verbose_name = _('Equipment reference'), max_length = 50, 
                                 null = True, blank = True)
    supervisor = models.CharField(verbose_name = _('Supervisor'), max_length = 50, 
                                  null = True, blank = True)# colocar entidade ou colaborador
    #body_condition =
    animal_status = models.ForeignKey('AnimalStatus', verbose_name = _('Animal state'))
    obs = models.TextField(verbose_name = _('Observations'), null = True, blank = True)


    def __unicode__(self):
        return '%s - %s - %s' % (force_unicode(self.performance), self.date, self.weight)


    class Meta():
        unique_together = ('performance', 'date', 'animal_status')
        verbose_name = _('Weighing')
        verbose_name_plural = _('Weighings')


    def unique_error_message(self, model_class, unique_check):
        if model_class == type(self) and unique_check == ('performance', 'date', 'animal_status'):
            return _('You already have one weighing for %s on this date.') % self.animal_status
        else:
            return super(Weighing, self).unique_error_message(model_class, unique_check)
        
        
    def clean(self):
        """
        Convert weight string value into float
        """
        from utils.to_float import to_float
        if self.weight:
            self.weight = to_float(self.weight)

    
    def get_absolute_url(self):
        return '%s/edit' % self.id


class AnimalStatus(models.Model):
    __metaclass__ = TransMeta
    status = models.CharField(verbose_name = _('State'), max_length = 50, null = True, blank = True)
    farm = models.ForeignKey('exploracao.Farm', verbose_name = _('Farm'), 
                             null = True, blank = True)
    all_farms = models.BooleanField(verbose_name = _('Availed to all farms'), default = True)


    def __unicode__(self):
        return '%s' % (self.status)


    class Meta():
        verbose_name = 'Animal status'
        verbose_name_plural = 'Animal status'
        translate = ('status',)


    def get_absolute_url(self):
        return '%s/edit' % self.id

