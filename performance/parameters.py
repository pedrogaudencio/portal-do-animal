# -*- coding: UTF-8 -*-
'''
File with parameters for animal performance
'''

'''
@var BOVINE_120: Para tal, devem ser asseguradas duas pesagens, realizadas com mais de 60
                dias de intervalo e menos de 210 dias. As duas pesagens devem ser realizadas
                respetivamente, antes e após os 120 dias de idade. No entanto, a primeira ou a segunda
                pesagem podem ser realizadas, respetivamente 30 dias depois ou 30 dias antes dos 120 dias
                de idade. O Peso ao Nascimento (PN) (ou o peso de referência (PR) da raça ao
                nascimento) pode eventualmente ser utilizado como a primeira pesagem.

'''
BOVINE_120 = {'delta_min': 60,
              'delta_max': 210,
              'midle_date': 120,
              'deviation': 30,
              }

'''
@var BOVINE_210: Por norma as duas pesagens devem ser
                realizadas com mais de 60 dias de intervalo e menos de 300 dias (10 meses). As duas
                pesagens devem ser realizadas respetivamente, antes e após os 210 dias de idade. No
                entanto, a primeira ou a segunda pesagem podem ser realizadas, respetivamente, 30 dias
                depois ou 30 dias antes dos 210 dias de idade.
'''
BOVINE_210 = {'delta_min': 60,
              'delta_max': 300,
              'midle_date': 210,
              'deviation': 30,
              }

'''
@var BOVINE_CARCASS: Só até 30% dos controlos de crescimento é que podem utilizar peso de
                    carcaça.Os animais têm de sair diretamente da exploração para o matadouro (manter a
                    regras de até 2 dias).
'''

BOVINE_CARCASS = {'max_number_controls': 0.3,
                  'days_range': 2,
                  }


'''
@var CAPRINE_OVINE_30: Esta avaliação é realizada mediante uma pesagem nas crias entre os 21 e os 46 dias de idade (P2)
                    e tendo como base o peso ao nascimento (PN) ou um peso de referência da raça ao
                    nascimento (PR).
'''

CAPRINE_OVINE_30 = {'deviation_min': 21,
                    'deviation_max': 46,
                    'midle_date': 30,
                    }

'''
@var CAPRINE_OVINE_70: A avaliação da
                    recria será realizado com pelo menos uma pesagem complementar entre os 59 e os 92 dias
                    de vida (P3), isto é cerca de 38 a 46 dias após a primeira pesagem de forma a permitir o
                    calculo do entre estas duas pesagens que será considerado o Ganho Médio Diário 30-70
                    dias (GMD 30-70), de cada cria.
'''

CAPRINE_OVINE_70 = {'deviation_min': 59,
                    'deviation_max': 92,
                    'midle_date': 70,
                    }




