# -*- coding: UTF-8 -*-
from django import forms
from django.db.models import Q
from portaldoanimal import settings, error
from utils.widgets import DatePickerWidget, ChosenSingleWidget
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from performance.models import AnimalStatus, Weighing, Performance
from animal.models import Animal 
from utils.to_float import to_float
from django.utils.translation import ugettext as _
from django.forms.widgets import HiddenInput, Widget, RadioInput
from django.forms.models import ModelChoiceField


class AddAnimalStatus(forms.Form):
    """
    Form for animal status
    """
    def __init__(self, *args, **kwargs):
        self.farm = kwargs.pop('farm', None)
        self.animal_state_pk = kwargs.pop('animal_state_pk', None)
        self.lang = '_' + kwargs.pop('lang', 'pt')
        super(AddAnimalStatus, self).__init__(*args, **kwargs)
        
        self.fields['status' + self.lang] = forms.CharField(label = _('Status'), max_length = 50)


    def clean_state(self):
        """
        Verify if the state is unique
        """
        status = self.cleaned_data['status' + self.lang]

        for lang, dont_give_a_fuck in settings.LANGUAGES:
            field_status = 'status_%s' % lang
            
            try:
                if AnimalStatus.objects.get(Q(**{field_status: status, 'farm': self.farm}) | Q(**{field_status: status, 'all_farms': True})):
                    raise forms.ValidationError(_('Already exists one Indentifier type with this name'))
            except MultipleObjectsReturned:
                raise forms.ValidationError(error.PerformanceError().except_2001())
            except ObjectDoesNotExist:
                pass
        
        return status
            

    def save(self):
        try:
            animal_status = AnimalStatus.objects.get(id = self.animal_state_pk)
        except:
            animal_status= AnimalStatus()
        
        for lang, dont_give_a_fuck in settings.LANGUAGES:
            field_status = 'status_%s' % lang
            setattr(animal_status, field_status, self.cleaned_data['status' + self.lang])

        animal_status.farm = self.farm
        animal_status.all_farms = False
        animal_status.save()


class WeighingModifyForm(forms.ModelForm):
    """
    Form to modify
    """
    def __init__(self, *args, **kwargs):
        self.farm = kwargs.pop('farm', None)
        super(WeighingModifyForm, self).__init__(*args, **kwargs)
        if self.instance:
            self.fields['animal_status'].queryset = \
                        AnimalStatus.objects.filter(Q(farm = self.farm)|  Q(all_farms = True))


    class Meta():
        model = Weighing
        widgets = {'performance': forms.HiddenInput(),
                   'weight': forms.TextInput(attrs={'class': 'input-block-level'}),
                   'date': DatePickerWidget(),
                   'equipment': forms.TextInput(attrs={'class': 'input-block-level'}),
                   'supervisor': forms.TextInput(attrs={'class': 'input-block-level'}),
                   'animal_status': forms.Select(attrs={'class': 'input-block-level'}),
                   'obs': forms.Textarea(attrs={'rows':2, 'cols':40}),
                   }


class ModelChoiceField_NEW_unicode(ModelChoiceField):
    def label_from_instance(self, obj):
        """
        This method is used to convert objects into strings; it's used to
        generate the labels for the choices presented by this object. Subclasses
        can override this method to customize the display of the choices.
        """
        if obj.sia:
            return (obj.sia.number)
        else:
            return (obj)


class WeighingForm(forms.ModelForm):
    """
    Form for weighing
    """
    def __init__(self, *args, **kwargs):
        self.farm = kwargs.pop('farm', None)
        super(WeighingForm, self).__init__(*args, **kwargs)
        if self.instance:
            self.fields['animal_status'].queryset = \
                        AnimalStatus.objects.filter(Q(farm = self.farm)|  Q(all_farms = True))
        
        self.fields['performance'] = ModelChoiceField_NEW_unicode(Animal.actives.filter(farm = self.farm), 
                                                            label = _('Animal'), 
                                                            widget = ChosenSingleWidget)
    
    
    def clean_performance(self):
        """
        Verify if the animal belongs to that farm
        """
        animal = self.cleaned_data['performance']
        
        try:
            performance = Performance.objects.get(animal = animal)
        except:
            try:
                performance = Performance()
                performance.animal = animal
                performance.save()
            except:
                raise forms.ValidationError(str(error.PerformanceError().except_2006()))
            
        return performance
    
    
    class Meta():
        model = Weighing
        exlude = ['performance']
        widgets = {'weight': forms.TextInput(attrs={'class': 'input-block-level'}),
                   'date': DatePickerWidget(),
                   'equipment': forms.TextInput(attrs={'class': 'input-block-level'}),
                   'supervisor': forms.TextInput(attrs={'class': 'input-block-level'}),
                   'animal_status': ChosenSingleWidget(),
                   'obs': forms.Textarea(attrs={'rows':2, 'cols':40}),
                   }
