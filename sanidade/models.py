# -*- coding: UTF-8 -*-
from django.db import models
from django.utils.translation import ugettext as _
from transmeta import TransMeta


class InterventionEntry(models.Model):
    animals = models.ManyToManyField('animal.Animal', verbose_name= _('Animals list'), 
                                     blank = False, null = False)
    timestamp = models.DateTimeField(_('Date and time'), blank = False, null =False)

    intervention = models.ForeignKey('sanidade.Intervention', verbose_name=_('Intervention'))
    index_measurements_values = models.ManyToManyField('sanidade.IndexMeasurementsValues', 
                                                       verbose_name=_('Measurements index values'), 
                                                       blank = True, null = True)

    ''' FALTAM VALORES DE INDICES '''

    def __unicode__(self):
        return '%s' % (self.interventions)

    class Meta():
        verbose_name=_('Intervention entry')
        verbose_name_plural= _('Interventions entry')


class Intervention(models.Model):
    __metaclass__ = TransMeta
    name = models.CharField(_('Name'), max_length = 30, blank = False, null= False)
    is_mandatory = models.BooleanField(_('Required by law'))
    is_periodic = models.BooleanField(_('Regular'))
    PERIOD_OPTIONS = (('y', _('Year')),
                      ('m', _('Month')),
                      ('w', _('Week')),
                      ('d', _('Day')),
                      )
    period = models.CharField(_('Period'), max_length=1, choices=PERIOD_OPTIONS, blank = True, null = True)
    periodicy = models.PositiveIntegerField(_('Regularity'), 
                                            help_text=_('De quantos em quantos anos/meses/semanas/dias'), 
                                            blank = True, null = True)
    #mandatory_areas = models.ManyToManyField('cities_light.City', verbose_name='Zonas de risco', blank = True, null = True)

    index_measurements = models.ManyToManyField('sanidade.IndexMeasurements', verbose_name=_('Measurements index'))

    actions = models.ManyToManyField('sanidade.Action', verbose_name=_('Actions'), blank = False, null = False)

    def __unicode__(self):
        return '%s' % (self.name)

    class Meta():
        verbose_name= _('Intervention')
        verbose_name_plural= _('Interventions')
        translate = ('name',)


class IndexMeasurements(models.Model):
    __metaclass__ = TransMeta
    name = models.CharField(_('Index name'), max_length = 30, blank = False, null= False)
    measurement_count = models.PositiveIntegerField(_('Number of measurement required'), 
                                                    blank = False, null = False)
    measurement_interval = models.PositiveIntegerField(_('Number of days between measurements'), 
                                                       blank = True, null = True)

    def __unicode__(self):
        return '%s' % (self.name)

    class Meta():
        verbose_name= _('Index measurement')
        verbose_name_plural = _('Index measurements')
        translate = ('name',)


class IndexMeasurementsValues(models.Model):
    index = models.ForeignKey('sanidade.IndexMeasurements')
    value = models.PositiveIntegerField(_('Index value'), blank = True, null = True)
    intervention_entry = models.ForeignKey('sanidade.InterventionEntry')

    def __unicode__(self):
        return '%d' % (self.value)

    class Meta():
        verbose_name=_('Index measurement value')
        verbose_name_plural=_('Index measurement values')


class Action(models.Model):
    #TYPE_OPTIONS = (('',''),)
    #type=models.CharField('Tipo de ação', max_length=5, choices=TYPE_OPTIONS, blank = False, null = False)
    disease = models.ForeignKey('sanidade.Disease', verbose_name = _('Disease(s)'), 
                                     blank = True, null = True)

    class Meta():
        verbose_name=_('Action')
        verbose_name_plural=_('Actions')


class Vaccination(Action):
    vaccine = models.ForeignKey('sanidade.Vaccine', verbose_name = _('Vaccine'), blank = False, null = False)
    vaccine_lot = models.CharField(_('Lot'), max_length = 50, blank = False)
    quantity = models.PositiveIntegerField(_('Delivered quantity'), blank = False, null = False)
    staff = models.ForeignKey('utilizador.UserProfile', verbose_name = _('Tratador'), 
                                   blank = True, null = True)
    withdrawal = models.PositiveIntegerField(_('Safety interval'), 
                                                      help_text = _('Safety interval in days'), 
                                                      blank = False, null = False)

    def __unicode__(self):
        return '%s (%s - %s)' % (self.vaccine, self.quantity, self.vaccine_lot)

    class Meta():
        verbose_name=_('Vaccination')
        verbose_name_plural=_('Vaccinations')
        

class MedicalCare(Action):
    problem = models.TextField(_('Problem'), blank = False, null = False)
    treatment = models.TextField(_('Treatment description'), blank = True, null = True)
    staff = models.ManyToManyField('utilizador.UserProfile', verbose_name = _('Staff list'), 
                                   blank = True, null = True)
    obs = models.TextField(_('Observations'), blank=True, null=True)

    def __unicode__(self):
        return '%s (%s)' % (self.problem, self.disease)

    class Meta():
        verbose_name= _('Medical care')
        verbose_name_plural= _('Medical care')


class Disease(models.Model):
    __metaclass__ = TransMeta
    name = models.CharField(_('Name'), max_length = 500, blank = False, null = False)
    causes = models.TextField(_('Known causes'), blank = True, null = True)
    treatment = models.TextField(_('Treatment'), blank = True, null = True) 
    duration = models.CharField(_('Normal duration'), max_length = 50, blank = True, null = True)
    
    def __unicode__(self):
        return '%s' % self.name
    
    class Meta():
        verbose_name= _('Disease')
        verbose_name_plural = _('Diseases')
        translate = ('name', 'causes', 'treatment', 'duration')


class Vaccine(models.Model):
    __metaclass__ = TransMeta
    name = models.CharField(_('Name'), max_length = 150, blank = False, null = False)
    disease = models.ManyToManyField('sanidade.Disease', verbose_name = _('Disease list'), 
                                     blank = False, null = False)

    def __unicode__(self):
        return '%s' % self.name
    
    class Meta():
        verbose_name=_('Vaccine')
        verbose_name_plural = _('Vaccine')
        translate = ('name',)


class Medicine(models.Model):
    __metaclass__ = TransMeta
    name = models.CharField(_('Name'), max_length = 150, blank = False, null = False)
    description = models.CharField(_('Description'), max_length = 150, blank = True, null = True)
    withdrawal = models.PositiveIntegerField(_('Safety interval'),
                                                      help_text = _('Safety interval in days'), 
                                                      blank = False, null = False)
    species_choices = (('Bird', _('Bird')),
                       ('Bovine', _('Bovine')),
                       ('Caprine', _('Caprine')),
                       ('Equine', _('Equine')),
                       ('Leporine', _('Leporine')),
                       ('Ovine', _('Ovine')), 
                       ('Swine', _('Swine')))

    breed = models.ManyToManyField('animal.SpecieBreed')
    obs = models.TextField(_('Observations'), blank=True, null=True)

    def __unicode__(self):
        return '%s' % (self.name)

    class Meta():
        verbose_name=_('Medicine')
        verbose_name_plural=_('Medicines')
        translate = ('name', 'description', 'obs')
