from sanidade.models import InterventionEntry, Intervention, IndexMeasurements, IndexMeasurementsValues, Vaccination, Vaccine, MedicalCare, Disease, Medicine, Action
from django.contrib import admin

admin.site.register(InterventionEntry)
admin.site.register(Intervention)
admin.site.register(IndexMeasurements)
admin.site.register(IndexMeasurementsValues)

admin.site.register(Vaccination)
admin.site.register(MedicalCare)

admin.site.register(Vaccine)
admin.site.register(Disease)
admin.site.register(Medicine)