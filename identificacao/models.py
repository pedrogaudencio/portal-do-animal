# -*- coding: UTF-8 -*-
from django.db import models
from django.utils.translation import ugettext as _
#from portaldoanimal.utils.checkdigit import Checkdigit
from transmeta import TransMeta
from portaldoanimal import settings
from django.utils.encoding import force_unicode
from django.db.utils import IntegrityError
from django.core.exceptions import ObjectDoesNotExist


class Identifier(models.Model):
    number = models.CharField(_('Number'), max_length=30, blank = False, null = False)
    id_type = models.ForeignKey('identificacao.IdentifierType', verbose_name = _('Identifier type'))
    current_breeder = models.ForeignKey('entidade.Breeder', 
                                        verbose_name = _('Current breeder'), blank = True, null = True)
    inactivation_date = models.DateField(verbose_name = _('Inactivation date'), blank = True, null = True)
    active = models.BooleanField(verbose_name = _('Active'), default = True)


    def __unicode__(self):
        return '%s #%s' % (self.number, self.id_type)


    class Meta():
        unique_together = ('number', 'id_type')
        verbose_name = _('Identifier')
        verbose_name_plural= _('Identifiers')

    
    def clean(self):
        from django.core.exceptions import ValidationError
        
        if not self.id_type.id:
            raise ValidationError(_('Not a good id_type'))
        #TODO: checkdigit
        pass


    def save(self, *args, **kwargs):
        """
        Add a identifier to IdReserved 
        """
        if not self.id:
            #create(cls, number, id_type, entitie, obs, active = False, inactivation_date = None)
            try:
                IdReserved.create(self.number, self.id_type, None, None, 
                              self.active,  self.inactivation_date or None)
            except IntegrityError:
                pass
        super(Identifier, self).save(*args, **kwargs)        
        #TODO: aqui já se pode colocar apartir do idMeta


class IdReserved(models.Model):
    """
    All used, inactive and reserved Identifiers
    """
    number = models.CharField(_('Number'), max_length = 30, blank = False, null = False)
    id_type = models.ForeignKey('identificacao.IdentifierType', verbose_name = _('Identifier type'))
    entitie = models.ForeignKey('entidade.Entities', blank = True, null = True)
    
    active = models.BooleanField(_('Active'), 
                                 help_text = _('If true the number identifier is been used'), 
                                 default = False)
    inactivation_date = models.DateField(verbose_name = _('Inactivation date'), 
                                         help_text = _('Identifier is no longer active'), 
                                         null = True, blank = True)
    obs = models.TextField(_('Observations'), null = True, blank = True)
    
    
    def __unicode__(self):
        return '%s' % self.number
    
    
    class Meta:
        unique_together = ('number', 'id_type') 
        verbose_name = _('Identifier Reservation')
        verbose_name_plural = _('Identifiers Reservation')
    
    
    @classmethod
    def create(cls, number, id_type, entitie, obs, active = False, inactivation_date = None):
        """
        Create a new identifier reservation
        """        
        identifier = cls(number = number, id_type = id_type, entitie = entitie, active = active,
                         inactivation_date = inactivation_date, obs = obs)
        identifier.save()

    
    def current_state(self):
        """
        Get the details of a given identifier
        """
        try:
            return Identifier.objects.get(number = self.number, id_type = self.id_type )
        except ObjectDoesNotExist:
            return None
        
        
class IdMeta(models.Model):
    """
    Information of who and when (entities) is the identifier owner
    """
    identifier = models.ForeignKey(Identifier)
    entitie = models.ForeignKey('entidade.Entities')
    date = models.DateTimeField(_('Date of buy/production'), auto_now_add=True)
    documents = models.ManyToManyField('documents.Document', blank = True, null = True)
    
    
    def __unicode__(self):
        return "%s - %s" % (self.identifier, force_unicode(self.entitie))
    
    
    class Meta():
        unique_together = ('identifier', 'entitie', 'date')
        ordering = ['-date']
        verbose_name = _('Identifier owners information')
        verbose_name_plural = _('Identifiers owners information')


class IdentifierType(models.Model):
    __metaclass__ = TransMeta
    name = models.CharField(_('Name'), max_length=30, blank=False, null = False)
    code = models.IntegerField(_('Code'), blank = False, null = False)
    obs = models.TextField(_('Observations'), blank=True, null=True)


    def clean(self):
        from django.core.exceptions import ValidationError, ObjectDoesNotExist, MultipleObjectsReturned
        from django.db.models import Q
        from portaldoanimal import error
        from portaldoanimal.error import IdentificationError as ERRO  
        
        for lang, dont_give_a_fuck in settings.LANGUAGES:
            field_name = 'name_%s' % lang
            field_code = 'code_%s' % lang
            try:
                if IdentifierType.objects.get(~Q(id = self.id), **{field_name: self.name}):
                    raise ValidationError(_('Already exists one IndentifierType with this name'))
            except MultipleObjectsReturned:
                raise ValidationError(error.ERROR['1001'])
            except ObjectDoesNotExist:
                pass
            
            try:
                if IdentifierType.objects.get(~Q(id = self.id), **{field_code: self.code}):
                    raise ValidationError(_('Already exists one IndentifierType with this code'))
            except MultipleObjectsReturned:
                raise ValidationError(ERRO().except_1002())
            except ObjectDoesNotExist:
                pass
                   

    def __unicode__(self):
        return '%s - %s' % (force_unicode(self.name), self.code)


    class Meta():
        verbose_name = _('Identifier type')
        verbose_name_plural= _('Identifiers type')
        translate = ('name', 'code', 'obs')

        
class IdentificationISO(models.Model):
    country = models.CharField(_('Country code'), max_length = 5, blank = False, null = False, unique = True)
    rfid_code = models.CharField(_('RFID Code'), max_length = 15, blank = False, null = False, unique = True)
    
    
    def __unicode__(self):
        return '%s - %s' % (self.country, self.rfid_code)
    
    class Meta():
        verbose_name = _('Identification ISO code')
        verbose_name_plural = _('Identification ISO codes')
