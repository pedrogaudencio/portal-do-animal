# -*- coding: UTF-8 -*-
from django.conf.urls.defaults import *
from django.views.generic import DetailView, ListView
from identificacao.models import Identifier
from django.views.generic.base import TemplateView

urlpatterns = patterns('',
    (r'^$',
        ListView.as_view(
            queryset=Identifier.objects.order_by('id')[:10],
            context_object_name='latest_identificacao_list',
            template_name='identificador/index.html')),

    (r'^(?P<pk>\d+)/$',
        'identificacao.views.listar'),
    #(r'^adicionar/$',
    #    'identificacao.views.adicionar'),
    
    (r'^$',
        TemplateView.as_view(
            template_name='identificacao/done.html')),

    url(r'^reserve/(?P<slug>[-\w\d]+)/$', 'identificacao.views.search_identifier', 
                                                name = 'search_identifier'), 
                       
    url(r'^make_reserve/(?P<slug>[-\w\d]+)/$', 'identificacao.views.reserve_identifier', 
                                                name = 'reserve_identifier'),
                       
    url(r'^identifier_list/(?P<slug>[-\w\d]+)/$', 'identificacao.views.list_reserved_list', 
                                                name = 'identifier_list'), 

)