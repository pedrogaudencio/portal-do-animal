from django.shortcuts import get_object_or_404, render_to_response
from identificacao.models import Identifier, IdReserved
from django.http import HttpResponseRedirect, Http404
from identificacao.forms import AdicionarIdentificadorIndividualmente, AdicionarIdentificadorSequencialmente,\
    IdRervedForm, ReservIdForm
from django.views.decorators.csrf import csrf_protect
from django.template.context import RequestContext
from django.contrib.auth.decorators import login_required
from exploracao.models import Farm
from django.core.exceptions import ObjectDoesNotExist
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse
from django.contrib import messages


@csrf_protect
def listar(request, pk):
    obj = get_object_or_404(Identifier, id=pk)

    return render_to_response('identificacao/listar.html', {'identificacao': obj},
                              context_instance=RequestContext(request))


@csrf_protect
def adicionar_identificador_individualmente(request):
    if request.method == 'POST':
        form = AdicionarIdentificadorIndividualmente(request.POST)

        if form.is_valid():
            print "all validation passed"
            form.save()

            return HttpResponseRedirect('../')
    else:
        print "validation failed"
        form = AdicionarIdentificadorIndividualmente()

    return render_to_response('animal/adicionar.html', {'form': form},
                                context_instance=RequestContext(request))


@csrf_protect
def adicionar_identificador_sequencialmente(request):
    if request.method == 'POST':
        form = AdicionarIdentificadorSequencialmente(request.POST)

        if form.is_valid():
            print "all validation passed"
            form.save()

            return HttpResponseRedirect('../')
    else:
        print "validation failed"
        form = AdicionarIdentificadorSequencialmente()

    return render_to_response('animal/adicionar.html', {'form': form},
                                context_instance=RequestContext(request))


@login_required
def search_identifier(request, slug = None):
    """
    Allow user to search a set of identifiers
    """
    try:
        farm = Farm.objects.get(slug = slug)
    except ObjectDoesNotExist:
        raise Http404
    
    if request.method == 'POST':
        form = IdRervedForm(farm, request.POST)
        if form.is_valid():
            pass
    else:
        print 'else'
        form = IdRervedForm(farm)
    
    context = {'form': form,
               'farm': farm,
               }
    
    return render_to_response('identificacao/reserve_identifier.html', context,
                                context_instance=RequestContext(request))


@login_required
def reserve_identifier(request, slug = None):
    """
    Allow user to reserve a set of identifiers
    """
    #TODO: qq entidade pode reserver
    try:
        farm = Farm.objects.get(pk = slug)
    except ObjectDoesNotExist:
        raise Http404
    
    try:
        identifiers_list = request.session['identifiers_list']
        id_type = request.session['id_type']
    except KeyError:
        raise Http404 
    
    if request.method == 'POST':
        form = ReservIdForm(farm.breeder, identifiers_list, id_type, request.POST)
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.SUCCESS, 
                                     _('You reserve the identifiers'))
            #Remove from session
            try:
                del request.session['identifiers_list']
                del request.session['id_type']
            except:
                pass
            
            return HttpResponseRedirect(reverse('identifier_list', args = (farm.slug,)))
    else:
        #Remove from session
        try:
            del request.session['identifiers_list']
            del request.session['id_type']
        except:
            pass
            
        messages.add_message(request, messages.ERROR, 
                                     _('Fail to reserve the identifiers'))
        return HttpResponseRedirect(reverse('search_identifier'))


def list_reserved_list(request, slug = None):
    """
    Return the list of all reserved identifiers of a given entitie
    """
    try:
        farm = Farm.objects.get(slug = slug)
    except ObjectDoesNotExist:
        raise Http404
    
    id_list = IdReserved.objects.filter(entitie = farm.breeder)
    
    context = {'farm': farm,
               'id_reserved_list': id_list,
               }
    return render_to_response('identificacao/identifiers_list.html', context,
                                context_instance=RequestContext(request)) 
    