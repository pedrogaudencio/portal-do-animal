# -*- coding: UTF-8 -*-
from identificacao.models import Identifier, IdentifierType, IdReserved, IdentifierType
from django import forms
from django.forms.models import ModelForm
from django.forms.widgets import CheckboxSelectMultiple, Select
from entidade.models import Entities
from django.utils.translation import ugettext_lazy as _
from django.db.utils import IntegrityError
from utils.sia import set_checkdigit


class AdicionarIdentificadorIndividualmente(ModelForm):
    class Meta:
        model=Identifier


class AdicionarIdentificadorSequencialmente(forms.Form):
    codigo_do_primeiro=forms.IntegerField(label=('Código do primeiro'), required=True)
    tipo = forms.CharField(widget=forms.Select(choices=IdentifierType.objects.all()), required=True)
    quantidade=forms.IntegerField(label=('Quantidade a inserir'), required=True)
    entidade = forms.CharField(widget=forms.Select(choices=Entities.objects.all()), required=True)
    
    
class IdRervedForm(forms.Form):
    """
    Form to ask the reserve one or a set of Identifiers
    """
    def __init__(self, farm, *args, **kwargs):
        super(IdRervedForm, self).__init__(*args, **kwargs)
        self.farm = farm
        
        self.fields['id_type'] = forms.ModelChoiceField(IdentifierType.objects.all())
        self.fields['from_sia'] = forms.CharField(label = _('First number'))
        self.fields['quantity'] = forms.IntegerField(label = _('Quantity'), required = False)
        

    #TODO: Clean do tamanho do numero dado dependendo do tipo de identificador
    #TODO: confirmar tamanhos
    def clean_from_sia(self):
        """
        Verify length of number
            code:
                01 - MA-convencional -> 9 ou 8 
                02 - MA-electectrónica -> 9 ou 8
                03 - Subcutâneo IDE -> 15 ou 16
                04 - Reticular IDE -> 15 ou 16
        """
        from_sia = self.cleaned_data['from_sia']
        id_type = self.cleaned_data['id_type']
        
        if  id_type.code == 1 and len(from_sia) not in [10, 11]:
            raise forms.ValidationError(_('Invalid number. Number must have the format PTXXXXXXXXX'))
        if  id_type.code == 2 and len(from_sia) not in [10, 11]:
            raise forms.ValidationError(_('Invalid number. Number must have the format PTXXXXXXXXX'))
        if  id_type.code == 3 and len(from_sia) not in [15, 16]:
            raise forms.ValidationError(_('Invalid number. Number must have length 15 or 16'))
        if  id_type.code == 4 and len(from_sia) not in [15, 16]:
            raise forms.ValidationError(_('Invalid number. Number must have length 15 or 16')) 
        
        return from_sia
         
    
    def make_list(self):
        from_sia = self.cleaned_data['from_sia']
        id_type = self.cleaned_data['id_type']
        wanted_numbers = []
        
        try:
            quantity = self.cleaned_data['quantity']
        except KeyError:
            quantity = None
        
        #TODO: para electrónica vale a pena reservar?
        from_sia_number = int(from_sia[3:])
        for x in range(quantity):
            result = str(from_sia_number + x)
                            
            while len(result) < 8:
                result = '0'+result
            
            

            wanted_numbers.append((set_checkdigit(str(from_sia[:3] + result)),
                                  IdReserved.objects.filter(number = (from_sia[:3] + result), 
                                                            id_type = id_type).exists()
                                  ))
        number_unavailable = []
        for number, val in wanted_numbers:
            if val:
                number_unavailable.append(IdReserved.objects.get(number = number, 
                                                            id_type = id_type))

        return (wanted_numbers, number_unavailable, (len(wanted_numbers) - len(number_unavailable)) or 0) 
        
        
class ReservIdForm(forms.Form):
    """
    Form to reserve one or a set of Identifiers
    """
    def __init__(self, entitie, identifier_list, id_type = None, *args, **kwargs):
        self.entitie = entitie
        self.identifier_list = identifier_list
        self.id_type = id_type
        super(ReservIdForm, self).__init__(*args, **kwargs)
        
        id_list = []
        for number, val in self.identifier_list:
            if not val: #if value is false(doesn't exists on database)
                id_list.append((number, number))
        
        self.fields['id_numbers'] = forms.MultipleChoiceField(id_list, widget=forms.CheckboxSelectMultiple(), 
                                                              required = False)

    def save(self):
        numbers = self.cleaned_data['id_numbers']
        if self.id_type:
            for number in numbers:
                identifier = IdReserved(number = number, id_type = self.id_type, entitie = self.entitie)
                try:
                    identifier.save()
                except IntegrityError:
                    pass