# -*- coding: UTF-8 -*-
from dajax.core import Dajax
from dajaxice.decorators import dajaxice_register
from django.template.loader import render_to_string
from dajaxice.utils import deserialize_form
from identificacao.forms import IdRervedForm, ReservIdForm
from entidade.models import Entities
from django.template.context import RequestContext
from django.shortcuts import render_to_response
from exploracao.models import Farm


@dajaxice_register
def is_identifiers_available(request, entitie_pk, form):
    dajax = Dajax()
    
    entitie = Entities.objects.get(pk = entitie_pk)
    
    form = IdRervedForm(None, deserialize_form(form))
    #TODO: return erros de form
    if form.is_valid():
        identifiers_list, number_unavailable, number_available = form.make_list()
        #set session with identifiers_list and id_type
        request.session['identifiers_list'] = identifiers_list
        request.session['id_type'] = form.cleaned_data['id_type']
        identifiers_form = ReservIdForm(entitie, identifiers_list)
        #show identifier list
        render = render_to_string('identificacao/identifiers_reserve_list.html', 
                              locals(), context_instance=RequestContext(request))
        dajax.assign('#identifiers_list', 'innerHTML', render)
        
        #refresh form, in case of previously errors
        form_render = render_to_string('identificacao/identifier_form.html', locals(), 
                                  context_instance=RequestContext(request))
        dajax.assign('.identifier_form', 'innerHTML', form_render)
        
        
    else:
        render = render_to_string('identificacao/identifier_form.html', locals(), 
                                  context_instance=RequestContext(request))
        
        dajax.assign('.identifier_form', 'innerHTML', render)
                
    return dajax.json()


@dajaxice_register
def reserve_identifiers(request, entitie_pk, form):
    dajax = Dajax()
    print form
    entitie = Entities.objects.get(pk = entitie_pk)
    
    form = ReservIdForm(entitie, deserialize_form(form))
    #TODO: return erros de form
    if form.is_valid():
        print 'is_valid'
        form.save()
            
    render = render_to_string('identificacao/identifiers_reserve_list.html', 
                              locals(), context_instance=RequestContext(request))
    dajax.assign('#identifiers_list', 'innerHTML', render)
    
    return dajax.json()