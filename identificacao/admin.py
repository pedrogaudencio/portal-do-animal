# -*- coding: UTF-8 -*-
from django.contrib import admin
from identificacao.models import Identifier, IdentifierType, IdentificationISO, IdMeta,\
    IdReserved
from animal.models import Animal
from utilizador.models import Staff
from entidade.models import Entities


class IdentifierAdmin(admin.ModelAdmin):
    """
    fieldsets = [
                 (None, {
                         'classes': ['wide', 'extrapretty'],
                         'fields': (
                                    ('number','id_type',),
                                    'entity',
                                    )
                         }),
    ]
    """
    list_display = ('number','id_type','current_breeder')
    list_filter = ['id_type']
    search_fields = ['number']    


class IdMetaAdmin(admin.ModelAdmin):
    filter_horizontal = ['documents'] 


class IdReservedAdmin(admin.ModelAdmin):
    search_fields = ['number']
    list_filter = ['id_type', 'entitie']
    

admin.site.register(Identifier, IdentifierAdmin)
admin.site.register(IdentifierType)
admin.site.register(IdentificationISO)
admin.site.register(IdMeta, IdMetaAdmin)
admin.site.register(IdReserved, IdReservedAdmin)
