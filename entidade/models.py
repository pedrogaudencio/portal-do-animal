# -*- coding: UTF-8 -*-
from django.contrib.auth.models import User
from django.db import models
from utils.nif import Nif
from django.utils.translation import ugettext as _
from animal.models import Animal
from django.utils.encoding import force_unicode
from exploracao.models import Farm


class Entities(models.Model):
    name = models.CharField(_('Name'), max_length = 150, blank = False, null = False) 
    contact = models.ManyToManyField('contactos.Contact', blank = True, null = True) # colocar false?
    nif = models.IntegerField(_('NIF'), blank = True, null = True) # colcoar a FALSE
    nib = models.CharField(_('NIB'), max_length = 50, blank = True, null = True)
    obs = models.TextField(_('Observations'), blank = True, null = True)
    active = models.BooleanField(_('Active'), default = True)


    class Meta:
        verbose_name = _('Entities')
        verbose_name_plural = _('Entities')
        

    def __unicode__(self):
        return '%s' % self.name
 

class Breeder(Entities):
    ifap = models.CharField(_('IFAP'), max_length = 50, blank = True, null = True)
    
    class Meta:
        verbose_name = _('Breeder')
        verbose_name_plural = _('Breeders')

    
    def get_animals_species(self):
        """
        Get list of animal species
        """
        species_list = []
        
        animals_list = Animal.objects.filter(farm__breeder = self)
        
        for animal in animals_list:
            if animal.specie_breed.specie not in species_list:
                species_list.append(animal.specie_breed.specie)
    
        return species_list

    
    def get_not_associated_farms(self):
        """
        Get farms there aren't managed by an association  
        """
        return Farm.not_managed.filter(breeder = self)
        
        
class BreederMembership(models.Model):
    breeder = models.ForeignKey(Breeder)
    member_number = models.CharField(_('Member number'), max_length = 50, blank = True, null = True) #colocar false?
    date_entry = models.DateField(_('Entry date'), blank = True, null = True) #colocar false?
    date_leave = models.DateField(_('Leave date'), blank = True, null = True) #colocar false?
    is_active = models.BooleanField(_('Is active'), default = False)
    reason_to_leave = models.TextField(_('Reasons to leave'), max_length = 250, blank = True, null = True)
    association = models.ForeignKey('Association')

    class Meta:
        verbose_name = _('Breeder membership')
        verbose_name_plural = _('Breeders memberships')   


    def __unicode__(self):
        return 'Membership #%s - (%s)' % (self.member_number, self.association)


class Association(Entities):
    tec = models.CharField(_('Technician in charge'), max_length = 50, blank = True, null = True) #colcar false?
    breed = models.ManyToManyField('animal.SpecieBreed', help_text = _('Breeds'), blank = False)
    members = models.ManyToManyField('BreederMembership', related_name='association_members', blank = True)


    def __unicode__(self):
        return '%s' % self.name


    class Meta:
        verbose_name = _('Association')
        verbose_name_plural = _('Associations')


class Veterinarian(Entities):
    vet_license = models.CharField(_('License'), max_length = 50, blank = True, null = True)

    class Meta:
        verbose_name = _('Veterinarian')
        verbose_name_plural = _('Veterinarians')


class Dra(Entities):
    acronym = models.CharField(_('Acronym'), max_length=10, blank = False, null = False)
    cod = models.PositiveIntegerField(_('Cod'), max_length=10, blank = True, null = True)

    def __unicode__(self):
        return '%s' % (self.acronym)

    class Meta():
        verbose_name= _('DRA')
        verbose_name_plural= _('DRA')


class Slaughterhouse(Entities):
    license = models.CharField(_('License'), max_length = 50, blank = True, null = True)

    class Meta:
        verbose_name = _('Slaughterhouse')
        verbose_name_plural = _('Slaughterhouses')


class DeliveryChain(Entities):
    license = models.CharField(_('License'), max_length = 50, blank = True, null = True)

    class Meta:
        verbose_name = _('Delivery Chain')
        verbose_name_plural = _('Delivery Chains')

