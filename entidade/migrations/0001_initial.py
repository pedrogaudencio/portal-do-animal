# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Entities'
        db.create_table('entidade_entities', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=150)),
            ('nif', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('nib', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('obs', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal('entidade', ['Entities'])

        # Adding M2M table for field contact on 'Entities'
        db.create_table('entidade_entities_contact', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('entities', models.ForeignKey(orm['entidade.entities'], null=False)),
            ('contact', models.ForeignKey(orm['contactos.contact'], null=False))
        ))
        db.create_unique('entidade_entities_contact', ['entities_id', 'contact_id'])

        # Adding model 'Breeder'
        db.create_table('entidade_breeder', (
            ('entities_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['entidade.Entities'], unique=True, primary_key=True)),
            ('ifap', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
        ))
        db.send_create_signal('entidade', ['Breeder'])

        # Adding model 'BreederMember'
        db.create_table('entidade_breedermember', (
            ('entities_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['entidade.Entities'], unique=True, primary_key=True)),
            ('ifap', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('member_number', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('date_entry', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('date_leave', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('reason_to_leave', self.gf('django.db.models.fields.TextField')(max_length=250, null=True, blank=True)),
        ))
        db.send_create_signal('entidade', ['BreederMember'])

        # Adding model 'Association'
        db.create_table('entidade_association', (
            ('entities_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['entidade.Entities'], unique=True, primary_key=True)),
            ('tec', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
        ))
        db.send_create_signal('entidade', ['Association'])

        # Adding M2M table for field breed on 'Association'
        db.create_table('entidade_association_breed', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('association', models.ForeignKey(orm['entidade.association'], null=False)),
            ('speciebreed', models.ForeignKey(orm['animal.speciebreed'], null=False))
        ))
        db.create_unique('entidade_association_breed', ['association_id', 'speciebreed_id'])

        # Adding model 'Veterinarian'
        db.create_table('entidade_veterinarian', (
            ('entities_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['entidade.Entities'], unique=True, primary_key=True)),
            ('vet_license', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
        ))
        db.send_create_signal('entidade', ['Veterinarian'])

        # Adding model 'Dra'
        db.create_table('entidade_dra', (
            ('entities_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['entidade.Entities'], unique=True, primary_key=True)),
            ('acronym', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('cod', self.gf('django.db.models.fields.PositiveIntegerField')(max_length=10, null=True, blank=True)),
        ))
        db.send_create_signal('entidade', ['Dra'])

    def backwards(self, orm):
        # Deleting model 'Entities'
        db.delete_table('entidade_entities')

        # Removing M2M table for field contact on 'Entities'
        db.delete_table('entidade_entities_contact')

        # Deleting model 'Breeder'
        db.delete_table('entidade_breeder')

        # Deleting model 'BreederMember'
        db.delete_table('entidade_breedermember')

        # Deleting model 'Association'
        db.delete_table('entidade_association')

        # Removing M2M table for field breed on 'Association'
        db.delete_table('entidade_association_breed')

        # Deleting model 'Veterinarian'
        db.delete_table('entidade_veterinarian')

        # Deleting model 'Dra'
        db.delete_table('entidade_dra')

    models = {
        'animal.speciebreed': {
            'Meta': {'object_name': 'SpecieBreed'},
            'breed_en': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'breed_es': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'breed_fr': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'breed_pt': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'cod_en': ('django.db.models.fields.CharField', [], {'max_length': '4', 'null': 'True', 'blank': 'True'}),
            'cod_es': ('django.db.models.fields.CharField', [], {'max_length': '4', 'null': 'True', 'blank': 'True'}),
            'cod_fr': ('django.db.models.fields.CharField', [], {'max_length': '4', 'null': 'True', 'blank': 'True'}),
            'cod_pt': ('django.db.models.fields.CharField', [], {'max_length': '4', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'specie': ('django.db.models.fields.CharField', [], {'max_length': '15'})
        },
        'contactos.concelho': {
            'Meta': {'object_name': 'Concelho'},
            'concelho': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'contactos.contact': {
            'Meta': {'object_name': 'Contact'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'cellphone': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'company_number': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'company_number_extension': ('django.db.models.fields.CharField', [], {'max_length': '5', 'null': 'True', 'blank': 'True'}),
            'concelho': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contactos.Concelho']"}),
            'district': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contactos.District']"}),
            'fax': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'freguesia': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contactos.Freguesia']"}),
            'home_number': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'zip_code': ('django.db.models.fields.CharField', [], {'max_length': '10'})
        },
        'contactos.district': {
            'Meta': {'object_name': 'District'},
            'district': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'contactos.freguesia': {
            'Meta': {'object_name': 'Freguesia'},
            'freguesia': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'entidade.association': {
            'Meta': {'object_name': 'Association', '_ormbases': ['entidade.Entities']},
            'breed': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['animal.SpecieBreed']", 'symmetrical': 'False'}),
            'entities_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['entidade.Entities']", 'unique': 'True', 'primary_key': 'True'}),
            'tec': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'})
        },
        'entidade.breeder': {
            'Meta': {'object_name': 'Breeder', '_ormbases': ['entidade.Entities']},
            'entities_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['entidade.Entities']", 'unique': 'True', 'primary_key': 'True'}),
            'ifap': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'})
        },
        'entidade.breedermember': {
            'Meta': {'object_name': 'BreederMember', '_ormbases': ['entidade.Entities']},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'date_entry': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'date_leave': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'entities_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['entidade.Entities']", 'unique': 'True', 'primary_key': 'True'}),
            'ifap': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'member_number': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'reason_to_leave': ('django.db.models.fields.TextField', [], {'max_length': '250', 'null': 'True', 'blank': 'True'})
        },
        'entidade.dra': {
            'Meta': {'object_name': 'Dra', '_ormbases': ['entidade.Entities']},
            'acronym': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'cod': ('django.db.models.fields.PositiveIntegerField', [], {'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'entities_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['entidade.Entities']", 'unique': 'True', 'primary_key': 'True'})
        },
        'entidade.entities': {
            'Meta': {'object_name': 'Entities'},
            'contact': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['contactos.Contact']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'nib': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'nif': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'obs': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'})
        },
        'entidade.veterinarian': {
            'Meta': {'object_name': 'Veterinarian', '_ormbases': ['entidade.Entities']},
            'entities_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['entidade.Entities']", 'unique': 'True', 'primary_key': 'True'}),
            'vet_license': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['entidade']