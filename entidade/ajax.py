# -*- coding: UTF-8 -*-
from dajax.core import Dajax
from dajaxice.decorators import dajaxice_register
from exploracao.models import Farm
from django.template.context import RequestContext
from django.shortcuts import get_object_or_404, render_to_response, render
from django.template.loader import render_to_string
from entidade.forms import BreederMembershipReplyForm
from entidade.models import Breeder, Association, BreederMembership
from django.utils.translation import ugettext as _ 


@dajaxice_register
def new_membership(request, association_id = None, breeder_id = None):
    dajax = Dajax()
    members_numbers = ''
    
    #TODO: Exception and get not filter
    #Get all memberships
    all_requests = BreederMembership.objects.filter(association__id = association_id)
    #Get applications for new memberships 
    application = all_requests.filter(breeder__id = breeder_id, member_number = None)
    #Get memberships numbers
    members = all_requests.exclude(member_number__isnull = True) #TODO: test this
    for member in members:
        members_numbers += str(member.member_number) + ',' 

    
    form = BreederMembershipReplyForm(initial = {'breeder': Breeder.objects.get(id=breeder_id)})
    
    render = render_to_string('entidade/add_breeder_membership.html', locals(),
                               context_instance = RequestContext(request))

    dajax.assign('#new_membership-' + breeder_id, 'innerHTML', render)
    
    return dajax.json()
