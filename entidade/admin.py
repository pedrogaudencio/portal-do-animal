# -*- coding: UTF-8 -*-
from django.contrib import admin
from entidade.models import Entities, Breeder, BreederMembership, Association, Veterinarian, Dra


#admin.site.register(Entidade, EntidadeAdmin)
admin.site.register(Entities)
admin.site.register(Breeder)
admin.site.register(BreederMembership)
admin.site.register(Association)
admin.site.register(Veterinarian)
admin.site.register(Dra)