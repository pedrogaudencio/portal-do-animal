# -*- coding: UTF-8 -*-
from django.contrib.auth.decorators import login_required
from django.template.context import RequestContext
from django.shortcuts import render_to_response, get_object_or_404
from entidade.models import Association, Breeder, BreederMembership
from entidade.forms import MembershipApplicationForm, BreederMembershipReplyForm,\
                         AddFarmsToBreederMembershipForm
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from exploracao.models import Farm


@login_required
def association_aplication(request, breeder_pk = None):
    #TODO: user permissions
    breeder = get_object_or_404(Breeder, id = breeder_pk)
    
    if request.method == 'POST':
        application_form = MembershipApplicationForm(breeder, request.POST)
        
        if application_form.is_valid():
            application_form.save()
            return HttpResponseRedirect(reverse('view_my_farms'))
    else:
        application_form = MembershipApplicationForm(breeder, initial = {'breeder': breeder})

    context = {'breeder': breeder,
               'application_form': application_form}
    
    return render_to_response('entidade/association_aplication.html', context,
                              context_instance=RequestContext(request))


@login_required
def association_reply(request, association_id = None):
    """
    Association membership reply
    """
    #Association
    association = Association.objects.get(id = association_id)

    #Context for members
    members_application_list = BreederMembership.objects.filter(association = association,
                                                                member_number__isnull = False)
    members = []
    for application in members_application_list:
        farms = Farm.not_managed.filter(breeder = application.breeder, 
                                                             applied_to = association)
        members.append({'form': AddFarmsToBreederMembershipForm(farms, association),
                        'farms': farms,
                        })
    
    #Context for not members
    not_members_application_list = BreederMembership.objects.filter(association = association, 
                                                                    member_number__isnull = True)
    not_members = []
    for application in not_members_application_list:
        not_members.append({'breeder': application.breeder,
                            'farms': Farm.not_managed.filter(breeder = application.breeder, 
                                                             applied_to = association)
                            })
    
    #On submit new membership
    if request.method == 'POST' and 'new_membership' in request.POST:
        #instance beerderMembership
        membership = BreederMembership.objects.get(association__id = association_id, 
                                      breeder__id = request.POST['breeder'])
        #form with alterations
        application_form = BreederMembershipReplyForm(request.POST, instance = membership)
        if application_form.is_valid():
            application_form.save()    
            not_members_application_list = BreederMembership.objects.filter(association = association, 
                                                                            member_number = None)
            return HttpResponseRedirect(reverse('association_reply', kwargs = {'association_id': association_id}))
    
    #On submit add farms to a membership
    if request.method == "POST" and 'add_farms' in request.POST:
        add_farm_form = AddFarmsToBreederMembershipForm(farms, association, request.POST)
        if add_farm_form.is_valid():
            add_farm_form.save()
            return HttpResponseRedirect(reverse('association_reply', kwargs = {'association_id': association_id}))
    
    #Context
    context = {'not_members': not_members,
               'members': members,
               'association': association,
               }
    
    return render_to_response('entidade/association_reply.html', context,
                              context_instance = RequestContext(request))
