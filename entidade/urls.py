# -*- coding: UTF-8 -*-
from django.conf.urls.defaults import patterns, url

"""
Entidade urls
"""
urlpatterns = patterns('entidade.views',
    #--------------Association-------------#
    url(r'^application/(?P<breeder_pk>\d+)/$', 'association_aplication'), #Breeder application
    url(r'^applications/(?P<association_id>\d+)/$', 'association_reply', name = 'association_reply'), #Association reply to breeder application
)