# -*- coding: UTF-8 -*-
from entidade.models import BreederMembership, Association
from django.forms import ModelForm, CheckboxSelectMultiple
from django import forms
from django.utils.safestring import mark_safe
from utils.widgets import DatePickerWidget
from django.forms.widgets import HiddenInput
from django.core.exceptions import ObjectDoesNotExist
from exploracao.models import Farm


class CheckboxSelectMultipleWidget(forms.CheckboxSelectMultiple):
    '''
    Renders horizontal multiple check box.
    '''
    def render(self, *args, **kwargs):
        output = super(CheckboxSelectMultipleWidget, self).render(*args,**kwargs) 
        return mark_safe(output.replace(u'<ul>', u'').replace(u'</ul>', u'').\
                         replace(u'<li>', u'').replace(u'</li>', u''))


class MembershipApplicationForm(forms.Form):
    
    def __init__(self, breeder, *args, **kwargs):
        self.breeder = breeder
        super(MembershipApplicationForm, self).__init__(*args, **kwargs)
        
        self.fields['association'] = forms.ModelChoiceField(Association.objects.all())
        self.fields['farms'] = forms.ModelMultipleChoiceField(breeder.get_not_associated_farms(),
                                                              widget=CheckboxSelectMultipleWidget())
    
    
    def save(self):
        association = self.cleaned_data['association']
        try:
            membership = BreederMembership.objects.get(association = association, 
                                                       breeder = self.breeder)
        except ObjectDoesNotExist:
            membership = BreederMembership()
            membership.breeder = self.breeder
            membership.association = association
            membership.save()
        
        for farm in self.cleaned_data['farms']:
            farm.applied_to = association
            farm.save() 


class BreederMembershipReplyForm(ModelForm):
    
    def __init__(self, *args, **kwargs):
        super(BreederMembershipReplyForm, self).__init__(*args, **kwargs)


    class Meta:
        model = BreederMembership
        fields = ('breeder', 'member_number', 'date_entry')
        widgets = {'date_entry': DatePickerWidget(),
                   'breeder': HiddenInput(),
                   'association': HiddenInput(),
                   }


class AddFarmsToBreederMembershipForm(forms.Form):
    
    def __init__(self, farms, association, *args, **kwargs):
        self.farms = farms
        self.association = association
        super(AddFarmsToBreederMembershipForm, self).__init__(*args, **kwargs)
    
        self.fields['farms'] = forms.ModelMultipleChoiceField(self.farms,
                                                              widget = CheckboxSelectMultiple())

    def save(self):
        
        for farm in self.cleaned_data['farms']:
            farm.managed_by = self.association
            farm.applied_to = None
            farm.save()
        