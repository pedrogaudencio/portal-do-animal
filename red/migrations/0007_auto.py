# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Removing M2M table for field occurrences on 'RED'
        db.delete_table('red_red_occurrences')

        # Adding M2M table for field occurrence on 'RED'
        db.create_table('red_red_occurrence', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('red', models.ForeignKey(orm['red.red'], null=False)),
            ('occurrence', models.ForeignKey(orm['red.occurrence'], null=False))
        ))
        db.create_unique('red_red_occurrence', ['red_id', 'occurrence_id'])

    def backwards(self, orm):
        # Adding M2M table for field occurrences on 'RED'
        db.create_table('red_red_occurrences', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('red', models.ForeignKey(orm['red.red'], null=False)),
            ('occurrence', models.ForeignKey(orm['red.occurrence'], null=False))
        ))
        db.create_unique('red_red_occurrences', ['red_id', 'occurrence_id'])

        # Removing M2M table for field occurrence on 'RED'
        db.delete_table('red_red_occurrence')

    models = {
        'animal.animal': {
            'Meta': {'object_name': 'Animal'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'adoptive_mother': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'adoptive_female_mother'", 'null': 'True', 'to': "orm['animal.Animal']"}),
            'birthdate': ('django.db.models.fields.DateField', [], {}),
            'farm': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['exploracao.Farm']"}),
            'father': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'male_father'", 'null': 'True', 'to': "orm['animal.Animal']"}),
            'first_identification': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'herd': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['exploracao.Herd']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'identification': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'animal_identification'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['red.AnimalIdentification']"}),
            'male_castrated': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'mother': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'female_mother'", 'null': 'True', 'to': "orm['animal.Animal']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'nifPI': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'obs': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'passport': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'pelage': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['animal.PelageType']", 'null': 'True', 'blank': 'True'}),
            'sex': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'sia': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'animal_identifier'", 'null': 'True', 'to': "orm['identificacao.Identifier']"}),
            'specie_breed': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['animal.SpecieBreed']"})
        },
        'animal.pelagetype': {
            'Meta': {'object_name': 'PelageType'},
            'color_en': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'color_es': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'color_fr': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'color_pt': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'name_es': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'name_fr': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'name_pt': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'specie_breed': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['animal.SpecieBreed']"})
        },
        'animal.speciebreed': {
            'Meta': {'object_name': 'SpecieBreed'},
            'breed_en': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'breed_es': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'breed_fr': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'breed_pt': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'code_en': ('django.db.models.fields.CharField', [], {'max_length': '4', 'null': 'True', 'blank': 'True'}),
            'code_es': ('django.db.models.fields.CharField', [], {'max_length': '4', 'null': 'True', 'blank': 'True'}),
            'code_fr': ('django.db.models.fields.CharField', [], {'max_length': '4', 'null': 'True', 'blank': 'True'}),
            'code_pt': ('django.db.models.fields.CharField', [], {'max_length': '4', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'specie': ('django.db.models.fields.CharField', [], {'max_length': '15'})
        },
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contactos.concelho': {
            'Meta': {'object_name': 'Concelho'},
            'concelho': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'contactos.contact': {
            'Meta': {'object_name': 'Contact'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'cellphone': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'company_number': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'company_number_extension': ('django.db.models.fields.CharField', [], {'max_length': '5', 'null': 'True', 'blank': 'True'}),
            'concelho': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contactos.Concelho']"}),
            'district': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contactos.District']"}),
            'fax': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'freguesia': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contactos.Freguesia']"}),
            'home_number': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'zip_code': ('django.db.models.fields.CharField', [], {'max_length': '10'})
        },
        'contactos.district': {
            'Meta': {'object_name': 'District'},
            'district': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'contactos.freguesia': {
            'Meta': {'object_name': 'Freguesia'},
            'freguesia': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'entidade.breeder': {
            'Meta': {'object_name': 'Breeder', '_ormbases': ['entidade.Entities']},
            'entities_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['entidade.Entities']", 'unique': 'True', 'primary_key': 'True'}),
            'ifap': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'})
        },
        'entidade.entities': {
            'Meta': {'object_name': 'Entities'},
            'contact': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['contactos.Contact']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'nib': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'nif': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'obs': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'})
        },
        'exploracao.farm': {
            'Meta': {'object_name': 'Farm'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'breeder': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['entidade.Breeder']"}),
            'close_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'contact': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contactos.Contact']", 'null': 'True', 'blank': 'True'}),
            'geolocation': ('django_google_maps.fields.GeoLocationField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'health_status': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['exploracao.HealthStatus']", 'null': 'True', 'blank': 'True'}),
            'herds': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'farm_herd'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['exploracao.Herd']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'manager': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'map_address': ('django_google_maps.fields.AddressField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'obs': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'production': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['exploracao.Production']", 'symmetrical': 'False', 'blank': 'True'}),
            'register_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'register_number': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '150'}),
            'system': ('django.db.models.fields.CharField', [], {'default': "'extensive'", 'max_length': '20'}),
            'trademark': ('django.db.models.fields.CharField', [], {'max_length': '150', 'unique': 'True', 'null': 'True', 'blank': 'True'})
        },
        'exploracao.healthstatus': {
            'Meta': {'object_name': 'HealthStatus'},
            'acronym_en': ('django.db.models.fields.CharField', [], {'max_length': '15', 'null': 'True', 'blank': 'True'}),
            'acronym_es': ('django.db.models.fields.CharField', [], {'max_length': '15', 'null': 'True', 'blank': 'True'}),
            'acronym_fr': ('django.db.models.fields.CharField', [], {'max_length': '15', 'null': 'True', 'blank': 'True'}),
            'acronym_pt': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'animal': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['animal.SpecieBreed']", 'symmetrical': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'law_en': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'law_es': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'law_fr': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'law_pt': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'start_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'status_en': ('django.db.models.fields.CharField', [], {'max_length': '250', 'null': 'True', 'blank': 'True'}),
            'status_es': ('django.db.models.fields.CharField', [], {'max_length': '250', 'null': 'True', 'blank': 'True'}),
            'status_fr': ('django.db.models.fields.CharField', [], {'max_length': '250', 'null': 'True', 'blank': 'True'}),
            'status_pt': ('django.db.models.fields.CharField', [], {'max_length': '250'})
        },
        'exploracao.herd': {
            'Meta': {'object_name': 'Herd'},
            'farm': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'herd_farm'", 'to': "orm['exploracao.Farm']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'obs': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'})
        },
        'exploracao.production': {
            'Meta': {'object_name': 'Production'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'name_es': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'name_fr': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'name_pt': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        },
        'identificacao.identifier': {
            'Meta': {'object_name': 'Identifier'},
            'entity': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'identifier_entity'", 'null': 'True', 'to': "orm['entidade.Entities']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'id_type': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'type_dentifier'", 'to': "orm['identificacao.IdentifierType']"}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'number': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'identificacao.identifiertype': {
            'Meta': {'object_name': 'IdentifierType'},
            'code': ('django.db.models.fields.IntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'obs': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'})
        },
        'red.animalidentification': {
            'Meta': {'ordering': "['-timestamp']", 'object_name': 'AnimalIdentification', '_ormbases': ['red.Event']},
            'animal': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'animal_id'", 'to': "orm['animal.Animal']"}),
            'employee': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'colaborador_id'", 'null': 'True', 'to': "orm['utilizador.Staff']"}),
            'entity_nif': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'event_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['red.Event']", 'unique': 'True', 'primary_key': 'True'}),
            'identifier': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'identifier_animal'", 'to': "orm['identificacao.Identifier']"})
        },
        'red.event': {
            'Meta': {'ordering': "['-timestamp']", 'object_name': 'Event'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {})
        },
        'red.movement': {
            'Meta': {'ordering': "['-timestamp']", 'object_name': 'Movement', '_ormbases': ['red.Event']},
            'animals': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['animal.Animal']", 'symmetrical': 'False'}),
            'entity': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['entidade.Entities']"}),
            'event_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['red.Event']", 'unique': 'True', 'primary_key': 'True'}),
            'farm': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['exploracao.Farm']", 'null': 'True', 'blank': 'True'}),
            'localization': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'mov_type': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'nr_guia_circ': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'obs': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'})
        },
        'red.occurrence': {
            'Meta': {'ordering': "['-timestamp']", 'object_name': 'Occurrence', '_ormbases': ['red.Event']},
            'animals': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['animal.Animal']", 'symmetrical': 'False'}),
            'event_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['red.Event']", 'unique': 'True', 'primary_key': 'True'}),
            'farm': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['exploracao.Farm']"}),
            'guide': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'guide_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'obs': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'oc_type': ('django.db.models.fields.CharField', [], {'max_length': '15'})
        },
        'red.red': {
            'Meta': {'ordering': "['year']", 'unique_together': "(('year', 'farm'),)", 'object_name': 'RED'},
            'authorization_number': ('django.db.models.fields.PositiveIntegerField', [], {'max_length': '20'}),
            'book_appendix': ('django.db.models.fields.PositiveIntegerField', [], {'max_length': '20'}),
            'book_number': ('django.db.models.fields.PositiveIntegerField', [], {'max_length': '20'}),
            'entity': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['entidade.Entities']"}),
            'farm': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['exploracao.Farm']"}),
            'herd_count': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0', 'max_length': '20'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'identifications': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['red.AnimalIdentification']", 'null': 'True', 'blank': 'True'}),
            'issuer': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'last_update_timestamp': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'medical_interventions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['sanidade.InterventionEntry']", 'null': 'True', 'blank': 'True'}),
            'movimentations': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['red.Movement']", 'null': 'True', 'blank': 'True'}),
            'notes': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'obs': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'occurrence': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['red.Occurrence']", 'null': 'True', 'blank': 'True'}),
            'year': ('django.db.models.fields.IntegerField', [], {})
        },
        'sanidade.action': {
            'Meta': {'object_name': 'Action'},
            'disease': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['sanidade.Disease']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'sanidade.disease': {
            'Meta': {'object_name': 'Disease'},
            'causes_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'causes_es': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'causes_fr': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'causes_pt': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'duration_en': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'duration_es': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'duration_fr': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'duration_pt': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'name_es': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'name_fr': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'name_pt': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'treatment_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'treatment_es': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'treatment_fr': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'treatment_pt': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'})
        },
        'sanidade.indexmeasurements': {
            'Meta': {'object_name': 'IndexMeasurements'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'measurement_count': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'measurement_interval': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'name_es': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'name_fr': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'name_pt': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        'sanidade.indexmeasurementsvalues': {
            'Meta': {'object_name': 'IndexMeasurementsValues'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'index': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['sanidade.IndexMeasurements']"}),
            'intervention_entry': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['sanidade.InterventionEntry']"}),
            'value': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        'sanidade.intervention': {
            'Meta': {'object_name': 'Intervention'},
            'actions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['sanidade.Action']", 'symmetrical': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'index_measurements': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['sanidade.IndexMeasurements']", 'symmetrical': 'False'}),
            'is_mandatory': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_periodic': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'name_es': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'name_fr': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'name_pt': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'period': ('django.db.models.fields.CharField', [], {'max_length': '1', 'null': 'True', 'blank': 'True'}),
            'periodicy': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        'sanidade.interventionentry': {
            'Meta': {'object_name': 'InterventionEntry'},
            'animals': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['animal.Animal']", 'symmetrical': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'index_measurements_values': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['sanidade.IndexMeasurementsValues']", 'null': 'True', 'blank': 'True'}),
            'intervention': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['sanidade.Intervention']"}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {})
        },
        'utilizador.staff': {
            'Meta': {'object_name': 'Staff'},
            'breeder': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['entidade.Breeder']", 'null': 'True'}),
            'contact': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contactos.Contact']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'identification': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '250'}),
            'nif': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'post': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'user_profile': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['utilizador.UserProfile']", 'null': 'True', 'blank': 'True'})
        },
        'utilizador.userprofile': {
            'Meta': {'object_name': 'UserProfile', '_ormbases': ['utilizador.Staff']},
            'is_account_owner': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'staff_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['utilizador.Staff']", 'unique': 'True', 'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['auth.User']", 'unique': 'True'}),
            'user_type': ('django.db.models.fields.CharField', [], {'max_length': '1'})
        }
    }

    complete_apps = ['red']