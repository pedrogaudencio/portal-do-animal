from django.conf.urls.defaults import patterns, include, url

urlpatterns = patterns('red.views',
	url(r'^$', 'choose_year'),
    url(r'^(?P<red_year>\d+)/$', 'view_red', name='view_red'),

    url(r'^movimentos/$', 'view_movimentations', name='view_movimentations'),
    url(r'^movimentos/(?P<movement_pk>\d+)/$', 'view_movement_details', name='view_movement_details'),
    #url(r'^movimentos/add/$', 'add_movimentation', name='add_movimentation'),
    url(r'^movimentos/add/(?P<mov_type>\w+)/$', 'add_movimentation', name='add_movimentation'),
    url(r'^movimentos/remove/(?P<movement_pk>\d+)/$', 'remove_movimentation', name='remove_movimentation'),

    url(r'^movimentos/pendentes/accept_req/(?P<mov_pk>\d+)/$', 'accept_movimentation_request', name='accept_movimentation_request'),
    url(r'^movimentos/pendentes/reject_req/(?P<mov_pk>\d+)/$', 'reject_movimentation_request', name='reject_movimentation_request'),
    url(r'^movimentos/pendentes/', 'view_pending_movimentations', name='view_pending_movimentations'),

	url(r'^ocorrencias/$', 'view_occurrences', name='view_occurrences'),
    url(r'^ocorrencias/(?P<occurrence_pk>\d+)/$', 'view_occurrence_details', name='view_occurrence_details'),
    url(r'^ocorrencias/add/$', 'add_occurrence', name='add_occurrence'),
    url(r'^ocorrencias/remove/(?P<occurrence_pk>\d+)/$', 'remove_occurrence', name='remove_occurrence'),
)
