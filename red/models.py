# -*- coding: UTF-8 -*-
from django.db import models
from utils.to_float import to_float
from django.utils.translation import ugettext as _

#import models
from identificacao.models import Identifier


class RED(models.Model):
    farm = models.ForeignKey('exploracao.Farm')
    entity = models.ForeignKey('entidade.Entities')
    year = models.IntegerField(_('Year'))
    herd_count = models.PositiveIntegerField(_('Herd count'), max_length=20, default = 0, blank = False, null = False)
    book_number = models.PositiveIntegerField(_('Book number'), max_length=20, blank = False)
    book_appendix = models.PositiveIntegerField(_('Book number (appendix)'), max_length=20, blank = False)
    authorization_number = models.PositiveIntegerField(_('Authorization number'), max_length=20, blank = False)
    #dra = models.ForeignKey('entidade.Dra', related_name=, blank = True, null=True) #colocar false
    issuer = models.CharField(_('Issuing entity'), max_length = 50, blank = True) #colocar false
    obs = models.TextField(_('Observations'), blank = True, null = True)

    movimentations=models.ManyToManyField('Movement', verbose_name= _('Movements'), null=True, blank=True)
    identifications=models.ManyToManyField('AnimalIdentification', verbose_name= _('Animal Identification'), null=True, blank=True)
    occurrences=models.ManyToManyField('Occurrence', verbose_name= _('Occurrences'), null=True, blank=True)
    medical_interventions=models.ManyToManyField('sanidade.InterventionEntry', verbose_name=_('Medical interventions'), null=True, blank=True)

    last_update_timestamp=models.DateTimeField(auto_now=True)
    notes=models.TextField(_('Annotations'), blank = True, null = True)

    def __unicode__(self):
        return 'RED #%d' % (self.book_number)

    class Meta():
        unique_together = ("year", "farm")
        verbose_name= _('Registo de Existencias e Deslocacoes')
        verbose_name_plural= _('Registo de Existencias e Deslocacoes')
        ordering = ["year"]


class Event(models.Model):
    timestamp=models.DateTimeField(_('Date and time'), null=False, blank=False)

    class Meta():
        verbose_name = _('Event')
        verbose_name_plural= _('Events')
        ordering = ["-timestamp"]

    def __unicode__(self):
        return 'Event %s (%s)' % (self.pk, self.timestamp)


class Movement(Event):
    animals = models.ManyToManyField('animal.Animal', verbose_name= _('Animal(s)'))
    MOV_TYPE_CHOICES=(
                      ('entrada_compra','Entrada/Compra'),
                      ('identificacao','Identificação'),
                      ('saida_producao','Saída para Produção'),
                      ('saida_abate','Saída para Abate'),
                      ('abate_consumo','Abate para Consumo'),
                      )
    mov_type = models.CharField(_('Movement type'), max_length=15, choices=MOV_TYPE_CHOICES, null=False, blank=False)
    farm = models.ForeignKey('exploracao.Farm', verbose_name=_('Farm'), null=True, blank=True)
    entity = models.ForeignKey('entidade.Entities', verbose_name=_('Entity'))
    localization = models.CharField(_('Local'), max_length=30, null=False, blank=False)
    nr_guia_circ = models.PositiveIntegerField(_('Transport guide'), null=False, blank=False)
    obs = models.TextField(_('Observations'), blank=True, null=True)

    requested_from = models.ForeignKey('entidade.Entities', verbose_name=_('Entity requested from'), related_name='entity_requested_from', null=True, blank=True)
    requested_to = models.ForeignKey('entidade.Entities', verbose_name=_('Entity requested to'), related_name='entity_requested_to', null=True, blank=True)

    #if saida_producao:
    farm_requested_from = models.ForeignKey('exploracao.Farm', verbose_name=_('Farm requested from'), related_name='farm_requested_from', null=True, blank=True)
    farm_requested_to = models.ForeignKey('exploracao.Farm', verbose_name=_('Farm requested to'), related_name='farm_requested_to', null=True, blank=True)
    
    approved = models.BooleanField()
    request_timestamp = models.DateTimeField(_('Date and time of request'), auto_now=True, null=False, blank=False)
    answer_timestamp = models.DateTimeField(_('Date and time of answer'), null=True, blank=True)
    REQUEST_STATUS_CHOICES =(
                             (-1,'Rejeitado'),
                             (0,'Pendente'),
                             (1,'Aceite'),
                            )
    request_status = models.IntegerField(_('Request Status'), choices=REQUEST_STATUS_CHOICES, null=True, blank=True)

    class Meta():
        verbose_name = _('Movement')
        verbose_name_plural= _('Movements')
        ordering = ["-timestamp"]

    def __unicode__(self):
        return '%s (%s) - %s (%s %s)' % (self.mov_type, self.timestamp, self.localization, self.entity, self.nr_guia_circ)

    
class Occurrence(Event):
    animals = models.ManyToManyField('animal.Animal', verbose_name= _('Animal(s)'))
    OC_TYPE_CHOICES = (
                       ('desaparecimento','Desaparecimento'),
                       ('queda_brinco','Queda de brinco'),
                       ('outro','Outro'),
                       ('dupla_identificacao','Dupla Identificação')
                       )
    oc_type = models.CharField(_('Event Type'), max_length = 15, choices=OC_TYPE_CHOICES, blank=False, null=False)
    farm = models.ForeignKey('exploracao.Farm', verbose_name=_('Farm'), null=False, blank=False)
    guide_date = models.DateField(_('Guide date'), blank = True, null = True) #ver disto
    guide = models.CharField(_('Guide'), max_length = 20, blank = False, null = False) #ver disto
    obs = models.TextField(_('Observations'), blank=True, null=True)


    class Meta:
        verbose_name = _('Occurrence')
        verbose_name_plural = _('Occurrences')
        ordering = ["-timestamp"]


    def __unicode__(self):
        return '%s - %s - %s' % (self.oc_type, self.timestamp, self.animals)


class AnimalIdentification(Event):
    identifier = models.ForeignKey('identificacao.Identifier', related_name='identifier_animal')
    animal = models.ForeignKey('animal.Animal', related_name='animal_id')
    employee = models.ForeignKey('utilizador.Staff', related_name='colaborador_id', blank = True, null = True)
    entity_nif = models.IntegerField(_('NIF'), help_text= _('If identifier agent isn\'t the animal owner'), blank = True, null = True)

    def __unicode__(self):
        return '%s - %s' % (self.identifier, self.animal)

    class Meta():
        verbose_name = _('Animal identification')
        verbose_name_plural = _('Animal identifications')
        ordering = ["-timestamp"]

