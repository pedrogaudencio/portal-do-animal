# -*- coding: UTF-8 -*-
from django import forms
from django.forms import ModelForm, Form, Textarea, Select, SelectMultiple, TextInput, HiddenInput
from django.utils.translation import ugettext_lazy as _
from utils.widgets import DatePickerWidget
from red.models import Event, Movement, Occurrence


class MovementForm(ModelForm):
    
    class Meta:
        model = Movement
        fields = ('timestamp', 'mov_type', 'farm', 'animals', 'localization', 'nr_guia_circ', 'obs', 'entity')
        
        widgets = {'timestamp': DatePickerWidget(),
                   'mov_type': HiddenInput(),
                   'animals': SelectMultiple(attrs={'class': 'input-block-level'}),
                   'farm': Select(attrs={'class': 'input-block-level'}),
                   'entity': Select(attrs={'class': 'input-block-level'}),
                   'localization': TextInput(attrs={'class': 'input-block-level'}),
                   'nr_guia_circ': TextInput(attrs={'class': 'input-block-level'}),
                   'obs': Textarea(attrs={'rows':2, 'cols':40}),
                   }


class OccurrenceForm(ModelForm):
    
    class Meta:
        model = Occurrence
        fields = ('animals', 'oc_type', 'farm', 'timestamp', 'guide', 'guide_date', 'obs')
        
        widgets = {'animal': Select(attrs={'class': 'input-block-level'}),
                   'oc_type': Select(attrs={'class': 'input-block-level'}),
                   'farm': HiddenInput(),
                   'timestamp': DatePickerWidget(),
                   'guide': TextInput(attrs={'class': 'input-block-level'}),
                   'guide_date': DatePickerWidget(attrs={'class': 'input-block-level'}),
                   'obs': Textarea(attrs={'rows':2, 'cols':40}),
                   }


class MovementInForm(Form):

    def __init__(self, farm, *args, **kwargs):
        self.farm = farm
        self.entity = farm.entity
        super(MovementInForm, self).__init__(*args, **kwargs)

    farm = forms.CharField(label = _('Farm'), required = True)
    entity = forms.CharField(label = _('Entity'), required = True)
    timestamp = forms.DateTimeField(label = _('Date and Time'), required = True)
    localization = forms.CharField(label = _('Localization'), required = True)
    nr_guia_circ = forms.IntegerField(label = _('Transport Guide'), required = True)
    obs = forms.CharField(label = _('Observations'), required = False, 
                          widget = forms.Textarea(attrs={'rows':2, 'cols':40}))

    animals = forms.CharField()
    animal_birthdate = forms.DateTimeField()
    animal_sia = forms.CharField()
    animal_identification = forms.CharField()
    animal_male_castrated = forms.BooleanField()
    GENDER_CHOICES = (
                      ('M',_('Male')),
                      ('F',_('Female')),
                     )
    animal_gender = forms.ChoiceField(choices=GENDER_CHOICES)
    animal_specie_breed = forms.CharField()


