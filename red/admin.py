from red.models import RED, Movement, Occurrence, AnimalIdentification
from django.contrib import admin

class AnimalIdentificationAdmin(admin.ModelAdmin):
    
    fieldsets = [
                 (None, {
                         'classes': ['wide', 'extrapretty'],
                         'fields': (
                                    'identifier',
                                    'animal',
                                    'employee',
                                    'timestamp',
                                    )
                         }),
    ]
    list_display = ('animal','identifier','timestamp','employee')
    list_filter = ['timestamp']
    #list_display_links = ('animal')
    search_fields = ['identifier']


admin.site.register(RED)
admin.site.register(Movement)
admin.site.register(Occurrence)
admin.site.register(AnimalIdentification, AnimalIdentificationAdmin)
