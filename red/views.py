# -*- coding: UTF-8 -*-
from django.contrib.auth.decorators import login_required
from django.template.context import RequestContext
from django.shortcuts import render_to_response, get_object_or_404
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from notification.models import Notice

#import models
from red.models import RED, Event, Movement, Occurrence
from animal.models import Animal
from exploracao.models import Farm
from entidade.models import Breeder, Slaughterhouse, DeliveryChain

#import forms
from red.forms import MovementForm, OccurrenceForm#, MovementInForm

#others
from datetime import datetime, date
import copy


@login_required
def choose_year(request, farm_pk):
    farm = get_object_or_404(Farm, pk = farm_pk)
    
    context = {'red_list': RED.objects.filter(farm = farm),
               }

    return render_to_response('red/listar_anos.html', context)


@login_required
def view_red(request, farm_pk, red_year):
    #user = request.user
    farm = get_object_or_404(Farm, pk = farm_pk)
    red = None
    red = get_object_or_404(RED, year = red_year, farm = farm)

    events = []
    print red
    for mov in red.movimentations.all():
        event = [mov.timestamp.__str__(), mov.mov_type.__str__(), mov.nr_guia_circ, mov.farm.name+' ('+mov.farm.trademark.__str__()+')']
        events.append(event)
    print red.occurrences
    for oc in red.occurrences.all():
        event = [oc.timestamp.__str__(), oc.oc_type, oc.guide, oc.farm.name+' ('+oc.farm.trademark.__str__()+')']
        events.append(event)
    for idt in red.identifications.all():
        event = [idt.timestamp.__str__(), 'Identificacao', idt.identifier.__str__(), idt.entity_nif.__str__()]

    events_sorted = sorted(events, key=lambda event: event[0], reverse=False)
    print events_sorted

    context = {'red': red, 'events': events_sorted,
               }

    return render_to_response('red/listar.html', context)


def add_mov_to_red(movement):
    farm = movement.farm

    mov_year = movement.timestamp.year

    red_list = RED.objects.filter(farm=farm)
    the_red = None
    
    for red in red_list:
        if red.year == mov_year:
            the_red=red

    if the_red != None:
        the_red.movimentations.add(movement)
    else:
        the_red=open_new_red(farm, movement)
        the_red.movimentations.add(movement)


def remove_mov_from_red(movement):
    farm = movement.farm
    mov_year = movement.timestamp.year

    red_list = RED.objects.filter(farm=farm)
    the_red = None
    
    for red in red_list:
        if red.year == mov_year:
            the_red=red

    if the_red != None:
        the_red.movimentations.remove(movement)


def add_oc_to_red(occurrence):
    farm = occurrence.farm
    oc_year = occurrence.timestamp.year

    red_list = RED.objects.filter(farm=farm)
    the_red = None
    
    for red in red_list:
        if red.year == oc_year:
            the_red=red

    if the_red != None:
        the_red.occurrences.add(occurrence)
    else:
        the_red=open_new_red(farm, occurrence)
        the_red.occurrences.add(occurrence)


def remove_oc_from_red(occurrence):
    farm = occurrence.animal.farm
    oc_year = occurrence.event_date.year

    red_list = RED.objects.filter(farm=farm)
    the_red = None
    
    for red in red_list:
        if red.year == oc_year:
            the_red=red

    if the_red != None:
        the_red.movimentations.remove(occurrence)


def open_new_red(farm, event):
    r = farm.red_set.create(farm=farm, entity=farm.breeder, year=event.timestamp.year, book_number=0, book_appendix=0, authorization_number=0)

    '''
    a fazer:
    1- verificar se a entidade tem reds com o book_number a 0, significa que foi aberto automaticamente
    pode-se resolver na primeira consulta ao novo red ter de se inserir o nr para se poder ver
    '''

    return r


def open_red(farm):
    farm.red_set.create(farm=farm, entity=farm.breeder, year=datetime.now().year, book_number=0, book_appendix=0, authorization_number=0)


@login_required
def view_movimentations(request, farm_pk):
    #user = request.user
    farm = get_object_or_404(Farm, pk = farm_pk)

    context = {'movimentations': Movement.objects.filter(farm = farm, approved = True)
               }
    
    return render_to_response('movimentacao/listar.html', context)


@login_required
def view_movement_details(request, farm_pk, movement_pk):
	#user = request.user
    movement = get_object_or_404(Movement, pk = movement_pk)

    context = {'movement': movement,
               }
    
    return render_to_response('movimentacao/detalhes.html', context)


@login_required
def view_occurrences(request, farm_pk):
    #user = request.user
    farm = get_object_or_404(Farm, pk = farm_pk)

    context = {'occurrences': Occurrence.objects.filter(farm = farm),
               }
    
    return render_to_response('ocorrencia/listar.html', context)


@login_required
def view_occurrence_details(request, farm_pk, occurrence_pk):
	#user = request.user
    occurrence = get_object_or_404(Event, pk = occurrence_pk)

    context = {'occurrence': occurrence,
               }
    
    return render_to_response('ocorrencia/detalhes.html', context)


@login_required
def add_movimentation(request, farm_pk, mov_type, edit = False):
    #user = get_object_or_404(UserProfile, user=request.user)
    farm = Farm.objects.get(pk=farm_pk)    
    movement = None

    if request.method == 'POST':
        mov_form = MovementForm(request.POST)

        if mov_form.is_valid():
            movement = mov_form.save(commit=False)
            movement.mov_type = mov_type

            if mov_type == 'entrada_compra':
                '''
                Cria novo movimento para a saida na outra exploracao
                '''
                movement_out = copy.copy(movement) #ver depois se se deve usar copy ou deepcopy
                
                entity = movement_out.entity
                if Breeder.objects.get(pk=entity.pk) != None:
                    movement_out.mov_type = 'saida_producao'
                    movement_out.requested_to = entity
                    movement_out.farm_requested_to = movement.farm
                elif Slaughterhouse.objects.get(pk=entity.pk) != None:
                    movement_out.mov_type = 'saida_abate'
                    movement_out.requested_to = entity
                elif DeliveryChain.objects.get(pk=entity.pk) != None:
                    movement_out.mov_type = 'saida_consumo'
                    movement_out.requested_to = entity
                else:
                    print 'form mov_entrada: chegou ao pass'
                    pass#raise errorcenas

                movement_out.farm = movement.farm
                movement_out.farm_requested_to = movement.farm
                movement_out.farm_requested_from = farm
                movement_out.request_status = 0
                movement_out.save()

                movement.farm = farm
                movement.approved = True

                '''
                Verifica se os animais existem no sistema
                '''
                '''
                animals_list[]# <- lista de animais inseridos
                for animal_idt in animals_list:
                    if Animal.objects.get(identifier=animal_idt):
                        movement.animals.add(animal)
                    else:
                        pass
                        #adiciona os animais ao sistema e mete numa lista de preenchimento de dados de animais pendentes ou pede logo para preencher o resto dos dados (por js??)
                '''

            '''
            elif mov_type = 'identificacao':
                movement.mov_type = 'identificacao' # remeter para a view de identificação??
            '''
            if mov_type == 'saida_producao' or mov_type == 'saida_abate' or mov_type == 'abate_consumo':
                '''
                Cria novo movimento para a entrada na outra explocacao
                '''
                movement_in = copy.copy(movement) #ver depois se se deve usar copy ou deepcopy
                movement_in.mov_type = 'entrada_compra'
                movement_in.farm = movement.farm
                movement_in.farm_requested_to = movement.farm
                movement_in.requested_to = movement.entity
                movement_in.farm_requested_from = farm
                movement_in.requested_from = farm.breeder
                movement_in.request_status = 0
                movement_in.save()

                movement.farm = farm
                movement.entity = farm.breeder
                movement.approved = True


            movement.save()
            add_mov_to_red(movement) #adiciona o movimento ao RED respetivo

            return HttpResponseRedirect(reverse('view_movimentations', args=(farm_pk,)))

    else:
        mov_form = MovementForm(initial={'mov_type': mov_type})

        if mov_type == 'entrada_compra':
            mov_form.fields["animals"].queryset = Animal.objects.exclude(farm = farm)
            mov_form.fields["farm"] = farm # a farm aqui entende-se como a exploração onde entra
        #elif mov_type == 'identificacao':
        #    return HttpResponseRedirect(reverse('add_identification', args=(farm.pk,)))
        elif mov_type == 'saida_producao':
            mov_form.fields["animals"].queryset = Animal.objects.filter(farm = farm)
            mov_form.fields["farm"].queryset = Farm.objects.exclude(pk = farm_pk) # a farm aqui entende-se como a exploração para onde vai
            mov_form.fields["entity"] = farm.breeder
            #mov_form.fields["entity"].queryset = Breeder.objects.all()
        elif mov_type == 'saida_abate':
            mov_form.fields["animals"].queryset = Animal.objects.filter(farm = farm)
            mov_form.fields["farm"] = farm # a farm aqui entende-se como a exploração donde sai
            mov_form.fields["entity"].queryset = Slaughterhouse.objects.all()
        elif mov_type == 'abate_consumo':
            mov_form.fields["animals"].queryset = Animal.objects.filter(farm = farm)
            mov_form.fields["farm"] = farm # a farm aqui entende-se como a exploração donde sai
            mov_form.fields["entity"].queryset = DeliveryChain.objects.all()

    context = {'mov_form': mov_form}

    return render_to_response('movimentacao/add_movimentation.html', context, context_instance=RequestContext(request))


@login_required
def remove_movimentation(request, farm_pk, movement_pk):
    movement = get_object_or_404(Movement, pk=movement_pk)
    remove_mov_from_red(movement)
    movement.delete()

    return HttpResponseRedirect(reverse('view_movimentations', args=(farm_pk,)))


@login_required
def view_pending_movimentations(request, farm_pk):
    farm = get_object_or_404(Farm, pk=farm_pk)
    pending_movs = Movement.objects.filter(farm = farm, request_status = 0)
    rejected_reqs = Movement.objects.filter(farm = farm, request_status = -1)
    accepted_reqs = Movement.objects.filter(farm = farm, request_status = 1)
    print 'pending: ',pending_movs
    print 'accepted: ',accepted_reqs
    print 'rejected: ',rejected_reqs

    context = {'pending_movs': pending_movs, 'rejected_reqs': rejected_reqs, 'accepted_reqs': accepted_reqs,
               }
    
    return render_to_response('movimentacao/pendentes.html', context)


@login_required
def accept_movimentation_request(request, farm_pk, mov_pk):
    print 'entrou accept_movimentation_request'
    mov = get_object_or_404(Movement, pk=mov_pk)
    mov.request_status = 1
    mov.approved = True
    mov.answer_timestamp = datetime.now()
    mov.save()
    #alterar a farm do animal
    print 'movimento '+mov.__str__()+'aceite'
    add_mov_to_red(mov)
    #adicionar notificação para avisar o request_from que o pedido foi aceite

    return HttpResponseRedirect(reverse('view_pending_movimentations', args=(farm_pk,)))


@login_required
def reject_movimentation_request(request, farm_pk, mov_pk):
    mov = get_object_or_404(Movement, pk=mov_pk)
    mov.request_status = -1
    mov.approved = False
    mov.answer_timestamp = datetime.now()
    mov.save()
    #adicionar notificação para avisar o request_from que o pedido foi rejeitado

    return HttpResponseRedirect(reverse('view_pending_movimentations', args=(farm_pk,)))


@login_required
def add_occurrence(request, farm_pk, edit = False):
    #user = get_object_or_404(UserProfile, user=request.user)
    farm = get_object_or_404(Farm, pk=farm_pk)
    occurrence = None

    if request.method == 'POST':
        oc_form = OccurrenceForm(request.POST)

        if oc_form.is_valid():
            occurrence = oc_form.save(commit=False)
            print "oc farm: ", farm
            occurrence.farm = farm
            occurrence.save()
            add_oc_to_red(occurrence) #adiciona a ocorrência ao RED respetivo

            return HttpResponseRedirect(reverse('view_occurrences', args=(farm_pk,)))
    else:
        oc_form = OccurrenceForm(initial={'farm': farm})
        oc_form.fields["animals"].queryset = Animal.objects.filter(farm = farm)

    context = {'oc_form': oc_form}

    return render_to_response('ocorrencia/add_occurrence.html', context, context_instance=RequestContext(request))
                                             

@login_required
def remove_occurrence(request, farm_pk, occurrence_pk):
    occurrence = get_object_or_404(Event, pk=occurrence_pk)
    remove_oc_from_red(occurrence)
    occurrence.delete()

    return HttpResponseRedirect(reverse('view_occurrences', args=(farm_pk,)))

