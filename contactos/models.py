# -*- coding: UTF-8 -*-
from django.db import models
from django.utils.translation import ugettext as _


class District(models.Model):
    district = models.CharField(max_length=50, blank=False, unique=True)

    def __unicode__(self):
        return '%s'% (self.district)

class Concelho(models.Model):
    concelho = models.CharField(max_length=50, blank=False, unique=True)

    def __unicode__(self):
        return '%s'% (self.concelho)

class Freguesia(models.Model):
    freguesia = models.CharField(max_length=50, blank=False, unique=True)

    def __unicode__(self):
        return '%s'% (self.freguesia)

class Contact(models.Model):
    company_number = models.CharField(_('Company number'), max_length=20, blank=True, null=True)
    company_number_extension = models.CharField(_('Company number extension'), max_length=5, blank=True, null=True)
    cellphone = models.CharField(_('Cellphone'), max_length=20, blank=True, null=True)
    home_number= models.CharField(_('Home number'), max_length=20, blank=True, null=True)

    fax =  models.CharField(max_length=20, blank=True, null=True)

    address = models.CharField(max_length=50, blank=False)
    zip_code = models.CharField(_('Zip code'), max_length=10, blank=False)
    district = models.ForeignKey('District', verbose_name = _('District'), blank=False)
    concelho = models.ForeignKey('Concelho', verbose_name = _('Concelho'), blank=False)
    freguesia = models.ForeignKey('Freguesia', verbose_name = _('Freguesia'), blank=False)

    class Meta():
        verbose_name = _('Contact')
        verbose_name_plural= _('Contacts')
        
    def __unicode__(self):
        return '%s (%s, %s)' % (self.address, self.freguesia, self.concelho)
        