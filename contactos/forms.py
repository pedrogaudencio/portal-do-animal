# -*- coding: UTF-8 -*-
from contactos.models import Contact
from django.forms import ModelForm


class AddressForm(ModelForm):
    
    class Meta:
        model = Contact
        fields = ('address', 'freguesia', 'zip_code', 'concelho', 'district')
        widgets = {}
