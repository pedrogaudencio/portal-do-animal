# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'District'
        db.create_table('contactos_district', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('district', self.gf('django.db.models.fields.CharField')(unique=True, max_length=50)),
        ))
        db.send_create_signal('contactos', ['District'])

        # Adding model 'Concelho'
        db.create_table('contactos_concelho', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('concelho', self.gf('django.db.models.fields.CharField')(unique=True, max_length=50)),
        ))
        db.send_create_signal('contactos', ['Concelho'])

        # Adding model 'Freguesia'
        db.create_table('contactos_freguesia', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('freguesia', self.gf('django.db.models.fields.CharField')(unique=True, max_length=50)),
        ))
        db.send_create_signal('contactos', ['Freguesia'])

        # Adding model 'Contact'
        db.create_table('contactos_contact', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('company_number', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('company_number_extension', self.gf('django.db.models.fields.CharField')(max_length=5, null=True, blank=True)),
            ('cellphone', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('home_number', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('fax', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('address', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('zip_code', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('district', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contactos.District'])),
            ('concelho', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contactos.Concelho'])),
            ('freguesia', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contactos.Freguesia'])),
        ))
        db.send_create_signal('contactos', ['Contact'])

    def backwards(self, orm):
        # Deleting model 'District'
        db.delete_table('contactos_district')

        # Deleting model 'Concelho'
        db.delete_table('contactos_concelho')

        # Deleting model 'Freguesia'
        db.delete_table('contactos_freguesia')

        # Deleting model 'Contact'
        db.delete_table('contactos_contact')

    models = {
        'contactos.concelho': {
            'Meta': {'object_name': 'Concelho'},
            'concelho': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'contactos.contact': {
            'Meta': {'object_name': 'Contact'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'cellphone': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'company_number': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'company_number_extension': ('django.db.models.fields.CharField', [], {'max_length': '5', 'null': 'True', 'blank': 'True'}),
            'concelho': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contactos.Concelho']"}),
            'district': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contactos.District']"}),
            'fax': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'freguesia': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contactos.Freguesia']"}),
            'home_number': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'zip_code': ('django.db.models.fields.CharField', [], {'max_length': '10'})
        },
        'contactos.district': {
            'Meta': {'object_name': 'District'},
            'district': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'contactos.freguesia': {
            'Meta': {'object_name': 'Freguesia'},
            'freguesia': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        }
    }

    complete_apps = ['contactos']