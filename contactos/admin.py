from contactos.models import Contact, District, Concelho, Freguesia
from django.contrib import admin

class ContactAdmin(admin.ModelAdmin):
    fieldsets = [
                 (None, {
                         'classes': ['wide', 'extrapretty'],
                         'fields': (
                                    ('address','zip_code'),
                                    ('district','concelho','freguesia'),
                                    ('company_number','company_number_extension'),
                                    ('cellphone','home_number','fax'),
                                    )
                         }),
    ]
    list_display = ('address','zip_code','district')#'disponivel')
    #list_display_links = ('__unicode__','morada')
    list_filter = ['district']
    search_fields = ['address'] #morada
    #save_as = True
    
class DistrictAdmin(admin.ModelAdmin):
    fieldsets = [
                 (None, {
                         'classes': ['wide', 'extrapretty'],
                         'fields': ('district',)
                         }),
    ]
    #save_as = True
    
class ConcelhoAdmin(admin.ModelAdmin):
    fieldsets = [
                 (None, {
                         'classes': ['wide', 'extrapretty'],
                         'fields': ('concelho',)
                         }),
    ]
    #save_as = True
    
class FreguesiaAdmin(admin.ModelAdmin):
    fieldsets = [
                 (None, {
                         'classes': ['wide', 'extrapretty'],
                         'fields': ('freguesia',)
                         }),
    ]
    #save_as = True

admin.site.register(Contact, ContactAdmin)
admin.site.register(District, DistrictAdmin)
admin.site.register(Concelho, ConcelhoAdmin)
admin.site.register(Freguesia, FreguesiaAdmin)