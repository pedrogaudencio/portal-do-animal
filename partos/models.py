# -*- coding: UTF-8 -*-
from django.db import models
from django.utils.translation import ugettext as _
#from pda.Animal.models import Animal

class Birth(models.Model):
    mother = models.ForeignKey('animal.Animal', related_name='female_birth', blank = False)
    father = models.ForeignKey('animal.Animal', related_name='male_birth', blank = True, null=True)
    donor = models.ForeignKey('animal.Animal', help_text = _('Animal embryos'), blank = True, null = True)
    farm = models.ForeignKey('exploracao.Farm')
    birth_date = models.DateTimeField(_('Birth date'), blank = False)
    offspring_number = models.PositiveIntegerField(_('Number of offspring'), blank = False)
    birth_number = models.PositiveIntegerField(_('Birth number'), blank = False) #incrementar depois!!!
    dificuldade_choices = (('Normal', _('Normal')),
                           ('Com_ajuda_facil','Com ajuda - fácil'),
                           ('Com_ajuda_dificil','Com ajuda - dificil'),
                           ('Cesariana', 'Cesariana'),
                           ('Aborto', 'Aborto'))
    kind = models.CharField(max_length=15, choices = dificuldade_choices, blank = False)
    accoucheur = models.ManyToManyField('utilizador.UserProfile', blank = False, null = False)
    reproduction_choices = (('Cobricao','Cobrição'),('Inseminacao','Inseminação'),('Desconhecido','Desconhecido'))
    reproduction = models.CharField(max_length=25, choices = reproduction_choices, blank = False)
    obs = models.TextField(_('Observations'), blank = True, null=True)

    def __unicode__(self):
        return '%s - %s' % (self.mother, self.birth_date)

    class Meta():
        verbose_name = 'Birth'
        verbose_name_plural = 'Births'