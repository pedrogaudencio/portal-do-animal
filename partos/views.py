# -*- coding: UTF-8 -*-
from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response, render, get_object_or_404
from django.template.context import RequestContext

from exploracao.models import Farm
from partos.models import Birth

#from partos.forms import AddBirth

@login_required
def viewBirths(request):
    #user = request.user
    thisfarm = get_object_or_404(Farm.objects.get(id = 1)) #FIX id 
    
    allbirths = get_object_or_404(Birth.objects.filter(farm = thisfarm))
    
    return render_to_response('ordenha/milk_storage.html', {'births': allbirths,})


@login_required
def getBirth(request):
    #user = request.user
    birth = Birth.objects.get(id = 1) #FIX id
    
#    birth = get_object_or_404(Partos, parto=thisbirth)
    
    return render(request, 'ordenha/milk_storage.html', {'birth': birth,
            })


@login_required
def addBirth(request):
    #user = request.user
    farm = Farm.objects.get(id = 1) #FIX id

    form = AddBirth(farm)
    
    if request.method == 'POST':
        if 'submit' in request.POST:
            form = AddBirth(farm, request.POST)
            if form.is_valid():
                form.save()
                return render_to_response('done.html')
    else:
        form = AddBirth(farm)

    context = {'form': form,
               }
    return render_to_response('ordenha/milk_storage_add.html', context,
                              context_instance=RequestContext(request))


@login_required
def viewMating(request):
    #user = request.user
    thisfarm = get_object_or_404(Farm.objects.get(id = 1)) #FIX id
    
    mating = get_object_or_404(Birth.objects.filter(farm = thisfarm, reproducao = 'Cobricao')) #FIX id
    
    return render_to_response('ordenha/milk_storage.html', {'mating': mating,})


@login_required
def viewInseminations(request):
    #user = request.user
    thisfarm = get_object_or_404(Farm.objects.get(id = 1)) #FIX id
    
    mating = get_object_or_404(Birth.objects.filter(farm = thisfarm, reproducao = 'Inseminacao')) #FIX id
    
    return render_to_response('ordenha/milk_storage.html', {'mating': mating,})


@login_required
def viewUnknown(request):
    #user = request.user
    thisfarm = get_object_or_404(Farm.objects.get(id = 1)) #FIX id
    
    mating = get_object_or_404(Birth.objects.filter(farm = thisfarm, reproducao = 'Desconhecido')) #FIX id
    
    return render_to_response('ordenha/milk_storage.html', {'mating': mating,})

