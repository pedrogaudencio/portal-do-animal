from partos.models import Birth
from django.contrib import admin

class BirthAdmin(admin.ModelAdmin):
    """
    fieldsets = [
                 (None, {
                         'classes': ['wide', 'extrapretty'],
                         'fields': (
                                    ('mother','father'),
                                    'birth_date',
                                    ('numero_parto','offspring_number'),
                                    ('reproduction','kind'),
                                    'obs',
                                    )
                         }),
    ]
    """
    filter_horizontal = ['accoucheur']
    list_display = ('birth_date','mother','father')
    #list_display_links = ('__unicode__','morada')
    list_filter = ['reproduction','kind']
    search_fields = ['mother']
    #save_as = True

admin.site.register(Birth, BirthAdmin)